<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">
 <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,400,700,900" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('css/fonts/fontawesome/font-awesome.min.css')}}">

<!--ikonki-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


 <!--ikonki-->

    <title>Panel CMS</title>
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css')}}">
    <!-- Styles -->
  
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/fix.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fix2.min.css') }}" rel="stylesheet">


    <link href="{{ asset('css/cms/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cms/datatable.css') }}" rel="stylesheet">



    
 <script src="{{ URL::asset('js/jquery.min.js')}}"></script>

 <!--sortable-->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!--lightbox-->
 <link rel="stylesheet" href="{{ asset('css/cms/jquery.fancybox.min.css')}}">
<script src="{{ asset('js/cms/jquery.fancybox.min.js')}}"></script>
<!--lightbox-->


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

</head>
<body>

  <div class="dashboard">
      <div class="dashboard-menu-box">
        <nav class="navbar navbar-default">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="menu-head">
        <a href="" target="_blank">
          <img src="" class="App-logo" alt="logo" />
        </a>
      </div>
    </div>

            <div class="menu collapse navbar-collapse" id="main-menu">
          <ul class="nav navbar-nav" v-on:click="$parent.refresh">
            @include('cms/cms.cmsmenu')
           
          </ul>

        </div>

</nav>

      </div>
      <div class="dashboard-header-box">
        <div class="logout-box">
          Zalogowany jako:  {{Auth::user()->customer->first_name}} {{Auth::user()->customer->last_name}}  | 
          @if(Auth::user()->AdminUser())
          <a href='/cms/admin'>Ustawienia admina</a> |
          @endif
            <a href='{{route('getlogoutcms')}}'><span>Wyloguj się</span><img src="/img/icons/logout.svg" class="icon icon-logout" /></a>
        </div>
      </div>
  
      <div>
<div class="dashboard-content-box">
   

       
            @yield('content')
       
    </div>
      </div>
  
    </div>

   

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('js/cms/jquery.dataTables.js') }}"></script>
      <script src="{{ asset('js/main.js') }}"></script>
   
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

   <script src="{{ asset('js/cms/chart.js') }}"></script>
    
   <script src="{{ url('vendor/ckeditor/ckeditor.js') }}"></script>
   <script src="{{ asset('js/cms/ckeditor.js') }}"></script>
   <script src="{{ asset('js/cms/dropzone.js') }}"></script>
   <script src="{{ asset('js/cms/main.js') }}"></script>

</body>
</html>
