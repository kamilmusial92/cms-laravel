@extends('layouts.app')

@section('content')

<div>
   <div class="App-header d-flex justify-items-start">
            <img src="/img/logo_interactivevision.svg" class="App-logo" alt="logo" />
        </div>


<div class="container">
     <div class="Login-content d-flex align-items-center">
    <div class="Login-title">
            <h1>Witamy w panelu CMS InteractiveVision</h1>
        </div>
    <div class="Message-content">
      <p>Zaloguj się</p>
  
      <span ></span>
    </div>

    <form method="POST" action="{{ route('logincms') }}">
   @csrf
      <div class="form-login">
  
        <p class="control has-icon has-icon-right">
          <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Login (e-mail)">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                
        </p>
        <p class="control has-icon has-icon-right">
     <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Hasło" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
        </p>
        <div class="help">
         
          
        </div>
        <button class="Confirm-button button is-primary" type="submit" name="button">Zaloguj się</button>
      </div>
  
    </form>


</div>
</div>
</div>
@endsection
