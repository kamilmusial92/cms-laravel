


@extends('shop/master')

@section('content')


  <div class="row register-section">
      <div class="col-12">
        <div class="register-section__header">
          <h2>Przypomnienie hasła</h2>
        </div>
      </div>
      <div class="col-12">
      <div class="register-section__container">
        <div class="row no-gutters">
          <div class="col-12 col-md-5">
            <div class="register-section__container--box">
              <h2>Przypomnienie hasła</h2>
              <p>Wpisz adres e-mail.</span></p>
                  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
 @include('notification')
                      @include('form_errors')
              <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                <div class="form--group">
                     <input type="email"  name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
             
    
             

                <button  type="submit" class="button gold">Wyślij</button>
                   </div>
              </form>


            </div>
          </div>
      
        </div>
      </div>
      </div>
    </div>



@endsection




