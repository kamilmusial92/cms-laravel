
@extends('shop/master')

@section('content')


  <div class="row register-section">
      <div class="col-12">
        <div class="register-section__header">
          <h2>Resetowanie hasła</h2>
        </div>
      </div>
      <div class="col-12">
      <div class="register-section__container">
        <div class="row no-gutters">
          <div class="col-12 col-md-5">
            <div class="register-section__container--box">
              <h2>Resetowanie hasła</h2>
              <p>Uzupełnij formularz.</span></p>
                  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
 @include('notification')
                      @include('form_errors')
               <form method="POST" action="{{ route('password.request') }}">
                        @csrf
                <div class="form--group">
                     <input type="email"  name="email" placeholder='adres e-mail' value="{{ old('email') }}" required>
  <input type="hidden" name="token" value="{{ $token }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
              <input  type="password"  name="password" placeholder='nowe hasło' required>
     @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                <input  type="password"  placeholder='powtórz nowe hasło' name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
             

                <button  type="submit" class="button gold">Wyślij</button>
                   </div>
              </form>


            </div>
          </div>
      
        </div>
      </div>
      </div>
    </div>



@endsection



