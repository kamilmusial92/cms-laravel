@extends('shop/master')

@section('content')


  <div class="row register-section">
      <div class="col-12">
        <div class="register-section__header">
          <h2>Rejestracja</h2>
        </div>
      </div>
      <div class="col-12">
      <div class="register-section__container">
        <div class="row no-gutters">
          <div class="col-12 col-md-12">
            <div class="register-section__container--box">
              <h2>Zarejestruj się</h2>
              <p>Jeśli nie posiadasz jeszcze konta<span> w naszym sklepie.</span></p>


            @include('notification')
                      @include('form_errors')
              <form class="form__login form-validate"  method="POST" action="{{ route('register') }}">
                   @csrf
                <div class="row no-gutters">
                  <div class="col-12 col-md-6">

                    <h3>Dane logowania</h3>
                    <div class="form--group">
                      <input name="email" value="{{ old('email') }}" type="email" placeholder="Adres email" required>
                      <input name="password"  type="password" placeholder="Hasło" required>
                      <input name="password_confirmation" type="password" placeholder="Powtórz hasło" required>
                    </div>

                  </div>

                  <div class="col-12 col-md-6">
                      <!-- <h3>Dostawa na inny adres</h3>
                      <div class="form--group">
                          <input type="email" placeholder="Dane kontaktowe / Adres Dostawy">
                          <input type="email" placeholder="Imię ">
                          <input type="email" placeholder="Nazwisko">
                          <input type="email" placeholder="Firma">
                          <input type="email" placeholder="Kod pocztowy">
                          <input type="email" placeholder="Miejscowość">
                          <input type="email" placeholder="Ulica i numer domu">
                          <input type="email" placeholder="Telefon">
                        </div> -->

                        <h3>Dane kontaktowe / Adres Dostawy</h3>
                        <div class="form--group">
                          <input name="nip" type="text" placeholder="NIP" value="{{ old('nip') }}">
                          <input name="company_name" type="text" value="{{ old('company_name') }}" placeholder="Nazwa firmy">
                          <input name="first_name" type="text" placeholder="Imię" value="{{ old('first_name') }}" required>
                          <input name="last_name" type="text" placeholder="Nazwisko" value="{{ old('last_name') }}" required>
                          <input name="postcode" type="text" placeholder="Kod pocztowy" value="{{ old('postcode') }}" required>
                          <input name="city" type="text" value="{{ old('city') }}" placeholder="Miejscowość" required>
                          <input name="address" type="text" value="{{ old('address') }}" placeholder="Ulica i numer domu" required>
                          <input name="country" type="text" value="{{ old('country') }}" placeholder="Kraj" required>
                          <input name="phone" type="text" value="{{ old('phone') }}" placeholder="Telefon" required>
                        </div>
                <div class="g-recaptcha" data-sitekey="6LexGmgUAAAAALbBtE7Ysj9w2m6BWn10jw6QlXlh" data-callback="onSubmit"></div>
                        <label class="form__acceptable defauly-checkbox">
                          <input type="checkbox" name="acceptable" />
                          <span class="checkmark"></span>
                          Akceptuję warunki <a href="/menu/regulamin" target="_blank">regulaminu</a>. Zgadzam się na otrzymywanie informacji dotyczących zamówień w myśl ustawy z dnia 18 lipca 2002r. o świadczeniu usług drogą elektroniczną.
                        </label>

                        <span id="error-login" class="error-login">Wypełnij poprawnie obowiązkowe pola</span>
                        <span id="error-checkbox" class="error-login">Zaakceptuj regulamin</span>

                        <button onclick="validateForm(1)" type="submit" class="button gold">Załóż konto</button>

                    </div>
  
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>
      </div>
    </div>


@endsection
