@extends('shop/master')


@section('content')


  <div class="row register-section">
      <div class="col-12">
        <div class="register-section__header">
          <h2>Logowanie i rejestracja</h2>
        </div>
      </div>
      <div class="col-12">
      <div class="register-section__container">
        <div class="row no-gutters">
          <div class="col-12 col-md-5">
            <div class="register-section__container--box">
              <h2>Zaloguj się</h2>
              <p>Jeśli posiadasz już konto <span> w naszym sklepie.</span></p>
 @include('notification')
                      @include('form_errors')
              <form class="form__login form-validate" name="Login" method="POST" action="{{ route('login') }}">
                   @csrf
                <div class="form--group">
                  <input name="email" id="email" type="email" placeholder="adres email" value="{{ old('email') }}" required>
                  <input name="password" id="password" type="password" placeholder="hasło" required>
                </div>
      <div class="g-recaptcha" data-sitekey="6LexGmgUAAAAALbBtE7Ysj9w2m6BWn10jw6QlXlh" data-callback="onSubmit"></div>
                <a href="{{route('password.request')}}" class="form__login--lost-password">Nie pamiętasz hasła?</a>

                <span id="error-login" class="error-login">Niepoprawny login lub hasło</span>

                <button onclick="validateForm()" href="register_ok" type="submit" class="button gold">Zaloguj się</button>
              </form>


            </div>
          </div>
          <div class="col-12 col-md-7 register-section__container--border-left">
            <div class="register-section__container--box">
              <h2>Zarejestruj się</h2>
              <p>Jeśli nie posiadasz jeszcze konta<span> w naszym sklepie.</span></p>
              <div class="register-section__container--box--link">
                <a href="/register" class="button gold">Zarejestruj się</a>
              </div>

            </div>
          </div>
        </div>
      </div>
      </div>
    </div>



@endsection
