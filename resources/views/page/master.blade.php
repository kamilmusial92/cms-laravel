<!DOCTYPE html>
<html lang="pl-PL" prefix="og: http://ogp.me/ns#" >
    <head>
       
        <title>{{$page->title_page}}</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="InteractiveVision.pl" />
        <meta name="description" content="{{$page->description_page}}" />
        <meta name="keywords" content="{{$page->keywords_page}}">
        <meta property="og:locale" content="pl_PL"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="{{$page->title_page}}"/>
        <meta property="og:description"
          content="{!! strip_tags($page->page_description)!!}"/>
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:site_name" content="{{$page->title_page}}"/>
        <meta property="og:image" content="{{$page->image_page}}"/>
        <meta property="og:image:type" content="image/jpeg"/>

        <meta property="og:image:alt" content="{{$page->title_page}}"/>

 <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ URL::asset('page/dist/bootstrap-4.1.3/css/bootstrap.min.css')}}">

    <!-- Slick -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('page/dist/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('page/dist/slick/slick-theme.css')}}"/>

    <!-- fancyBox CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('page/dist/fancybox-2.1.7/source/jquery.fancybox.css?v=2.1.5')}}" media="screen" />

    <!-- AOS -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <!-- Custom css -->
    <link rel="stylesheet" href="{{ URL::asset('page/dist/css/main.css')}}">

    </head>
    <body>

            @yield('content')

  <!-- jQuery -->
  <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

  <!-- jQuery mobile -->
  <script src="http://code.jquery.com/mobile/1.2.1/jquery.mobile-1.2.1.min.js"></script>

  <!-- Bootstrap -->
  <script src="{{ URL::asset('page/dist/bootstrap-4.1.3/js/bootstrap.min.js')}}"></script>

  <!-- Slick -->
  <script type="text/javascript" src="{{ URL::asset('page/dist/slick/slick.min.js')}}"></script>

  <!-- Custom js -->
  <script src="{{ URL::asset('page/dist/js/main.js')}}"></script>

  <!-- fancyBox JS -->
  <script type="text/javascript">
    $(document).ready(function() {


      $('.fancybox').fancybox();

      $(".fancybox-effects-d").fancybox({
        padding: 0,

        openEffect : 'elastic',
        openSpeed  : 150,

        closeEffect : 'elastic',
        closeSpeed  : 150,

        closeClick : true,

        helpers : {
        overlay : null
        }
      });
    });
  </script>
  <script type="text/javascript" src="{{ URL::asset('page/dist/fancybox-2.1.7/source/jquery.fancybox.pack.js?v=2.1.5')}}"></script>

  <!-- AOS -->
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
  AOS.init({
     duration: 1200,
  });
  </script>

</body>
</html>