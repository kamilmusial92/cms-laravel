<section class="custom-padding-t custom-padding-b">
    <div class="container" style="background-image: url('{{$component->backgroundsrc($component->background)}}'); background-repeat: no-repeat; background-size: cover;">
      <!-- Box-text-left -->
      <div class="box-text-left row">
        <div class="col-xs-12 col-lg-6">
          <h2>{{$component->title}}</h2>
          <p>{!!$component->text!!}</p>
          <p><a href="{{$component->url}}" class="button">Zobacz szczegółowy opis</a></p>
        </div>
        <div class="col-xs-12 col-lg-6">
          {!!$component->imgsrc($component->image,"")!!}
        </div>
      </div>
      <!-- END Box-text-left -->
    </div>
  </section>