<a class="pb-0" href="{{$component->url}}" target="_blank">
            <svg width="21px" height="21px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>002-instagram</title>
                <desc>Created with Sketch.</desc>
                <g id="-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.500558036">
                    <g id="optimal-www---strona-głowna" transform="translate(-1626.000000, -83.000000)" fill="#7c7a7a">
                        <g id="002-instagram" transform="translate(1626.000000, 83.000000)">
                            <g id="insta2">
                                <path d="M12.375,0 L5.625,0 C2.518875,0 0,2.518875 0,5.625 L0,12.375 C0,15.481125 2.518875,18 5.625,18 L12.375,18 C15.481125,18 18,15.481125 18,12.375 L18,5.625 C18,2.518875 15.481125,0 12.375,0 Z M16.3125,12.375 C16.3125,14.54625 14.54625,16.3125 12.375,16.3125 L5.625,16.3125 C3.45375,16.3125 1.6875,14.54625 1.6875,12.375 L1.6875,5.625 C1.6875,3.45375 3.45375,1.6875 5.625,1.6875 L12.375,1.6875 C14.54625,1.6875 16.3125,3.45375 16.3125,5.625 L16.3125,12.375 Z" id="Shape" fill-rule="nonzero"></path>
                                <path d="M9,4.5 C6.514875,4.5 4.5,6.514875 4.5,9 C4.5,11.485125 6.514875,13.5 9,13.5 C11.485125,13.5 13.5,11.485125 13.5,9 C13.5,6.514875 11.485125,4.5 9,4.5 Z M9,11.8125 C7.44975,11.8125 6.1875,10.55025 6.1875,9 C6.1875,7.448625 7.44975,6.1875 9,6.1875 C10.55025,6.1875 11.8125,7.448625 11.8125,9 C11.8125,10.55025 10.55025,11.8125 9,11.8125 Z" id="Shape" fill-rule="nonzero"></path>
                                <circle id="Oval" fill-rule="evenodd" cx="14.1428571" cy="3.85714286" r="1"></circle>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
          </a>