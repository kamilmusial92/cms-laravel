@extends ('page/master')
@section('content')

@foreach($components as $component)
   
   
    @include($component->view())
    
    
@endforeach

@stop