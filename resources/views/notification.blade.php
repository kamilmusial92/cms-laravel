 									@if(Session::has('success'))
									<div class="alert alert-success card">
									{{Session::get('success')}}
									</div>
									@endif

										@if(Session::has('error'))
									<div class="alert alert-danger card">
									{{Session::get('error')}}
									</div>
									@endif

										@if(Session::has('info'))
									<div class="alert alert-warning card">
									{{Session::get('info')}}
									</div>
									@endif