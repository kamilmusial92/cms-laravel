@extends('layouts.cms')
@section('content')


<div class="dashboard-inside">

  <div class="dashboard-head">
    <h2>Użytkownik: <b>{{Auth::user()->customer->first_name}} {{Auth::user()->customer->last_name}}</b></h2>
  </div>
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')
      @include('form_errors')

      <h3>Zmiana hasła</h3> 

      <form action="/cms/user/{{$user->id}}/editpassword" method='post'>
        @method('PUT')
        @csrf

      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="current_password" type="password"  value="{{old('current_password')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Aktualne hasło</span>
          </label>

        </div>
      
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="new_password" type="password"  value="{{old('new_password')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nowe hasło</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
            <input id="input-1" class="input__field" name="repeat_new_password" type="password"  value="{{old('repeat_new_password')}}">
            <label class="input__label input__label--haruki" for="input-1">
              <span class="input__label-content input__label-content--haruki">Powtórz nowe hasło</span>
            </label>
  
          </div>

      </div>

     
  
      <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Zapisz</button>
        </div>
      </div>
    </form>






    </div>

  </div>
</div>




@stop

