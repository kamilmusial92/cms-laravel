@extends('layouts.cms')
@section('content')


<div class="dashboard-inside">

  <div class="dashboard-head">
    <h2>Użytkownik: <b>{{Auth::user()->customer->first_name}} {{Auth::user()->customer->last_name}}</b></h2>
  </div>
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')
      @include('form_errors')

      <h3>Dane</h3> 

      <form action="/cms/user/{{$user->id}}" method='post'>
        @method('PUT')
        @csrf

      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text"  value="{{old('first_name',$user->customer->first_name)}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Imię</span>
          </label>

        </div>
      
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="last_name" type="text"  value="{{old('last_name',$user->customer->last_name)}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwisko</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
            <input id="input-1" class="input__field" name="email" type="text"  value="{{old('email',$user->email)}}">
            <label class="input__label input__label--haruki" for="input-1">
              <span class="input__label-content input__label-content--haruki">E-mail</span>
            </label>
  
          </div>

      </div>

     
  
      <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Zapisz</button>
        </div>
      </div>
    </form>






    </div>

  </div>
</div>



@stop

