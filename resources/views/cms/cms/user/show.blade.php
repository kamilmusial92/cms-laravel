@extends('layouts.cms')
@section('content')


<div class="dashboard-inside">

  <div class="dashboard-head">
    <h2>Użytkownik: <b>{{Auth::user()->customer->first_name}} {{Auth::user()->customer->last_name}}</b></h2>
  </div>
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')


      <h3>Dane</h3> <a href="/cms/user/{{$user->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>



      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text" disabled value="{{$user->customer->first_name}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Imię</span>
          </label>

        </div>
      
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="last_name" type="text" disabled value="{{$user->customer->last_name}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwisko</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
            <input id="input-1" class="input__field" name="email" type="text" disabled value="{{$user->email}}">
            <label class="input__label input__label--haruki" for="input-1">
              <span class="input__label-content input__label-content--haruki">E-mail</span>
            </label>
  
          </div>

      </div>

      <div class="row input-group">
          <h3>Hasło</h3> <a href="/cms/user/{{$user->id}}/editpassword " class="button-link waves-effect waves-light">Zmień hasło</a>

      </div>
  
      <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/dashboard' class="action-button active">Zakończ</a>
        </div>
      </div>
    






    </div>

  </div>
</div>


<script>
  $(document).ready(function () {

    $('#usun').click(function () {
      $('.modal-change-password').show();
    });

    $('.anuluj').click(function () {
      $('.modal-change-password').hide();
    });

  });
</script>

@stop

