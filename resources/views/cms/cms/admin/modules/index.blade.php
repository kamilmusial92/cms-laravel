   @extends('layouts.cms')
   @section('content')

 <div class="dashboard-inside">
    <div class='row shop_nav'>
        <div class="shop_nav__box">
            <a href="/cms/admin/modules/create">
              <div class="shop_nav__box--icon">
                  <img alt="icon" src="/src/img/icons/pencil.svg" />
              </div>
                <div class="shop_nav__box--text">
                    <p>Nowy moduł</p>
                </div>
            </a>
        </div>
    </div>
<div class='row'>

@include('cms/cms/admin.menu')


	<div class='col-md-10'>
    

   @include('notification')
    
    
 	  <div class="dashboard-column">
    <div class="dashboard-tab">
      <div class="tab-header"  data-toggle="collapse" data-target="#collapseTab1" aria-expanded="true" aria-controls="collapseTab1">
        <h2><span>Moduły</span></h2>
      </div>
      <div id="collapseTab1" class="collapse show">
        <table id="table_id">
              <thead >
              
                <tr >
                  <th>Nazwa</th>
               <th>Url</th>
                  <th>Opcje</th>
                
                </tr>
              </thead>
              <tbody>
  			@foreach($modules as $module)
         <tr >
  			<td>{{$module->name}}</td>
  			<td>{{$module->url}}</td>
  			<td><a href='/cms/admin/modules/{{$module->id}}' class='button-link waves-effect waves-light'>Opcje</a></td>
  			 </tr >
  			@endforeach
              </tbody>
              
            </table>
  </div>

 	 </div>
		</div>
	</div>

    </div>
  </div>
 	   @stop