@extends('layouts.cms')
@section('content')

<section class="row">
    <div class="row blog__head">
                            <div class="col-sm-12 col-md-6">
                               
                                <span>
                                    <a href="/cms/admin/users">Użytkownicy</a>
                                </span>
                                
                                 <span>
                                    {{$user->customer->first_name}} {{$user->customer->last_name}}
                                </span>
                            </div>
        
                            <div class="col-sm-12 col-md-6">
                                <h2>Użytkownik</h2>
                               
                          
                            </div>
                        </div>
            
                     
        
                    </section>
<div class="dashboard-inside">

  
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')


      <h3>Dane</h3> <a href="/cms/admin/users/{{$user->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>



      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text" disabled value="{{$user->customer->first_name}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Imię</span>
          </label>

        </div>
      
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="last_name" type="text" disabled value="{{$user->customer->last_name}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwisko</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
            <input id="input-1" class="input__field" name="email" type="text" disabled value="{{$user->email}}">
            <label class="input__label input__label--haruki" for="input-1">
              <span class="input__label-content input__label-content--haruki">E-mail</span>
            </label>
  
          </div>

      </div>

      <div class="row input-group">
          <h3>Hasło</h3> <a href="/cms/admin/users/{{$user->id}}/editpassword " class="button-link waves-effect waves-light">Zmień hasło</a>

      </div>
     
      <div class="row input-group">
        <h3>Przypisane moduły</h3> <a href="/cms/admin/users/{{$user->id}}/editmodules " class="button-link waves-effect waves-light">Zmień</a>
     
        <div class="question-box">
          <!---->
          <!---->
          <!---->
          <ul class="options">
              @foreach($modules as $module)


              <li class='col-md-2'>
              
                         
                
                 <i class='fa fa-check'></i> {{$module->module->name}}
                  <label> <span> </span></label>
              </li>

              @endforeach
          </ul>

          </div>    

      </div>

      <div class="row input-group">
        <h3>Logi</h3> <a href="/cms/admin/users/{{$user->id}}/logs " class="button-link waves-effect waves-light">Zobacz</a>
     
      </div>
  
      <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/admin/users' class="action-button active">Zakończ</a>
        </div>
      </div>
    
  <div class="button-container button-show">
        <div class="button-box">
          <a href='#' id='usun' class="action-button active">Usuń</a>
        </div>
      </div>

      <div class="modal-change-password modal-mask" style='display:none'>
                      <div class="modal-wrapper">
                        <div class="modal-container modal-contact-change-password">
                          <div class="anuluj close-box">
                            <span class="modal-close"></span>
                          </div>

                          <div class="modal-body">
                            <slot name="body">
                              <div class="Message-content">
                                <p>Potwierdź wybór</p>
                              </div>



                              <div class="column is-12 form-login">
                                <div class='col-md-12'>
                                <p class="delete-text"> Chcesz usunąć użytkownika?</p>
                                </div>
                                <div class='col-md-6'>
                                  <button class="anuluj modal-button button is-primary float-left" name="button" type="button">Anuluj</button>
                                </div>
                                <div class='col-md-6'>
                                  <form action='{{action('Cms\Cms\Admin\CmsAdminUsersController@destroy',$user->id)}}' method='post'>
                                    @method('DELETE')
                                    @csrf
                                    <button class="modal-button button is-primary">Potwierdź</button>
                                  </form>

                                </div>


                              </div>


                            </slot>
                          </div>

                        </div>
                      </div>
                    </div>



    </div>

  </div>
</div>


<script>
  $(document).ready(function () {

    $('#usun').click(function () {
      $('.modal-change-password').show();
    });

    $('.anuluj').click(function () {
      $('.modal-change-password').hide();
    });

  });
</script>

@stop

