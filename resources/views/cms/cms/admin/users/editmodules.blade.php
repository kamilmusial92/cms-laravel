@extends('layouts.cms')
   @section('content')
   
<div class="dashboard-inside">
<div class="dashboard-head">
            <h2>Przypisanie modułów: <b>{{$user->customer->first_name}} {{$user->customer->last_name}}</b></h2>
        </div>
        <div class="go-back right">
            <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
        </div>
        <div class="dashboard-inside">
            <div class="company-box">
           
               
<div class='question-container companyProfile col-xs-12 col-md-12'>
 
           
   @include('form_errors')  
   
              
  
   <form action="/cms/admin/users/{{$user->id}}/editmodules" method='post'>
    @csrf

          
      
                <h3>Moduły</h3>
                         <div class="row input-group" style='margin-bottom:20px;'>
                 <div class="question-box">
                        <!---->
                        <!---->
                        <!---->
                        <ul class="options vartiants-option">
                            <li ><label>Zaznacz wszystkie<input type='checkbox' id='zaznaczwszystkie'><span></span></label></li>

                        </ul>
                    </div>
                    
                </div>
            <div class="row input-group">   
        
            
                      <div class="question-box">
                       <!----> <!----> <!----> 
                       <ul class="options vartiants-option">
                       
                  
                        @foreach($modules as $module)


                      
                        <li class='col-sm-12 col-md-6 col-lg-3' >  
                           <span class="variants_checkboxes">
                         
                          <label><input class='checkboxmodule' name="checkboxmodule[]" type="checkbox" value="{{$module->id}}"
                             @if(is_array(old('checkboxmodule')) && in_array($module->id, old('checkboxmodule')))
                             checked 
                             @elseif(!is_array(old('checkboxmodule'))&&$module->chosenuser($user->id,$module->id)['module_id']==$module->id)
                                checked 
                            @endif> <span> </span></label>
                            </span>

                            {{$module->name}}
                     
                        </li>
                     
                    
                        @endforeach
                        </ul>


                         
                         </div>
                   
            </div>
               



         <div class="row input-group">   
                <div class="question-box"> 
    <div class="button-container">
        <div class=" button-box">
        <button type="submit" id='zapisz' name="button" class="action-button active" >Zapisz</button>
        </div>
    </div>
           </div>  
           </div>  
         
             </form>
         </div>
            </div>
        </div>
</div>

    <script>
$(document).ready(function () {

  

   $('#zaznaczwszystkie').click(function(){
     $("input[name='checkboxmodule[]']").not(this).prop('checked', this.checked);
  
  });

 

   $("input[name='checkboxframe[]").click(function(){

    if($(this).is(':checked'))
    {
       $(this) .attr('checked',true);
    }
    else{
          $(this) .attr('checked',false);
    }

   });
 
    $("input[name='checkboxmaterial[]").click(function(){

    if($(this).is(':checked'))
    {
       $(this) .attr('checked',true);
    }
    else{
          $(this) .attr('checked',false);
    }

   });

 });
    </script>

	  @stop