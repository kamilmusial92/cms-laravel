@extends('layouts.cms')
@section('content')


<div class="dashboard-inside">

  <div class="dashboard-head">
    <h2>Nowy użytkownik: </h2>
  </div>
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')
      @include('form_errors')

      <h3>Dane</h3> 

      <form action="/cms/admin/users" method='post'>
    
        @csrf

      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text"  value="{{old('first_name',$firstname)}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Imię</span>
          </label>

        </div>
      
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="last_name" type="text"  value="{{old('last_name',$lastname)}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwisko</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
            <input id="input-1" class="input__field" name="email" type="text"  value="{{old('email',$email)}}">
            <label class="input__label input__label--haruki" for="input-1">
              <span class="input__label-content input__label-content--haruki">E-mail</span>
            </label>
  
          </div>

      </div>


      <h3>Hasło</h3> 
     
         <div class="row input-group">

   
      
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="new_password" type="password"  value="{{old('new_password')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Hasło</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
            <input id="input-1" class="input__field" name="repeat_new_password" type="password"  value="{{old('repeat_new_password')}}">
            <label class="input__label input__label--haruki" for="input-1">
              <span class="input__label-content input__label-content--haruki">Powtórz hasło</span>
            </label>
  
          </div>

      </div>

      <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Zapisz</button>
        </div>
      </div>
    </form>






    </div>

  </div>
</div>



@stop

