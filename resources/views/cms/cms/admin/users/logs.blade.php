@extends('layouts.cms')
@section('content')

<section class="row">
        <div class="row blog__head">
                                <div class="col-sm-12 col-md-6">
                                   
                                    <span>
                                        <a href="/cms/admin/users/{{$id}}">{{$user->customer->first_name}} {{$user->customer->last_name}}</a>
                                    </span>
                                    
                                     <span>
                                            <a href="/cms/admin/users/{{$id}}/logs">Logi</a>
                                    </span>
                                    <span>
                                        Logi {{$category->name}}
                                    </span>
                                </div>
            
                                <div class="col-sm-12 col-md-6">
                                    <h2>Logi </h2>
                                   <p>{{$category->name}}</p>
                              
                                </div>
                            </div>
                
                         
            
                        </section>

<div class="dashboard-inside">

<div class='row'>

@include('cms/cms/admin.menucategorylogs')


 <div class='col-md-10'>
 

@include('notification')
 
 
    <div class="dashboard-column">
 <div class="dashboard-tab">
   <div class="tab-header"  data-toggle="collapse" data-target="#collapseTab1" aria-expanded="true" aria-controls="collapseTab1">
     <h2><span>Logi modułu {{$category->name}}</span></h2>
   </div>
   <div id="collapseTab1" class="collapse show">
     <table id="table_id">
           <thead >
           
             <tr >
               <th>Treść</th>
           
               <th>Data</th>
             
             </tr>
           </thead>
           <tbody>
           @foreach($logs as $log)
      <tr >
           <td>{{$log->text}}</td>
           <td>{{$log->created_at}}</td>
        
            </tr >
           @endforeach
           </tbody>
           
         </table>
</div>

   </div>
     </div>
 </div>

 </div>
</div>
<script>
        $(document).ready(function () {
            $('#table_id').DataTable({
               responsive: {
                details: false

              },
               "order": [[ 1, "desc" ]],
              columnDefs: [
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: -1 }
              ],
                language: {
                    searchPlaceholder: "szukaj...",
                    "processing": "Przetwarzanie...",
                    "search": "",
                    "lengthMenu": "Pokazuj _MENU_",
                    "info": "Pokazuje od _START_ do _END_ z _TOTAL_ pozycji",
                    "infoEmpty": "Pozycji 0 z 0 dostępnych",
                    "infoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                    "infoPostFix": "",
                    "loadingRecords": "Wczytywanie...",
                    "zeroRecords": "Nie znaleziono pasujących pozycji",
                    "emptyTable": "Brak danych",
                    "paginate": {
                        "first": "Pierwsza",
                        "previous": "<",
                        "next": ">",
                        "last": "Ostatnia"
                    },
                    aria: {
                        paginate: {
                            "sortAscending": ": aktywuj, by posortować kolumnę rosnąco",
                            "sortDescending": ": aktywuj, by posortować kolumnę malejąco",
                            previous: '<',
                            next: '>'
                        }
                    }
                }
            });
        });
    </script>
    
@stop