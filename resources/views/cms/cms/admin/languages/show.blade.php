@extends('layouts.cms')
@section('content')


<div class="dashboard-inside">

  <div class="dashboard-head">
    <h2>Wersja językowa: <b>{{$lang->name}}</b></h2>
  </div>
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')


      <h3>Dane</h3> <a href="/cms/admin/languages/{{$lang->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>



      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="name" type="text" disabled value="{{$lang->name}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwa</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="url" type="text" disabled value="{{$lang->url}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Url</span>
          </label>

        </div>
      
   

      

      </div>

      <h3>Ikona</h3> <a href="/cms/admin/languages/{{$lang->id}}/edit/2" class="button-link waves-effect waves-light">Zmień</a>
   <div class="row input-group">
       <div class='col-xs-2 col-sm-2 col-md-2 col-lg-2 gallery-show-image-box'>
                                <a data-fancybox="gallery" href="/storage/cms/languages/{{$lang->src}}"><img src="/storage/cms/languages/{{$lang->src}}"
                                        class="img-thumbnail hover-shadow"></a>
                            </div>
   </div>
     
  
      <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/admin/languages' class="action-button active">Zakończ</a>
        </div>
      </div>
    
  <div class="button-container button-show">
        <div class="button-box">
          <a href='#' id='usun' class="action-button active">Usuń</a>
        </div>
      </div>

      <div class="modal-change-password modal-mask" style='display:none'>
                      <div class="modal-wrapper">
                        <div class="modal-container modal-contact-change-password">
                          <div class="anuluj close-box">
                            <span class="modal-close"></span>
                          </div>

                          <div class="modal-body">
                            <slot name="body">
                              <div class="Message-content">
                                <p>Potwierdź wybór</p>
                              </div>



                              <div class="column is-12 form-login">
                                <div class='col-md-12'>
                                <p class="delete-text"> Chcesz usunąć wersję językową?</p>
                                </div>
                                <div class='col-md-6'>
                                  <button class="anuluj modal-button button is-primary float-left" name="button" type="button">Anuluj</button>
                                </div>
                                <div class='col-md-6'>
                                  <form action='{{action('Cms\Cms\Admin\CmsAdminLanguagesController@destroy',$lang->id)}}' method='post'>
                                    @method('DELETE')
                                    @csrf
                                    <button class="modal-button button is-primary">Potwierdź</button>
                                  </form>

                                </div>


                              </div>


                            </slot>
                          </div>

                        </div>
                      </div>
                    </div>



    </div>

  </div>
</div>


<script>
  $(document).ready(function () {

    $('#usun').click(function () {
      $('.modal-change-password').show();
    });

    $('.anuluj').click(function () {
      $('.modal-change-password').hide();
    });

  });
</script>

@stop

