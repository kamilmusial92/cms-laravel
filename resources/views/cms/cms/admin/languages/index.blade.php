   @extends('layouts.cms')
   @section('content')

 <div class="dashboard-inside">
    <div class='row shop_nav'>
        <div class="shop_nav__box">
            <a href="/cms/admin/languages/create">
              <div class="shop_nav__box--icon">
                  <img alt="icon" src="/src/img/icons/pencil.svg" />
              </div>
                <div class="shop_nav__box--text">
                    <p>Nowa wersja językowa</p>
                </div>
            </a>
        </div>
    </div>
<div class='row'>

@include('cms/cms/admin.menu')


	<div class='col-md-10'>
    

   @include('notification')
    
    
 	  <div class="dashboard-column">
    <div class="dashboard-tab">
      <div class="tab-header"  data-toggle="collapse" data-target="#collapseTab1" aria-expanded="true" aria-controls="collapseTab1">
        <h2><span>Wersje językowe</span></h2>
      </div>
      <div id="collapseTab1" class="collapse show">
        <table id="table_id">
              <thead >
              
                <tr >
                  <th>Nazwa</th>
               <th>Url</th>
               <th>Ikona</th>
                  <th>Opcje</th>
                
                </tr>
              </thead>
              <tbody>
  			@foreach($languages as $lang)
         <tr >
  			<td>{{$lang->name}}</td>
  			<td>{{$lang->url}}</td>
        <td><img src="/storage/cms/languages/{{$lang->src}}"></td>
  			<td><a href='/cms/admin/languages/{{$lang->id}}' class='button-link waves-effect waves-light'>Opcje</a></td>
  			 </tr >
  			@endforeach
              </tbody>
              
            </table>
  </div>

 	 </div>
		</div>
	</div>

    </div>
  </div>
 	   @stop