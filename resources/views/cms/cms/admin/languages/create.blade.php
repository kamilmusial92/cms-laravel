@extends('layouts.cms')
@section('content')


<div class="dashboard-inside">

  <div class="dashboard-head">
    <h2>Nowa wersja językowa </h2>
  </div>
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')
      @include('form_errors')

      <h3>Dane</h3> 

      <form action="/cms/admin/languages" method='post'>
    
        @csrf

      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="name" type="text"  value="{{old('name')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwa</span>
          </label>

        </div>
      
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="url" type="text"  value="{{old('url')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Url</span>
          </label>

        </div>
  

   
      </div>



      <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Zapisz</button>
        </div>
      </div>


    </form>

  <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/admin/languages'  class="action-button active">Anuluj</a>
        </div>
      </div>




    </div>

  </div>
</div>



@stop

