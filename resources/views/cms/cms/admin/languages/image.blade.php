@extends('layouts.cms')
@section('content')


<div class="dashboard-inside">

    <div class="dashboard-head">
        <h2>Wersja językowa: <b>{{$lang->name}}</b></h2>
    </div>
    <div class="go-back right">
        <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
    </div>
    <div class="dashboard-inside">
        <div class="company-box">


            <h3>Ikona</h3>
            @include('form_errors')

            <ul class="sortable row">
               @if($lang->src!='')
                <li class='col-xs-12 col-sm-6 col-md-4 col-lg-3 propduct-img-box' >
                    <input type='hidden' name='' value='{{$lang->id}}'>
                    <a data-fancybox="gallery" href="/storage/cms/languages/{{$lang->src}}"><img src="/storage/cms/languages/{{$lang->src}}" class=" hover-shadow" style='width:100%'>
                    </a> <button class="product-img-button">Usuń</button>
                </li>
             @endif
            </ul>

          
            @include('notification')
            <form action="/cms/admin/languages/{{$lang->id}}/edit/2" method='post' enctype="multipart/form-data">
                @csrf
                <div class="row input-group">



                    <div class="dropFile">
                        <div class="wrapper">
                            <div class="drop">
                                <div class="cont">

                                    <div class="tit">

                                    </div>
                                    <div class="desc">
                                        Upuść plik nad typ polem <br> lub
                                    </div>
                                    <div class="browse">
                                        wybierz plik
                                    </div>
                                </div>
                                <output id="list"></output>
                                <input id="files"  name="files" type="file" />
                            </div>
                        </div>
                    </div>


                </div>



                <div class="button-container">
                    <div class=" button-box">
                        <button type="submit" name="button" class="button-link waves-effect waves-light">Wgraj</button>
                    </div>
                </div>


            </form>





            <div class="button-container">
                <div class=" button-box">
                    <a class="action-button active" href='/cms/admin/languages/{{$lang->id}}'>Zapisz</a>
                </div>
            </div>





        </div>

    </div>
</div>

<script>
$(document).ready(function(){

    $('.product-img-button').click(function(){

        image=$(this).closest('li').find('input').val();
        $(this).closest('li').remove();

    var _token=$('meta[name="csrf-token"]').attr('content');
                 
                $.ajax({
                    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
                    url: '{{ route('removelanguageimage',['id'=>$lang->id]) }}',
                    type: 'POST',
                   
                    data: {
                        
                       image:image,
                        token:_token
                    },
                     success: function(html){

                    }

                });

 });


});

</script>


@stop