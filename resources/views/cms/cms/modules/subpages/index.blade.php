@extends('layouts.cms')
@section('content')


            <section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                                Podstrony
                            </span>
                            
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Podstrony</h2>
                            Wersje językowe
                            <form action='' method='post'>
                            <select name='lang' id='lang'>

                                <option value='{{$mainlang->url}}'>{{$mainlang->name}}</option>
                              
                                @foreach($languages as $language)
                              
                                @if($mainlang->id!=$language->id)
                                <option value='{{$language->url}}'>{{$language->name}}</option>
                                @endif

                                @endforeach
                            </select>
                            </form>
                        </div>
                    </div>

           <div class="col-sm-12 col-md-12">
           
    <div class='row shop_nav'>
     

      

        <div class="shop_nav__box">
            <a href="/cms/module/subpages/{{$mainlang->url}}/create">
              <div class="shop_nav__box--icon">
                  <img alt="icon" src="/img/icons/pencil.svg" />
              </div>
                <div class="shop_nav__box--text">
                    <p>Nowa podstrona</p>
                </div>
            </a>

        </div>

        

    </div>



                    </div>
</section>
                    <div class="col-sm-12 col-md-12">
                         @include('notification')
                        <div class="dd">
                            <ol class="sortable dd-list">
                               
                               @foreach($menus as $menu)
                                <li class="dd-item" data-id="1" id='{{$menu->id}}'>
                                    <div class="item-box">
                                        <div class="block__list">
                                            <div class="block__handle">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <div class="block__box dd-nodrag">
                                                <h2>{{$menu->name}}</h2>
                                                <p>{{$menu->url}}</p>
                                            </div>
                                            <div class="block__menu dd-nodrag">
                                                 <a title='Edytuj dane podstrony' class="icon-settings" href="/cms/module/subpages/{{$mainlang->url}}/{{$menu->id}}/edit">
                                                    <img alt="menu icon" src="/img/icons/pencil.svg" />
                                                </a>
                                                <a title='Pozycjonowanie' class="icon-settings" href="/cms/module/subpages/{{$mainlang->url}}/{{$menu->id}}/editseo">
                                                    <img alt="menu icon" src="/img/icons/list.svg" />
                                                </a>
                                                <!--
                                                <a title='Wybierz szablon' class="icon-settings" href="/cms/module/subpages/{{$mainlang->url}}/{{$menu->id}}/choosetemplate">
                                                    <img alt="menu icon" src="/img/icons/icon-puzzle.svg" />
                                                </a>-->
                                                
                                                <a title='Komponenty' class="icon-settings" href="/cms/module/subpages/{{$mainlang->url}}/{{$menu->id}}/components/list">
                                                    <img alt="menu icon" src="/img/icons/icon-settings.svg" />
                                                </a>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                              
                            </ol>

                            <script>
var arraysort = [];

                        $(".sortable").sortable({
                            stop: function() {
                                $.map($(this).find('li'), function(el) {
                                    var id = el.id;
                                    var sorting = $(el).index();
                            arraysort.push({'id':id,'sorting':sorting});
                                });
                                          var _token=$('meta[name="csrf-token"]').attr('content');
                                    $.ajax({
                                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                                        url: '{{ route('SubpagePLSort',['lang'=>$mainlang->url]) }}',
                                        type: 'POST',
                                        data: {
                                            id: arraysort,
                                           
                                            token:_token
                                        },
                                        
                                    });
                            }
                        });
                    
                    
                        </script>

                        </div>
                    </div>
    
                
<script>
document.getElementById('lang').onchange = function() {
    window.location = "/cms/module/subpages/" + this.value;
};
</script>
@stop