@extends('layouts.cms')
@section('content')

<section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                            <a href="/cms/module/subpages/{{$mainlang->url}}">Podstrony</a>
                            </span>
                            <span>
                                {{$subpage->name}}
                            </span>
                            <span>
                                <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/list"> Komponenty</a>
                            </span>
                             <span>
                                {{$component->name}}
                            </span>
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Komponent</h2>
                            <p>{{$component->name}}</p>
                      
                        </div>
                    </div>
        
                 
    
                </section>

<div class="dashboard-inside">

  
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>

    <div class="company-box">


      @include('notification')
      @include('form_errors')



      <h3>Nazwa komponentu</h3>    <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
     
      <div class="row input-group">


          <div class="input-box col-xs-12 col-md-4">
            <input id="input-1" class="input__field" name="name" type="text" disabled value="{{old('name',$component->name)}}">
  
          
            <label class="input__label input__label--haruki" for="input-1">
              <span class="input__label-content input__label-content--haruki">Nazwa komponentu</span>
            </label>
  
          </div>
  
  
     
        </div>


        @if($templatecomponent->background_status==1)
      <h3>Tło</h3>  
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
       <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif
     

      <div class="row input__row">
        
        @if($component->background!=0)
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gallery-show-image-box">
          <a data-fancybox="gallery" href="/{{$component->image($component->background)->file_path.$component->image($component->background)->file_name}}">
            <img  src="/{{$component->image($component->background)->file_path.$component->image($component->background)->file_name}}" class="img-thumbnail hover-shadow">
          </a>
                            </div>
                            @endif


                           


  
      </div>
      
      @endif

      @if($templatecomponent->url_status==1)
      <h3>Url</h3>    
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
     
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif

        <div class="row input-group">


        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="url" type="text" disabled value="{{old('url',$component->url)}}">

        
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Url</span>
          </label>

        </div>

   
      </div>
      @endif

       @if($templatecomponent->title_status==1)
      <h3>Tytuł</h3>   
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
     
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif

        <div class="row input-group">


        <div class="input-box col-xs-12 col-md-9">
          <input id="input-1" class="input__field" name="title" type="text" disabled  value="{{old('title',$component->title)}}">

        
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Tytuł</span>
          </label>

        </div>

   
      </div>
      @endif

        @if($templatecomponent->subtitle_status==1)
      <h3>Podtytuł</h3>  
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
     
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif

        <div class="row input-group">


        <div class="input-box col-xs-12 col-md-9">
          <input id="input-1" class="input__field" name="subtitle" type="text" disabled   value="{{old('subtitle',$component->subtitle)}}">

        
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Podtytuł</span>
          </label>

        </div>

   
      </div>
      @endif


      @if($templatecomponent->header_status==1)
      <h3>Nagłówek</h3>  
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
     
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif
     

      <div class="row input-group">


               
      <div class="input-box col-xs-12 col-md-12">
           
 {!!$component->header!!}
        
 
                    
                    </div>
              

   
      </div>

      @endif

            @if($templatecomponent->text_status==1)
      <h3>Tekst</h3> 
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
     
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif
     

      <div class="row input-group">


               
      <div class="input-box col-xs-12 col-md-12">
           
 {!!$component->text!!}
        
 
                    
                    </div>
              

   
      </div>

      @endif


         @if($templatecomponent->image_status==1)
      <h3>Zdjęcie</h3>   
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif
     

      <div class="row input__row">
      
        @if($component->image!=0)
          <div  class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gallery-show-image-box">
                                <a data-fancybox="gallery" href="/{{$component->image($component->image)->file_path.$component->image($component->image)->file_name}}"><img  src="/{{$component->image($component->image)->file_path.$component->image($component->image)->file_name}}" class="img-thumbnail hover-shadow"></a>
                            </div>
                            @endif



  
      </div>
      
      @endif


      @if($templatecomponent->gallery_status==1)
      <h3>Galeria</h3>  
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
     
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif
     

      <div class="row input__row">
        
       
      @foreach($component->GalleryImage($component->gallery) as $galleryimage)
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gallery-show-image-box">
            <a data-fancybox="gallery" href="/{{$galleryimage->file_path.$galleryimage->file_name}}"><img  src="/{{$galleryimage->file_path.$galleryimage->file_name}}" class="img-thumbnail hover-shadow"></a>
          </div>
      @endforeach


                            

                     
                

  
      </div>
      
      @endif

   @if($templatecomponent->file_status==1)
      <h3>Pliki do pobrania</h3>   
      @if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
     
      <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
      @endif
       <div class="row input__row">
       
       
    
                         
       <div class='listoffiles row'>
    @foreach($chosenfiles as $chosenfile)

        <div class="filed col-md-2"><div class="gallery__photos"><div class="gallery__photos--files"><i class="fa fa-file-alt" style="font-size: 6em;"></i></div><div class="gallery__photos--icons"></div><div class="gallery__photos--text"><input type="text" name="changePicNameInput" class="" id="changePicNameInput" value="{{$chosenfile->file->file_name}}" disabled></div></div></div>

    @endforeach
       </div>



  
      </div>

 
      
      @endif

    <h3>Zapisuje zmiany do wszystkich skopiowanych komponentów</h3>  <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
     <div class="row input-group">
    
    <div class='question-container companyProfile col-xs-12 col-md-3'>

                                <div class="question-box">
                                    <!---->
                                    <!---->
                                    <!---->
                                  
                                        <ul class="options">
                                            @if($component->copyable==0)
                                            
                                            <li><label><input name="status" type="radio" checked disabled value="0"> <span> Nie
                                            </span></label></li>
                                            @else
                                            <li><label><input name="status" type="radio" checked disabled value="1"> <span> Tak
                                                    </span></label></li>
                                         
                                            @endif
                                        </ul>

                                </div>
                            </div>
     </div>

        <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/list'  class="action-button active">Zapisz</a>
        </div>
      </div>

  </div>

  </div>

@stop