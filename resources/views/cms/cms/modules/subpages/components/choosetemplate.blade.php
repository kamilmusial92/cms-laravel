


@extends('layouts.cms')
@section('content')

          <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                            <a href='/cms/module/subpages/{{$mainlang->url}}'>Podstrony</a>
                            </span>
                            <span>
                               {{$subpage->name}}
                            </span>
                            <span>
                              <a href='/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/list'>Komponenty</a>
                              </span>
                            <span>
                             Nowy komponent
                            </span>
                            
                           
                        </div>
     <div class="col-sm-12 col-md-6">
                            <h2>Nowy komponent</h2>
                        </div>
                      
                    </div>

   
                   </section>

<div class="dashboard-inside">

  
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>


    <div class="company-box">


      @include('notification')
      @include('form_errors')


      <h3>Wybierz szablon</h3>
    
      <div class="row input-group">
     @if(!empty($main))
     <form action="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$main}}/chosentemplate" method='post'>
    @else

      <form action="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/chosentemplate" method='post'>
    
     @endif
        @csrf
           <div class='col-sm-3 col-md-3 input__row--box'>
                              <div class="custom-select">
                                  <select id='chosen' name='chosen'>
                                    
                                    <option value="0">Wybierz szablon:</option>
                                   @foreach($templates as $chosetemplate)
                                     <option value='{{$chosetemplate->id}}'>{{$chosetemplate->name}}</option>
                                    @endforeach
                                  </select>
                              </div>
                                 <span></span>
                          </div>
   
   
        
      </form>


      </div>
      
      @if(!empty($components))

   <div class='question-container companyProfile col-xs-12 col-md-12'>
    @include('notification')

     <div class="question-box">
                        <!---->
                        <!---->
                        <!---->
                        <ul class="options vartiants-option">
                            <li ><label>Zaznacz wszystkie<input type='checkbox' id='zaznaczwszystkie'><span></span></label></li>

                        </ul>
                    </div>
  @if(!empty($main))
  <form action="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$main}}/copytemplate" method='post'>
  @else

   <form action="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/copytemplate" method='post'>
    @endif

    @csrf
         <div class="question-box">
                       <!----> <!----> <!----> 
                       <ul class="options vartiants-option">
                           <div class="dd">
                            <ol class="dd-list">
                               
                               @foreach($components as $component)
                                <li class="dd-item" data-id="1" >
                                   
                                    <div class="item-box">
                                        <div class="block__list">
                                             <span class="variants_checkboxes">
                         
                          <label><input class='checkboxmodule' name="checkboxmodule[]" type="checkbox" value="{{$component->id}}"> <span> </span></label>
                            </span>
                                            <div class="block__box dd-nodrag">
                                                <h2><b>{{$component->name}}</b> 
                                                    @if($component->copied_template_id!=0)
                                                    skopiowane z <b>{{$component->template($component->copied_template_id)->name}}</b>
                                                    @endif
                                                </h2>
                                                <p>Boxy: {{$component->chosenBoxesComponent($component->id)}} </p>
                                            
                                            </div>
                                            <div class="block__menu dd-nodrag">
                                                

                                               
                                               
                                            </div>
                                             <input type='text' name='count'>Ilość
                                        </div>

                                    </div>
                                </li>
                                @endforeach
                              
                            </ol>
                       
                    
                        </div>
                        </ul>

                    </div>
                     @if(empty($main))
     <div class='question-container companyProfile col-xs-12 col-md-12'>
  

     <div class="question-box">
                        <!---->
                        <!---->
                        <!---->
                        <ul class="options vartiants-option">
                            <li ><label>Z ułożoną kolejnością<input type='checkbox' checked name='withsort'><span></span></label></li>

                        </ul>
                    </div>
    </div>
    @endif
                       <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Skopiuj</button>
        </div>
      </div>
                  </form>
                </div>
@endif


    </div>

  </div>

        <script>
$(document).ready(function () {



   $('#zaznaczwszystkie').click(function(){
     $("input[name='checkboxmodule[]']").not(this).prop('checked', this.checked);
  
  });




var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {

      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      h = this.parentNode.previousSibling;
      for (i = 0; i < s.length; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          for (k = 0; k < y.length; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
      //pprzeładowanie strony
         $(this).closest('form').submit();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {

 
 

    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
   



    });
    </script>

@stop

