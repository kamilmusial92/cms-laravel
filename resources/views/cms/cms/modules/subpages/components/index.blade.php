@extends('layouts.cms')
@section('content')

<section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                            <a href="/cms/module/subpages/{{$mainlang->url}}">Podstrony</a>
                            </span>
                            <span>
                                {{$subpage->name}}
                            </span>
                            <span>
                                Komponenty
                            </span>
                            
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Komponenty</h2>
                      
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-12">
           
    <div class='row shop_nav'>
     
            <div class="shop_nav__box">
                    <a href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/choosetemplate">
                      <div class="shop_nav__box--icon">
                          <img alt="icon" src="/img/icons/pencil.svg" />
                      </div>
                        <div class="shop_nav__box--text">
                            <p>Nowy Komponent</p>
                        </div>
                    </a>
        
                </div>

            <div class="shop_nav__box">
            <a class='active' href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/choosecomponent">
              <div class="shop_nav__box--icon">
                  <img alt="icon" src="/img/icons/icon-puzzle.svg" />
              </div>
                <div class="shop_nav__box--text">
                    <p>Skopiuj uzupełnione komponenty</p>
                </div>
            </a>

        </div>

        

    </div>



                    </div>
                        <div class="col-sm-12 col-md-12">

                            @include('notification')
                           <div class="dd">

                            <ol class="sortable dd-list">
                               
                               @foreach($components as $component)
                               
                                @if(Auth::user()->admincms==1 or $component->template_module->user_visible==1)
                                    @if($component->status==0)
                                    <li class="dd-item"  data-id="1" id='{{$component->id}}'>
                                     @elseif($component->template_module->user_visible==1)
                                    <li class="dd-item" style='display:none;' data-id="1" id='{{$component->id}}'>
                                    @else
                                     <li class="dd-cancel dd-item" data-id="1" id='{{$component->id}}'>
                                    @endif
                                @else


                                <li class="ui-state-disabled dd-cancel" data-id="1" id='{{$component->id}}'>
                                        @endif
                                    <div class="item-box">
                                        <div class="block__list">
                                                @if(Auth::user()->admincms==1 or $component->template_module->user_visible==1)
                                            <div class="block__handle">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                            @endif
                                            <div class="block__box dd-nodrag">
                                                    @if($component->template_module->user_visible==1)
                                                <h2><b><i>{{$component->name}}</i></b> z szablonu komponentu <b>{{$component->template_module->name}}</b></h2>
                                                 <p>{!!$component->chosenBoxesComponent($component->template_component_id,$component->id)!!} </p>
                                                    @else
                                                    <h2><b>{{$component->name}}</b></h2>

                                                @endif

                                            </div>
                                            <div class="block__menu dd-nodrag">
                                                
                                                
                                                
                                                @if(Auth::user()->admincms==1 or $component->template_module->user_visible==1)
                                                    <div class="icon-settings gallery__item--delete">
                                                            <a class='usunkomponent' href="#" title="usuń">
                                                               <i class="fa fa-trash" ></i>
                                                            </a>
                                                        </div>
                                                       
                                                        @endif
                                        @if($component->template_module->user_visible==0)
                                           
                                                            <a  class="icon-settings" href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/choosetemplate" title="dodaj komponent">
                                                             <img alt="menu icon" src="/img/icons/pencil.svg" />
                                                            </a>
                                                        

                                                        
                                                            <a class="icon-settings" href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}/choosecomponent" title="dodaj uzupełniony komponent">
                                                              <img alt="menu icon" src="/img/icons/icon-puzzle.svg" />
                                                            </a>
                                                     
                                                 <div class="show-list block__handle">
                                                <i class="fa fa-chevron-down"></i>
                                               
                                                    </div>
                                                    @endif
                                               

                                            @if($component->template_module->user_visible==1)
                                               
                                            <a class="icon-settings" href="/cms/module/subpages/{{$mainlang->url}}/{{$subpage->id}}/components/{{$component->id}}">
                                                    <img alt="menu icon" src="/img/icons/icon-settings.svg" />
                                                </a>
                                            @endif   
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-change-password modal-mask" style='display:none'>
                                            <div class="modal-wrapper">
                                                <div class="modal-container modal-contact-change-password">
                                                    <div class="anuluj close-box">
                                                        <span class="modal-close"></span>
                                                    </div>
                    
                                                    <div class="modal-body">
                                                        <slot name="body">
                                                            
                                                            <div class="Message-content">
                                                                <p>Potwierdź wybór</p>
                                                            </div>
                    
                                                            <div class="column is-12 form-login">
                                                           
                                                                    <p class="delete-text">Chcesz usunąć komponent?</p>
                                                           
                    
                                                                <div class='col-md-6'>
                                                                    <button class="anuluj modal-button button is-primary float-left" name="button"
                                                                        type="button">Anuluj</button>
                                                                </div>
                                                                <div class='col-md-6'>
                                                                    <form id='removecomponent' action='{{action('Cms\Cms\Modules\CmsComponentsModuleController@destroy',['lang'=>$mainlang->url,'subpageid'=>$subpage->id,'id'=>$component->id])}}'
                                                                        method='post'>
                                                                        @method('DELETE')
                                                                        @csrf
                                                                        <button class="modal-button button is-primary">Potwierdź</button>
                                                                    </form>
                                                                </div>
                    
                    
                                                            </div>
                    
                    
                                                        </slot>
                                                    </div>
                    
                                                </div>
                                            </div>
                                        </div>
                                </li>
                         
                               
                             
                
                                @endforeach
                              
                            </ol>

  @if(!empty($new))
                            <script>
                             $(".sortable li").each(function(){
                                if($(this).attr('id')=={{$new}})
                                {
                                       $(this).find('i').attr('class','fa fa-chevron-up');
                                        $(this).closest('li').nextUntil('.dd-cancel').slideDown('fast');
                                }
                             });
                            </script>
                         @endif

                            <script>
 $( ".dd-cancel" ).sortable({
      cancel: ".ui-state-disabled"
    });
var arraysort = [];
                                    $(".sortable").sortable({
                                        stop: function() {
                                            $.map($(this).find('li'), function(el) {
                                                var id = el.id;
                                                var sorting = $(el).index();
                                                 arraysort.push({'id':id,'sorting':sorting});
                                            
                                            });
                                                 var _token=$('meta[name="csrf-token"]').attr('content');
                                                $.ajax({
                                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                                    url: '{{ route('ComponentPLSort',['lang'=>$mainlang->url,'subpageid'=>$subpage->id]) }}',
                                                    type: 'POST',
                                                    data: {
                                                        id: arraysort,
                                                       
                                                        token:_token
                                                    },
                                                    
                                                });
                                        }
                                    });
                                 
                                      $('.show-list').click(function(){
                                        
                                   if($(this).closest('li').nextUntil('.dd-cancel').css('display') == 'none')
                                   {
                                        $(this).find('i').attr('class','fa fa-chevron-up');
                                        $(this).closest('li').nextUntil('.dd-cancel').slideDown('fast');
                                   }
                                   else{
                                        $(this).find('i').attr('class','fa fa-chevron-down');
                                   // $(this).closest('li').nextUntil('.dd-cancel').css('display','none');
                                       $(this).closest('li').nextUntil('.dd-cancel').slideUp('fast');
                                   }
                                    

                                   

                                    
                                });


                                
                                    </script>
                        </div>
                        
                    </div>
    
                </section>
             

            

                <script>
                        $(document).ready(function () {
                
                            $('.usunkomponent').click(function () {
                                $(this).closest('li').find('.modal-change-password').show();
                              

                            });
                
                            $('.anuluj').click(function () {
                                $('.modal-change-password').hide();
                            });
                
                        });
                    </script>


@stop