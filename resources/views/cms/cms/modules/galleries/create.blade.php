@extends('layouts.cms')
@section('content')

<section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                            <a href="/cms/module/galleries">Galerie</a>
                            </span>
                            <span>
                                Nowa galeria
                            </span>
                            
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Nowa galeria</h2>
                           
                      
                        </div>
                    </div>
        
                 
    
                </section>
<div class="dashboard-inside">


     <div class="go-back right">
         <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
     </div>
     <div class="dashboard-inside">
         <div class="company-box">
        
            


       @include('form_errors')
             <h3>Dane</h3>
           <form action="/cms/module/galleries" method='post'>
                 @csrf
         
         
             <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-4">
                     <input id="input-1" class="input__field" name="name"  type="text" value="{{old('name')}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Nazwa</span>
                     </label>
                 
                 </div>
    
               


             </div>



               <div class="button-container">
     <div class=" button-box">
     <button type="submit" name="button" class="action-button active" >Zapisz</button>
     </div>
 </div>
          </form>
      
         </div>
    
     </div>
 </div>


@stop