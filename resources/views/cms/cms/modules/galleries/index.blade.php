@extends('layouts.cms')
@section('content')

            <section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                                Galerie
                            </span>
                            
                        </div>
    
                      
                    </div>

           <div class="col-sm-12 col-md-12">
           
    <div class='row shop_nav'>
     

      

        <div class="shop_nav__box">
            <a href="{{url('cms/module/galleries/create')}}">
              <div class="shop_nav__box--icon">
                  <img alt="icon" src="/img/icons/pencil.svg" />
              </div>
                <div class="shop_nav__box--text">
                    <p>Nowa galeria</p>
                </div>
            </a>

        </div>

        

    </div>



                    </div>
</section>

 <div class="dashboard-inside">
  <div class="dashboard-column">
   
    @include('notification')
    <div class="dashboard-tab">
  <div class="row gallery__tab">
                        <div class="col-sm-12">
                            <div class="gallery__tab--head">
                                <i class="icon gallery"></i>
                                <h2>Galerie</h2>
                            </div>
              
                            @foreach($list as $gallery)
                                <div class="gallery__tab--content">
                          
                                <div class="gallery__item">
                                     
                                    <div class="gallery__item--image">
                                          <a href="/cms/module/galleries/{{ $gallery->id}}/images">
                                       @if($gallery->firstpicture($gallery->id)['file_path'].$gallery->firstpicture($gallery->id)['file_name']!='')

                                        <img src='/{{$gallery->firstpicture($gallery->id)['file_path'].$gallery->firstpicture($gallery->id)['file_name']}}'>
                                        @endif    
                                    </a>
                                    </div>

                                    <div class="gallery__item--text">
                                          <a href="/cms/module/galleries/{{ $gallery->id}}/images">
                                        <h3><b>{{ $gallery->name}} </b></h3>
                                        
                                       </a>
                                    </div>
           
                                    <div class="gallery__item--delete">
                                       
                                        <a href="/cms/module/galleries/{{$gallery->id}}/delete"  title='usuń galerię'>
                                            <span></span>
                                            <span></span>
                                        </a>
                                    </div>
             
                                </div>
                             
                            </div>
                                
                            @endforeach
                       
                        </div>
    
                    </div>
    </div>
  </div>
</div>

   


  @stop