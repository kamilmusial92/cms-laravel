@extends('layouts.cms')
@section('content')

          <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/templates'>Szablony</a>
                            </span>
                             <span>
                               {{$template->name}}
                            </span>
                            
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Szablon </h2><p>{{$template->name}}</p>
                        </div>
                    </div>

   
                   </section>

<div class="dashboard-inside">

  
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')


      <h3>Dane</h3> <a href="/cms/module/templates/{{$template->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>



      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text" disabled value="{{$template->name}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwa</span>
          </label>

        </div>
      
    

      </div>

     
      <div class="row input-group">
        <h3>Pozycjonowanie</h3> <a href="/cms/module/templates/{{$template->id}}/edit " class="button-link waves-effect waves-light">Zmień</a>

     <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text" disabled value="{{$template->title_page}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Tytuł strony</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text" disabled value="{{$template->description_page}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Opis strony</span>
          </label>

        </div>
    
         <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="first_name" type="text" disabled value="{{$template->keywords_page}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Słowa kluczowe</span>
          </label>

        </div>

      </div>
       

      </div>

          <div class="row input-group">
        <h3>Pozycjonowanie - OG Image</h3> <a href="/cms/module/templates/{{$template->id}}/edit " class="button-link waves-effect waves-light">Zmień</a>
        
      
        @if($template->image_page!=0)
          <div  class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gallery-show-image-box">
                                <a data-fancybox="gallery" href="/{{$template->image($template->image_page)->file_path.$template->image($template->image_page)->file_name}}"><img  src="/{{$template->image($template->image_page)->file_path.$template->image($template->image_page)->file_name}}" class="img-thumbnail hover-shadow"></a>
                            </div>
                            @endif



  
        </div>     
      <div class="row input-group">
        <h3>Komponenty</h3> <a href="/cms/module/templates/{{$template->id}}/components " class="button-link waves-effect waves-light">Zmień</a>
    
           <div class="dd">
                            <ol class="dd-list">
                               
                               @foreach($components as $component)
                                <li class="dd-item" data-id="1" id='{{$component->id}}'>
                                    <div class="item-box">
                                        <div class="block__list">
                                           
                                            <div class="block__box dd-nodrag">
                                                <h2>
                                                    @if($component->user_visible==0)
                                                    Niewidoczne dla użytkownika
                                                    @endif
                                                  <b>{{$component->name}}</b>  @if($component->copied_template_id!=0)
                                                    skopiowane z <b>{{$component->template($component->copied_template_id)->name}}</b>
                                                    @endif</h2>
                                                <p>Boxy: {{$component->chosenBoxesComponent($component->id)}} </p>
                                            </div>
                                      
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                              
                            </ol>

                           
                        </div>

      </div>
  
      <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/module/templates' class="action-button active">Zakończ</a>
        </div>
      </div>
    
  <div class="button-container button-show">
        <div class="button-box">
          <a href='#' id='usun' class="action-button active">Usuń</a>
        </div>
      </div>

      <div class="modal-change-password modal-mask" style='display:none'>
                      <div class="modal-wrapper">
                        <div class="modal-container modal-contact-change-password">
                          <div class="anuluj close-box">
                            <span class="modal-close"></span>
                          </div>

                          <div class="modal-body">
                            <slot name="body">
                              <div class="Message-content">
                                <p>Potwierdź wybór</p>
                              </div>



                              <div class="column is-12 form-login">
                                <div class='col-md-12'>
                                <p class="delete-text"> Chcesz usunąć templatkę?</p>
                                </div>
                                <div class='col-md-6'>
                                  <button class="anuluj modal-button button is-primary float-left" name="button" type="button">Anuluj</button>
                                </div>
                                <div class='col-md-6'>
                                  <form action='{{action('Cms\Cms\Modules\CmsTemplatesModuleController@destroy',$template->id)}}' method='post'>
                                    @method('DELETE')
                                    @csrf
                                    <button class="modal-button button is-primary">Potwierdź</button>
                                  </form>

                                </div>


                              </div>


                            </slot>
                          </div>

                        </div>
                      </div>
                    </div>



    </div>

  </div>
</div>


<script>
  $(document).ready(function () {

    $('#usun').click(function () {
      $('.modal-change-password').show();
    });

    $('.anuluj').click(function () {
      $('.modal-change-password').hide();
    });

  });
</script>

@stop

