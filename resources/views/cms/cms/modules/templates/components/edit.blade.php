@extends('layouts.cms')
@section('content')

          <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/templates'>Szablony</a>
                            </span>
                            <span>
                                <a href='/cms/module/templates/{{$template->id}}'>{{$template->name}}</a>
                            </span>
                             <span>
                              <a href='/cms/module/templates/{{$template->id}}/components'>Komponenty</a>
                            </span>
                             <span>
                              {{$component->name}}
                            </span>
                        </div>
                    <div class="col-sm-12 col-md-6">
                            <h2>Komponent</h2>
                            <p> {{$component->name}}
                            </p>
                        </div>
                      
                    </div>

   
                   </section>

<div class="dashboard-inside">

  
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>

    <div class="company-box">


      @include('notification')
      @include('form_errors')

      <h3>Dane</h3> 

      <form action="/cms/module/templates/{{$template->id}}/components/{{$component->id}}" method='post'>
        @method('PUT')
        @csrf

      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="name" type="text"  value="{{old('name',$component->name)}}">

          <input type='hidden' name="template_id" type="text"  value="{{$template->id}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwa</span>
          </label>

        </div>


      
     
  

   
      </div>

  <h3>Widoczne dla użytkownika</h3>
            <div class='question-container companyProfile col-xs-12 col-md-12'>
                         <div class="row input-group" style='margin-bottom:20px;'>
                 <div class="question-box">
                <ul class="options">

               
          @if(old('user_visible')!=null)
                    @if(old('user_visible')==1)

                   <li><label><input name="user_visible" type="radio" checked  value="1"> <span> Tak</span></label></li>

                   <li><label><input name="user_visible" type="radio" value="0"> <span> Nie </span></label></li>

                    @else
                   <li><label><input name="user_visible" type="radio"   value="1"> <span> Tak</span></label></li>

                    <li><label><input name="user_visible" type="radio" checked value="0"> <span> Nie </span></label></li>
                    @endif
                @else

                   @if($component->user_visible==1)

                    <li><label><input name="user_visible" type="radio" checked  value="1"> <span> Tak</span></label></li>

                    <li><label><input name="user_visible" type="radio" value="0"> <span> Nie </span></label></li>

                    @else
                    <li><label><input name="user_visible" type="radio"   value="1"> <span> Tak</span></label></li>

                    <li><label><input name="user_visible" type="radio" checked value="0"> <span> Nie </span></label></li>
                   @endif

                @endif
                 </ul>
                    </div>
                    
                </div>

              </div>

                <h3>Zapisz zmiany do archiwum</h3>
            <div class='question-container companyProfile col-xs-12 col-md-12'>
                         <div class="row input-group" style='margin-bottom:20px;'>
                 <div class="question-box">
                <ul class="options">
                  <li><label><input name="savearchive" type="radio" checked  value="1"> <span> Tak</span></label></li>

                    <li><label><input name="savearchive" type="radio" value="0"> <span> Nie </span></label></li>
                 </ul>
                    </div>
                    
                </div>

              </div>

      <h3>Edytowalne wartości</h3>
      <div class='question-container companyProfile col-xs-12 col-md-12'>
                   <div class="row input-group" style='margin-bottom:20px;'>
           <div class="question-box">
                  <!---->
                  <!---->
                  <!---->
                  <ul class="options vartiants-option">
                      <li ><label>Zaznacz wszystkie<input type='checkbox' id='zaznaczwszystkie'><span></span></label></li>

                  </ul>
              </div>
              
          </div>

      <div class="row input-group">   
  
      
                <div class="question-box">
                 <!----> <!----> <!----> 
                 <ul class="options vartiants-option">
                 
            
                  @foreach($boxtypes as $box)


                
                  <li class='col-sm-12 col-md-6 col-lg-3' >  
                     <span class="variants_checkboxes">
                   

                    <label><input class='checkboxmodule' name="checkboxmodule[]" type="checkbox" value="{{$box->id}}"
                    
                      @if(is_array(old('checkboxmodule')) && in_array($box->id, old('checkboxmodule')))
                      checked 
                      @elseif(!is_array(old('checkboxmodule'))&&$box->chosenboxes($box->id,$component->id)==1)
                         checked 
                     @endif
                      > <span> </span></label>

                      </span>

                      {{$box->name}}
               
                  </li>
               
              
                  @endforeach
                  </ul>


                   
                   </div>
             
      </div>
</div>

<h3>Informacje</h3>

<div class="row input-group">   
  <div class="alert alert-info alert-dismissible">
   
    <p><b> Tytuł =></b> @php echo '{{$component->title}}' @endphp</p>
     <p><b> Podtytuł =></b> @php echo '{{$component->subtitle}}' @endphp</p>
   <p><b> Nagłówek =></b> @php echo '{!!$component->header!!}' @endphp</p>
    <p><b> Tło =></b> @php echo '{{$component->backgroundsrc($component->background)}}' @endphp</p>
    <p><b> Treść =></b> @php echo '{!!$component->text!!}' @endphp</p>
    <p><b> Zdjęcie =></b> @php echo '{!!$component->imgsrc($component->image,"klasa")!!}' @endphp</p>
    <p><b> Galeria =></b> @php echo '{!!$component->galleries($component->gallery)!!}' @endphp</p>
     <p><b> Odnośnik =></b> @php echo '{{$component->url}}' @endphp</p>
      <p><b> Pliki =></b> @php echo '{{$component->title}}' @endphp</p>
</div>
</div>

      <h3>Content</h3> 
                <div class="row input-group">   
      <div class="input-box col-xs-12 col-md-12">
           
 <textarea  id='codeeditor' class="ckeditor form-control" name="content">{!!$component->content!!}</textarea>
        
 
                    
                    </div>
                </div>


      <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Zapisz</button>
        </div>
      </div>


    </form>




  <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/module/templates/{{$template->id}}/components'  class="action-button active">Anuluj</a>
        </div>
      </div>

<div class="button-container button-show">
        <div class="button-box">
          <a href='#' id='usun' class="action-button active">Usuń</a>
        </div>
      </div>

  <div class="modal-change-password modal-mask" style='display:none'>
                      <div class="modal-wrapper">
                        <div class="modal-container modal-contact-change-password">
                          <div class="anuluj close-box">
                            <span class="modal-close"></span>
                          </div>

                          <div class="modal-body">
                            <slot name="body">
                              <div class="Message-content">
                                <p>Potwierdź wybór</p>
                              </div>



                              <div class="column is-12 form-login">
                                <div class='col-md-12'>
                                <p class="delete-text"> Chcesz usunąć komponent?</p>
                                </div>
                                <div class='col-md-6'>
                                  <button class="anuluj modal-button button is-primary float-left" name="button" type="button">Anuluj</button>
                                </div>
                                <div class='col-md-6'>
                                  <form action='{{action('Cms\Cms\Modules\CmsTemplatesComponentsModuleController@destroy',['template'=>$template->id,'id'=>$component->id])}}' method='post'>
                                    @method('DELETE')
                                    @csrf
                                    <button class="modal-button button is-primary">Potwierdź</button>
                                  </form>

                                </div>


                              </div>


                            </slot>
                          </div>

                        </div>
                      </div>
                    </div>

    </div>


</div>
 
<script>
  $(document).ready(function () {

    $('#usun').click(function () {
      $('.modal-change-password').show();
    });

    $('.anuluj').click(function () {
      $('.modal-change-password').hide();
    });
    $('#zaznaczwszystkie').click(function(){
     $("input[name='checkboxmodule[]']").not(this).prop('checked', this.checked);
  
  });
  });
</script>


@stop

