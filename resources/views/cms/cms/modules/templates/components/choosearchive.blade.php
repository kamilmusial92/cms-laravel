


@extends('layouts.cms')
@section('content')

          <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/templates'>Szablony</a>
                            </span>
                            <span>
                                <a href='/cms/module/templates/{{$template->id}}'>{{$template->name}}</a>
                            </span>
                             <span>
                              <a href='/cms/module/templates/{{$template->id}}/components'>Komponenty</a>
                            </span>
                             <span>
                             Archiwum
                            </span>
                             <span>
                             {{$component->name}}
                            </span>
                        </div>
     <div class="col-sm-12 col-md-6">
                            <h2>Archiwum</h2>
                            <p>
                           {{$component->name}}
                            </p>
                        </div>
                      
                    </div>

   
                   </section>

<div class="dashboard-inside">

  
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>


    <div class="company-box">


      @include('notification')
      @include('form_errors')


      <h3>Wybierz archiwum</h3>
    
      <div class="row input-group">
      <form action="/cms/module/templates/{{$template->id}}/components/{{$componentid}}/chosenarchive" method='post'>
        @csrf
        <div class='col-sm-3 col-md-3 input__row--box'>
          <div class="custom-select">
              <select name='chosen'>

                 @if(!empty($archive))
                  <option value="{{$component->id}}">{{$component->created_at}}</option>
                 @else
                  <option value="0">Wybierz archiwum:</option>
                 @endif
                
                @foreach($archives as $chosetemplate)
                <option value='{{$chosetemplate->id}}'>{{$chosetemplate->created_at}}</option>
                @endforeach
              </select>
          </div>
             <span></span>
      </div>


    
    
       
      </form>
      </div>
      @if(!empty($archive))

  
    @include('notification')

 

   <form action="/cms/module/templates/{{$template->id}}/components/{{$componentid}}/{{$archive}}/copyarchive" method='post'>
    @csrf

       
      <h3>Dane</h3> 

  
        @csrf

      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="name" type="text" disabled  value="{{old('name',$component->name)}}">

          <input type='hidden' name="template_id" type="text"  value="{{$template->id}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwa</span>
          </label>

        </div>


      
     
  

   
      </div>

  <h3>Widoczne dla użytkownika</h3>
            <div class='question-container companyProfile col-xs-12 col-md-12'>
                         <div class="row input-group" style='margin-bottom:20px;'>
                 <div class="question-box">
                <ul class="options">

               
          @if(old('user_visible')!=null)
                    @if(old('user_visible')==1)

                   <li><label><input name="user_visible" type="radio" checked disabled value="1"> <span> Tak</span></label></li>

                   <li><label><input name="user_visible" type="radio" disabled value="0"> <span> Nie </span></label></li>

                    @else
                   <li><label><input name="user_visible" type="radio" disabled  value="1"> <span> Tak</span></label></li>

                    <li><label><input name="user_visible" type="radio" disabled checked value="0"> <span> Nie </span></label></li>
                    @endif
                @else

                   @if($component->user_visible==1)

                    <li><label><input name="user_visible" type="radio" checked disabled value="1"> <span> Tak</span></label></li>

                    <li><label><input name="user_visible" type="radio" disabled value="0"> <span> Nie </span></label></li>

                    @else
                    <li><label><input name="user_visible" type="radio" disabled  value="1"> <span> Tak</span></label></li>

                    <li><label><input name="user_visible" type="radio" disabled checked value="0"> <span> Nie </span></label></li>
                   @endif

                @endif
                 </ul>
                    </div>
                    
                </div>

              </div>

          

      <h3>Edytowalne wartości</h3>
      <div class='question-container companyProfile col-xs-12 col-md-12'>
   

      <div class="row input-group">   
  
      
                <div class="question-box">
                 <!----> <!----> <!----> 
                 <ul class="options vartiants-option">
                 
            
                  @foreach($boxtypes as $box)


                
                  <li class='col-sm-12 col-md-6 col-lg-3' >  
                     <span class="variants_checkboxes">
                   

                    <label><input class='checkboxmodule' name="checkboxmodule[]" type="checkbox" value="{{$box->id}}"
                    
                      @if(is_array(old('checkboxmodule')) && in_array($box->id, old('checkboxmodule')))
                      checked disabled
                      @elseif(!is_array(old('checkboxmodule'))&&$box->chosenboxesarchive($box->id,$component->id)==1)
                         checked disabled
                     @endif
                      > <span> </span></label>

                      </span>

                      {{$box->name}}
               
                  </li>
               
              
                  @endforeach
                  </ul>


                   
                   </div>
             
      </div>
</div>



      <h3>Content</h3>
                <div class="row input-group">   
      <div class="input-box col-xs-12 col-md-12">
           
<textarea  id='codeeditor' class="ckeditor form-control" name="content">{{$component->content}}</textarea>
        
 
                    
                    </div>
                </div>
                       <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Skopiuj do aktualnego</button>
        </div>
      </div>
                  </form>
              <div class="button-container button-show">
        <div class="button-box">
          <a href='#' id='usun' class="action-button active">Usuń</a>
        </div>
      </div>

  <div class="modal-change-password modal-mask" style='display:none'>
                      <div class="modal-wrapper">
                        <div class="modal-container modal-contact-change-password">
                          <div class="anuluj close-box">
                            <span class="modal-close"></span>
                          </div>

                          <div class="modal-body">
                            <slot name="body">
                              <div class="Message-content">
                                <p>Potwierdź wybór</p>
                              </div>



                              <div class="column is-12 form-login">
                                <div class='col-md-12'>
                                <p class="delete-text"> Chcesz usunąć archiwum komponentu?</p>
                                </div>
                                <div class='col-md-6'>
                                  <button class="anuluj modal-button button is-primary float-left" name="button" type="button">Anuluj</button>
                                </div>
                                <div class='col-md-6'>
                                  <form action='{{action('Cms\Cms\Modules\CmsTemplatesComponentsModuleController@destroyarchive',['template'=>$template->id,'id'=>$componentid,'archive'=>$archive])}}' method='post'>
                                    @method('DELETE')
                                    @csrf
                                    <button class="modal-button button is-primary">Potwierdź</button>
                                  </form>

                                </div>


                              </div>


                            </slot>
                          </div>

                        </div>
                      </div>
                    </div>
@endif


    </div>

  </div>

        <script>
$(document).ready(function () {

  
    $('#usun').click(function () {
      $('.modal-change-password').show();
    });

    $('.anuluj').click(function () {
      $('.modal-change-password').hide();
    });
    $('#zaznaczwszystkie').click(function(){
     $("input[name='checkboxmodule[]']").not(this).prop('checked', this.checked);
  
  });

   $('#zaznaczwszystkie').click(function(){
     $("input[name='checkboxmodule[]']").not(this).prop('checked', this.checked);
  
  });




  var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      h = this.parentNode.previousSibling;
      for (i = 0; i < s.length; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          for (k = 0; k < y.length; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
        //pprzeładowanie strony
         $(this).closest('form').submit();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
   

    });
    </script>

@stop

