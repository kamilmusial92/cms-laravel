@extends('layouts.cms')
@section('content')


            <section class="row">
            
            <div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/templates'>Szablony</a>
                            </span>
                             <span>
                                <a href='/cms/module/templates/{{$template->id}}'>{{$template->name}}</a>
                            </span>
                             <span>
                               Komponenty
                            </span>
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Komponenty</h2>
                        </div>
            </div>
   <div class="col-sm-12 col-md-12">
            <div class='row shop_nav'>
                        <div class="shop_nav__box">
                            <a href="/cms/module/templates/{{$template->id}}/components/create">
                              <div class="shop_nav__box--icon">
                                  <img alt="icon" src="/img/icons/pencil.svg" />
                              </div>
                                <div class="shop_nav__box--text">
                                    <p>Nowy komponent</p>
                                </div>
                            </a>
                        </div>

                        <div class="shop_nav__box">
                            <a href="/cms/module/templates/{{$template->id}}/components/choosetemplate">
                              <div class="shop_nav__box--icon">
                                  <img alt="icon" src="/img/icons/icon-puzzle.svg" />
                              </div>
                                <div class="shop_nav__box--text">
                                    <p>Skopiuj komponenty</p>
                                </div>
                            </a>
                        </div>
            </div>
</div>

   <div class="col-sm-12 col-md-12">
    @include('notification')
                           <div class="dd">
                            <ol class="sortable dd-list">
                               
                               @foreach($components as $component)
                              
                               @if($component->status==0)
                                    <li class="dd-item"  data-id="1" id='{{$component->id}}'>
                                @elseif($component->user_visible==1)
                                <li class="dd-item" data-id="1" style='display:none;' id='{{$component->id}}'>
                                    @else
                                     <li class="dd-cancel dd-item"  data-id="1" id='{{$component->id}}'>
                                        @endif
                                    <div class="item-box">
                                        <div class="block__list">
                                            <div class="block__handle">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <div class="block__box dd-nodrag">
                                                <h2>
                                                   

                                                    <b>{{$component->name}}</b> 
                                                    @if($component->copied_template_id!=0)
                                                    skopiowane z <b>{{$component->template($component->copied_template_id)->name}}</b>
                                                    @endif
                                                </h2>
                                                <p>Boxy: {{$component->chosenBoxesComponent($component->id)}} </p>
                                            
                                            </div>
                                            <div class="block__menu dd-nodrag">
                                                
                                               <div class="icon-settings gallery__item--delete">
                                                            <a class='usunkomponent' href="#" title="usuń">
                                                               <i class="fa fa-trash" ></i>
                                                            </a>
                                                        </div>

                                                <a class="icon-settings" href="/cms/module/templates/{{$template->id}}/components/{{$component->id}}/showarchive">
                                                    <img alt="menu icon" src="/img/icons/briefs.svg" />
                                                </a>

                                                <a class="icon-settings" href="/cms/module/templates/{{$template->id}}/components/{{$component->id}}/edit">
                                                    <img alt="menu icon" src="/img/icons/icon-settings.svg" />
                                                </a>
                                                @if($component->user_visible==0)
                                                  <div class="show-list block__handle">
                                                <i class="fa fa-chevron-down"></i>
                                               
                                                    </div>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                     <div class="modal-change-password modal-mask" style='display:none'>
                                            <div class="modal-wrapper">
                                                <div class="modal-container modal-contact-change-password">
                                                    <div class="anuluj close-box">
                                                        <span class="modal-close"></span>
                                                    </div>
                    
                                                    <div class="modal-body">
                                                        <slot name="body">
                                                            
                                                            <div class="Message-content">
                                                                <p>Potwierdź wybór</p>
                                                            </div>
                    
                                                            <div class="column is-12 form-login">
                                                           
                                                                    <p class="delete-text">Chcesz usunąć komponent?</p>
                                                           
                    
                                                                <div class='col-md-6'>
                                                                    <button class="anuluj modal-button button is-primary float-left" name="button"
                                                                        type="button">Anuluj</button>
                                                                </div>
                                                                <div class='col-md-6'>
                                                                    <form id='removecomponent' action='{{action('Cms\Cms\Modules\CmsTemplatesComponentsModuleController@destroy',['template'=>$template->id,'id'=>$component->id])}}'
                                                                        method='post'>
                                                                        @method('DELETE')
                                                                        @csrf
                                                                        <button class="modal-button button is-primary">Potwierdź</button>
                                                                    </form>
                                                                </div>
                    
                    
                                                            </div>
                    
                    
                                                        </slot>
                                                    </div>
                    
                                                </div>
                                            </div>
                                        </div>
                                </li>
                                @endforeach
                              
                            </ol>
                             <script>

var arraysort = [];
                                    $(".sortable").sortable({
                                         
                                        stop: function() {
                                            $.map($(this).find('li'), function(el) {
                                                var id = el.id;
                                                var sorting = $(el).index();
                                                
                                                 //console.log(id+' '+sorting);
                                                 arraysort.push({'id':id,'sorting':sorting});
                                                 
                                             /*  $.ajax({
                                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                                    url: '{{ route('TemplateComponentSort',['templateid'=>$template->id]) }}',
                                                    type: 'POST',
                                                    data: {
                                                        id: id,
                                                        sorting: sorting,
                                                        token:_token
                                                    },
                                                     success:function(data) {
                                                    
                                                    }
                                                    
                                                });*/

                                            });
                                             var _token=$('meta[name="csrf-token"]').attr('content');
                                             $.ajax({
                                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                                    url: '{{ route('TemplateComponentSort',['templateid'=>$template->id]) }}',
                                                    type: 'POST',
                                                    data: {
                                                        id: arraysort,
                                                       
                                                        token:_token
                                                    },
                                                   
                                                   
                                                    
                                                });

  
                                        }

                                    });
                                

                                 $('.show-list').click(function(){
                                        
                                   if($(this).closest('li').nextUntil('.dd-cancel').css('display') == 'none')
                                   {
                                        $(this).find('i').attr('class','fa fa-chevron-up');
                                        $(this).closest('li').nextUntil('.dd-cancel').slideDown('fast');
                                   }
                                   else{
                                        $(this).find('i').attr('class','fa fa-chevron-down');
                                   // $(this).closest('li').nextUntil('.dd-cancel').css('display','none');
                                       $(this).closest('li').nextUntil('.dd-cancel').slideUp('fast');
                                   }
                                    

                                   

                                    
                                });
                             
                                    </script>
                    
                        </div>
                        
                    </div>
            </section>
   <script>
                        $(document).ready(function () {
                
                            $('.usunkomponent').click(function () {
                                $(this).closest('li').find('.modal-change-password').show();
                              

                            });
                
                            $('.anuluj').click(function () {
                                $('.modal-change-password').hide();
                            });
                
                        });
                    </script>

@stop