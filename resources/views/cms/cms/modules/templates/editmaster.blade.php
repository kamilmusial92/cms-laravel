@extends('layouts.cms')
@section('content')


 <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/templates'>Szablony</a>
                            </span>
                          
                            
                             <span>
                             Plik master
                            </span>
                        </div>
         <div class="col-sm-12 col-md-6">
                            <h2>Plik master</h2>
                          
                        </div>
                      
                    </div>

   
                   </section>

<div class="dashboard-inside">

 
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')
      @include('form_errors')

     

      <form action="/cms/module/templates/savemaster" method='post'>
    @method('PUT')
        @csrf

   <h3>Kod</h3>
                <div class="row input-group">   
      <div class="input-box col-xs-12 col-md-12">
           
 <textarea  id='codeeditor' class="ckeditor form-control" name="content">{{old('content',$templatemastercontent)}}</textarea>
        
 
                    
                    </div>
                </div>

   




      <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Zapisz</button>
        </div>
      </div>


    </form>

  <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/module/templates'  class="action-button active">Anuluj</a>
        </div>
      </div>




    </div>

  </div>
</div>



@stop

