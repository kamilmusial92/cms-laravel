@extends('layouts.cms')
@section('content')


            <section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                                Szablony
                            </span>
                            
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Szablony</h2>
                        </div>
                    </div>

        <div class="col-sm-12 col-md-12">
                    <div class='row shop_nav'>

                        <div class="shop_nav__box">
                            <a href="/cms/module/templates/create">
                              <div class="shop_nav__box--icon">
                                  <img alt="icon" src="/img/icons/pencil.svg" />
                              </div>
                                <div class="shop_nav__box--text">
                                    <p>Nowy szablon</p>
                                </div>
                            </a>
                        </div>


                         <div class="shop_nav__box">
                            <a href="/cms/module/templates/editmaster">
                              <div class="shop_nav__box--icon">
                                  <img alt="icon" src="/img/icons/offer.svg" />
                              </div>
                                <div class="shop_nav__box--text">
                                    <p>Plik Master</p>
                                </div>
                            </a>
                        </div>
                       
                    </div>
                </div>
                   </section>
 <div class="dashboard-inside">
                 <div class='row'>
                        <div class='col-md-6'>
                         @include('notification')
                
                  <div class="dashboard-column">
    <div class="dashboard-tab">
      <div class="tab-header"  data-toggle="collapse" data-target="#collapseTab1" aria-expanded="true" aria-controls="collapseTab1">
        <h2><span>Templatki</span></h2>
      </div>
      <div id="collapseTab1" class="collapse show">
        <table id="table_id">
              <thead >
              
                <tr >
                  <th>Nazwa</th>
                  
                  <th>Opcje</th>
                
                </tr>
              </thead>
              <tbody>
            @foreach($templates as $template)
         <tr >
            <td>{{$template->name}}</td>
           
            <td><a href='/cms/module/templates/{{$template->id}}' class='button-link waves-effect waves-light'>Opcje</a></td>
             </tr >
            @endforeach
              </tbody>
              
            </table>
  </div>

     </div>
        </div>
    </div>
                </div>    
</div>
             

@stop