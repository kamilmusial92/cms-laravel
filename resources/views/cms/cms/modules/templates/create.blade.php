@extends('layouts.cms')
@section('content')
         <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/templates'>Szablony</a>
                            </span>
                          
                             <span>
                               Nowa templatka
                            </span>
                        </div>
    
                      
                    </div>

   
                   </section>


<div class="dashboard-inside">

  <div class="dashboard-head">
    <h2>Nowa templatka </h2>
  </div>
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')
      @include('form_errors')

 

 <h3>Dane</h3> 
      <form action="/cms/module/templates" method='post'>
    
        @csrf

      <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="name" type="text"  value="{{old('name')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Nazwa</span>
          </label>

        </div>
      

     
  

   
      </div>

       <div class="row input-group">
        <h3>Pozycjonowanie</h3> 

     <div class="row input-group">

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="title_page" type="text"  value="{{old('name')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Tytuł strony</span>
          </label>

        </div>

        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="description_page" type="text"  value="{{old('escription_page')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Opis strony</span>
          </label>

        </div>
    
         <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="keywords_page" type="text"  value="{{old('keywords_page')}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Słowa kluczowe</span>
          </label>

        </div>

      </div>
       

      </div>

        <h3>Pozycjonowanie - OG Image</h3>
    <div class="row input__row">
           <input type="hidden" name='image' value=''>


  <div id='image-show' style='display:none' class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gallery-show-image-box">
                                <a data-fancybox="gallery" href=""><img id='image-image' src="" class="img-thumbnail hover-shadow"></a>
   </div>
       <div class='col-sm-3 col-md-3 input__row--box'>
                              <div class="custom-select" data='image'>
                                  <select >
                                      <option value="0">Wybierz kategorię:</option>
                                      @foreach($galleries as $gallery)

                                      <option value="{{$gallery->id}}">{{$gallery->name}}</option>

                                      @endforeach
                                  </select>
                              </div>
                              <span></span>
                          </div>
                          <div class='col-sm-6'>
                              <div class="row gallery__tab">
                                  <div class="col-sm-12">
                                      <div id='tloimage' class="gallery__tab--head">
                                          <i class="icon photos"></i>
                                          <h2>Zdjęcia z galerii <b></b></h2>
                                         
                                      </div>
              
                                      <div class="gallery__tab--photos">
                                          <div class="row no-gutters">
                                              <div class="col-md-12 col-lg-9">
                                                  <div class="images-image row no-gutters">
                                                     
              
              
                                                  </div>
                                              </div>
                                             
          
                                              
                                          </div>
              
              
                                      </div>
                                  </div>
              
                              </div>

                          </div>


      </div>



      <div class="button-container button-show">
        <div class="button-box">
                <button type="submit" name="button" class="action-button active">Następny krok</button>
        </div>
      </div>


    </form>

  <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/module/templates'  class="action-button active">Anuluj</a>
        </div>
      </div>




    </div>

  </div>
</div>


<script>

  
// Custom select
$(document).ready(function(){

              $('.choosefile').click(function(){
                  id=$(this).closest('.gallery__photos').find('.gallery__photos--icons').find('input').val();
                  name=$(this).closest('.gallery__photos').find('.gallery__photos--text').find('input').attr('value');
                  
                   $('.listoffiles').append('<div class="filed col-md-2"><div class="gallery__photos"><div class="gallery__photos--files"><i class="fa fa-file-alt" style="font-size: 6em;"></i></div><div class="gallery__photos--icons"><a class="usunfile" href="#"><input type="hidden" class="idim" name="choosefile[]" value="'+id+'"><i class="icon2 cancel" data-toggle="tooltip" title="Usuń"></i></a></div><div class="gallery__photos--text"><input type="text" name="changePicNameInput" class="" id="changePicNameInput" value="'+name+'" disabled></div></div></div>');
                   usunfile();
                });

   function usunfile()
   {

    $('.usunfile').click(function(e){
      e.preventDefault();
    $(this).closest('.filed').remove();
    });
   }
   usunfile();

$('.custom-select').click(function(){

  gallery=$(this).find( "option:selected" ).val();
name=$(this).attr('data');


  var _token=$('meta[name="csrf-token"]').attr('content');
  $('.images-'+name+'').html('');         
  $.ajax({
                    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
                    url: '{{ route('showgalleryimageSEO') }}',
                    type: 'POST',
                   
                    data: {
                        
                       gallery:gallery,
                        token:_token
                    },
                     success: function(html){
                      var gallery = JSON.parse(html);
                   
                    $('#tlo'+name+'').find('h2').html('<h2>Zdjęcia z galerii <b>'+gallery.gallery.name+'</b></h2>');
                      
                 var jsonData = JSON.parse(html);
                  for (var i = 0; i < jsonData.images.length; i++) {
                   var counter = jsonData.images[i];

                  $('.images-'+name+'').append("<div class='col-md-12 col-lg-3'><div class='gallery__photos'><div class='gallery__photos--image2'><a data-fancybox='gallery' href='/"+counter.file_path+counter.file_name+"'><img alt='lorem picsum' src='/"+counter.file_path+counter.file_name+"' /></a></div><div class='gallery__photos--icons'><button type='button' class='chooseimg"+name+" button__icon'><span class='docs-tooltip' data-toggle='tooltip' title='Wybierz'><img alt='icon' src='/img/icons/tick.svg' /></span></button><input type='hidden' name='' value='"+counter.id+"'></div><div class='gallery__photos--text'><p>"+counter.file_name+"</p></div></div></div>");
   
                  }

                  $('.chooseimg'+name+'').click(function(){
                  id=$(this).closest('.gallery__photos').find('input').val();
               
                  src=$(this).closest('.gallery__photos').find('.gallery__photos--image2').find('img').attr('src');
                  
                    $( "input[name='"+name+"']").val(id);
                    $('#'+name+'-show').show();
                    $('#'+name+'-hide').hide();
                    $('#'+name+'-image').attr('src',src);
                });


             
                    }

                });       
          
});





var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      h = this.parentNode.previousSibling;
      for (i = 0; i < s.length; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          for (k = 0; k < y.length; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
});
</script>


@stop

