@extends('layouts.cms')
   @section('content')
<section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                           Pliki do pobrania
                            </span>
                           
                            
                        </div>
    
                        <div class="col-sm-12 col-md-6">
                            <h2>Pliki do pobrania</h2>
                            
                      
                        </div>
                    </div>
        
                 
    
                </section>
 <div class="dashboard-inside">
  <div class="dashboard-column">
  
    @include('notification')
    <div class="dashboard-tab">
<div class="row gallery__tab">
                        <div class="col-sm-12">
                            <div class="gallery__tab--head">
                                <i class="icon photos"></i>
                                <h2>Pliki do pobrania</h2>
                               
                            </div>
    
                            <div class="gallery__tab--photos">
                                <div class="row no-gutters">
                                    <div class="col-md-12 col-lg-9">
                                        <div class="row no-gutters">
                                            @foreach($list as $image)
                                            <div class='col-md-12 col-lg-3'>
                                                <div class="gallery__photos">
                                                    <div class="gallery__photos--image">
                                                        <i class='fa fa-file-alt' style="font-size: 10em;"></i>
                                                    </div>
                                                    <div class="gallery__photos--icons">
                                                        <a href="#" class="editPicName" data-toggle="tooltip" title="Zmień nazwę">
                                                            <i class="icon2 pencil"></i>
                                                        </a>
                                                       <a class='usun' href='#'>
                                                            <input type='hidden' class='idim' value='{{$image->id}}'>
                                                            <i class="icon2 cancel" data-toggle="tooltip" title='Usuń'></i>
                                                        </a>
                                                       
                                                    </div>
                                                    <div class="gallery__photos--text">
                                                        <input type="text" name="changePicNameInput" class="" id="changePicNameInput" value="{{$image->file_name}}" disabled>
                                                        <button type="button" class="change-name button__icon">
                                                            <span class="docs-tooltip"  >
                                                                <img alt="icon" src="/img/icons/tick.svg" />
                                                            </span>
                                                        </button>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                            @endforeach
    
    
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-3">
                                        <form action="/cms/module/files_to_download/uploadimage" class="gallery__dropzone dropzone" method='post' enctype="multipart/form-data">
                                            @csrf 
                                            <div class="fallback">
                                                <input name="image" type="file" multiple />
                                            </div>
                                        </form>
    
                                    </div>

                                    
                                </div>
    
    
                            </div>
                        </div>
    
                    </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){

    $('.usun').click(function(){

        image=$(this).find('input').val();
        $(this).closest('li').remove();

    var _token=$('meta[name="csrf-token"]').attr('content');
                 
                $.ajax({
                    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
                    url: '{{ route('removefile') }}',
                    type: 'POST',
                   
                    data: {
                        
                       image:image,
                        token:_token
                    },
                     success: function(html){
  location.reload();
                    }

                });

 });


 $('.change-name').click(function(){

image=$(this).closest('.gallery__photos').find('.usun').find('input').val();
name=$(this).closest('.gallery__photos--text').find('input').val();

var _token=$('meta[name="csrf-token"]').attr('content');
         
        $.ajax({
            headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
            url: '{{ route('changenamefile') }}',
            type: 'POST',
           
            data: {
                
               image:image,
               name:name,
                token:_token
            },
             success: function(html){
location.reload();
            }

        });

});


});

</script>
    

  @stop