@extends('layouts.cms')
@section('content')
 <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/blog'>Posty</a>
                            </span>
                          
                             <span>
                               <a href='/cms/module/blog/{{$post->id}}'>{{$post->title}}</a> 
                            </span>
                            
                        </div>
    
                         <div class="col-sm-12 col-md-6">
                            <h2>Post</h2>
                            <p>{{$post->title}}</p>
                        </div>
                    </div>

   
                   </section>
 <div class="dashboard-inside">
 <div class="dashboard-head">
         <h2>Aktualne</h2>


     </div>
     @if(!empty($image))
     <div class='row'>
  <div class="col-md-4 col-lg-3">
    <div class="gallery__photos">
     <div class="gallery__photos--image">
     <a data-fancybox="gallery" href="/{{$image->file_path.$image->file_name}}">
      <img alt="lorem picsum" src="/{{$image->file_path.$image->file_name}}">
     </a>
      </div>
                                                 
       <div class="gallery__photos--text">
         <p>{{$image->file_name}}</p>
       </div>
      </div>
    </div>
  </div>
  @endif
 <div class="dashboard-head">
         <h2>Wybierz zdjęcie do {{$header}}</h2>
     </div>
  
  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
    @include('notification')
<div class="col-xs-12 col-md-2">

                           
<ul class="nav flex-column">
                                   @foreach($galleries as $gallery)
                                     <li class="nav-item">
                                        <a class="nav-link" href="/cms/module/blog/{{$post->id}}/header/{{$gallery->id}}">{{$gallery->name}}</a>
                                    </li>
                                    @endforeach

                                </ul>
                                 </div>
                                 <div class="row gallery__tab">
                        <div class="col-sm-12">
                            <div class="gallery__tab--head">
                                <i class="icon photos"></i>
                                <h2>Zdjęcia z galerii <b>{{$galleryid->name}}</b></h2>
                               
                            </div>
    
                            <div class="gallery__tab--photos">
                                <div class="row no-gutters">
                                    <div class="col-md-12 col-lg-9">
                                        <div class="row no-gutters">
                                            @foreach($images as $image)
                                            <div class='col-md-12 col-lg-3'>
                                                <div class="gallery__photos">
                                                    <div class="gallery__photos--image">
                                                        <a data-fancybox="gallery" href="/{{$image->file_path.$image->file_name}}">
                                                            <img alt="lorem picsum" src="/{{$image->file_path.$image->file_name}}" />
                                                        </a>
                                                    </div>
                                                    <div class="gallery__photos--icons">
                                                       
                                                       <form action='/cms/module/blog/{{$post->id}}/{{$form}}/{{$galleryid->id}}' method='post'>
                                                            @csrf
                                                            <input type='hidden' name='image' value='{{$image->id}}'>
                                                            <div class="button-container" style='margin:0;'>
                                                         <div class=" button-box">
        
       
                                                            <button class='button-link waves-effect waves-light'><i class="fa fa-check" title='wybierz'></i></button>
                                                             </div>
                                                          </div>
                                                        </form>
                                                    </div>
                                                    <div class="gallery__photos--text">
                                                        <p>{{$image->file_name}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
    
    
                                        </div>
                                    </div>
                              

                                    
                                </div>
    
    
                            </div>
                        </div>
    
                    </div>
<div class='row'>
                       <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/module/blog/{{$post->id}}' class="action-button active">Zakończ</a>
        </div>
      </div>
</div>

                                  <script>
            $(function(){
    var current = location.pathname;
    $('.nav li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href').indexOf(current) !== -1){
            $this.addClass('active');
        }
        else{
             $this.removeClass('active');
        }
    });
});
            </script>
  
</div>

    

  @stop