@extends('layouts.cms')
@section('content')


  <section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                              <span>
                                <a href="/cms/module/blog"> Posty</a>
                            </span>
                            <span>
                                Nowy post
                            </span>
                            
                        </div>
      <div class="col-sm-12 col-md-6">
                            <h2>Nowy post</h2>
                          
                      
                        </div>
                    
                    </div>

       
</section>

<div class="dashboard-inside">


     <div class="go-back right">
         <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
     </div>
     <div class="dashboard-inside">
         <div class="company-box">
        
            


       @include('form_errors')
             <h3>Dane</h3>
           <form action="/cms/module/blog" method='post'>
                 @csrf
         
         
             <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-4">
                     <input id="input-1" class="input__field" name="title"  type="text" value="{{old('title')}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Tytuł</span>
                     </label>
                 
                 </div>
                            <div class="input-box col-xs-12 col-md-4">
       <input id="input-1" class="input__field" name="url" type="text"  value="{{old('url')}}">
       <label class="input__label input__label--haruki" for="input-1">
         <span class="input__label-content input__label-content--haruki">Url</span>
       </label>

     </div>
  <div class="input-box col-xs-12 col-md-4">
   <input type="text" name="datetimes" />
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Data publikacji</span>
          </label>

        </div>

                <!-- <div class='question-container companyProfile col-xs-12 col-md-3'>
                
                   <div class="question-box"><h1>Opublikowany:</h1>
                  
                    <ul class="options">
                     <li><label><input name="active" type="radio"  value="Tak"> <span> Tak </span></label></li>
                     <li><label><input name="active" type="radio" checked value="Nie"> <span> Nie </span></label></li><li></ul>
                      </div>
                 </div>-->


             </div>

                  <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-12">
                     <input id="input-1" class="input__field" name="info"  type="text" value="{{old('info')}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Opis</span>
                     </label>
                 
                 </div>
                 </div> 

                     <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-12">
                     <input id="input-1" class="input__field" name="keywords"  type="text" value="{{old('keywords')}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Słowa kluczowe</span>
                     </label>
                 
                 </div>
                 </div> 

                     <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-12">
                     <input id="input-1" class="input__field" name="tags"  type="text" value="{{old('tags')}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Hashtagi</span>
                     </label>
                 
                 </div>
                 </div> 

<h3>Tekst</h3>
             <div class="row input-group">   
         <div class="input-box col-xs-12 col-md-12">
        
        
      
                    <textarea  class="ckeditor form-control" name='long_text'>{{old('long_text')}}</textarea>
                 
                 </div>
             </div>

               <div class="button-container">
     <div class=" button-box">
     <button type="submit" name="button" class="action-button active" >Następny krok</button>
     </div>
 </div>
          </form>
      
         </div>
    
     </div>
 </div>
<script>
  $(document).ready(function () {

    $('input[name="datetimes"]').daterangepicker({
    timePicker: true,
    timePicker24Hour: true,
    timePickerIncrement: 30,
    singleDatePicker: true,
    startDate: moment().startOf('hour'),
   dateLimit: { hours: 24 },
    locale: {
      format: 'DD-MM-YYYY HH:mm',

       "separator": " - ",
        "applyLabel": "Zatwierdź",
        "cancelLabel": "Anuluj",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Nd",
            "Pon",
            "Wt",
            "Śr",
            "Czw",
            "Pi",
            "So"
        ],
        "monthNames": [
            "Styczeń",
            "Luty",
            "Marzec",
            "Kwiecień",
            "Maj",
            "Czerwiec",
            "Lipiec",
            "Sierpień",
            "Wrzesień",
            "Październik",
            "Listopad",
            "Grudzień"
        ],
        "firstDay": 1
    }
  });

  });


</script>

@stop