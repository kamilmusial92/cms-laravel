@extends('layouts.cms')
@section('content')

 <section class="row">
<div class="row blog__head">
                          <div class="col-sm-12 col-md-6">
                            <span>
                                <a href='/cms/module/blog'>Posty</a>
                            </span>
                          
                             <span>
                               
                               {{$post->title}} 
                            </span>
                            
                        </div>
    
                         <div class="col-sm-12 col-md-6">
                            <h2>Post</h2>
                            <p>{{$post->title}}</p>
                        </div>
                    </div>

   
                   </section>
<div class="dashboard-inside">


  <div class="go-back right">
    <a href="javascript:history.go(-1)"><img src="/img/icons/back.svg" class="icon icon-back" /><span>Powrót</span></a>
  </div>
  <div class="dashboard-inside">
    <div class="company-box">


      @include('notification')


      <h3>Tytuł</h3> <a href="/cms/module/blog/{{$post->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>



      <div class="row input-group">
        
        <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="title" type="text" disabled value="{{$post->title}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Tytuł</span>
          </label>

        </div>

         <div class="input-box col-xs-12 col-md-4">
          <input id="input-1" class="input__field" name="url" type="text" disabled value="{{$post->url}}">
          <label class="input__label input__label--haruki" for="input-1">
            <span class="input__label-content input__label-content--haruki">Url</span>
          </label>

        </div>



        <div class='question-container companyProfile col-xs-12 col-md-3'>

          <div class="question-box">
            <h1>Opublikowany:</h1>
            <!---->
            <!---->
            <!---->
            <ul class="options">
              @if($post->visible==1)

              <li><label><input name="active" type="radio" checked disabled value="1"> <span> Tak </span></label></li>

              <li><label><input name="active" type="radio" disabled value="0"> <span> Nie </span></label></li>
              

                @else
              <li><label><input name="active" type="radio" disabled value="1"> <span> Tak </span></label></li>

              <li><label><input name="active" type="radio" checked disabled value="0"> <span> Nie </span></label></li>
             

                @endif
            </ul>



          </div>
        </div>



</div>

<h3>Data publikacji</h3> <a href="/cms/module/blog/{{$post->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
        <div class="row input-group">
          {{$post->publish_at}}
        </div>

        <h3>Dane</h3> <a href="/cms/module/blog/{{$post->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
          
           <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-12">
                     <input id="input-1" class="input__field" name="info" disabled type="text" value="{{old('info',$post->info)}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Opis</span>
                     </label>
                 
                 </div>
                 </div> 

                     <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-12">
                     <input id="input-1" class="input__field" name="keywords" disabled type="text" value="{{old('keywords',$post->keywords)}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Słowa kluczowe</span>
                     </label>
                 
                 </div>
                 </div> 

                     <div class="row input-group">   
            <div class="input-box col-xs-12 col-md-12">
                     <input id="input-1" class="input__field" name="tags" disabled type="text" value="{{old('tags',$post->tags)}} ">
                     <label class="input__label input__label--haruki" for="input-1">
                         <span class="input__label-content input__label-content--haruki">Hashtagi</span>
                     </label>
                 
                 </div>
                 </div>

  <h3>Miniaturka</h3> <a href="/cms/module/blog/{{$post->id}}/thumbnail" class="button-link waves-effect waves-light">Zmień</a>
      @include('form_errors')
      <div class="row input-group">
        @if($post->thumbnail_id!=0)
        <div class='col-md-3'>
          <a data-fancybox="gallery" href="/{{$post->picture($post->thumbnail_id)->file_path.$post->picture($post->thumbnail_id)->file_name}}"> <img src="/{{$post->picture($post->thumbnail_id)->file_path.$post->picture($post->thumbnail_id)->file_name}}"
              class="img-thumbnail hover-shadow" onclick="openModal();currentSlide({{$post->id}})"></a>
        </div>
        @endif
      </div>

          <h3>Nagłówek</h3> <a href="/cms/module/blog/{{$post->id}}/header" class="button-link waves-effect waves-light">Zmień</a>
      @include('form_errors')
      <div class="row input-group">
        @if($post->image_id!=0)
        <div class='col-md-3'>
          <a data-fancybox="gallery" href="/{{$post->picture($post->image_id)->file_path.$post->picture($post->image_id)->file_name}}"> <img src="/{{$post->picture($post->image_id)->file_path.$post->picture($post->image_id)->file_name}}"
              class="img-thumbnail hover-shadow" onclick="openModal();currentSlide({{$post->id}})"></a>
        </div>
        @endif
      </div>
      
      <h3>Tekst</h3><a href="/cms/module/blog/{{$post->id}}/edit" class="button-link waves-effect waves-light">Zmień</a>
        <div class="row input-group">
                            <div class="input-box col-xs-12 col-md-6">
                                {!! $post->long_text!!}

                            </div>
                        </div>

    
      <div class="button-container button-show">
        <div class="button-box">
          <a href='/cms/blog' class="action-button active">Zakończ</a>
        </div>
      </div>
      <div class="button-container button-show">
        <div class="button-box">
          <a href='#' id='usun' class="action-button active">Usuń</a>
        </div>
      </div>




          <div class="modal-change-password modal-mask" style='display:none'>
                      <div class="modal-wrapper">
                        <div class="modal-container modal-contact-change-password">
                          <div class="anuluj close-box">
                            <span class="modal-close"></span>
                          </div>

                          <div class="modal-body">
                            <slot name="body">
                              <div class="Message-content">
                                <p>Potwierdź wybór</p>
                              </div>



                              <div class="column is-12 form-login">
                                <div class='col-md-12'>
                                <p class="delete-text"> Chcesz usunąć post?</p>
                                </div>
                                <div class='col-md-6'>
                                  <button class="anuluj modal-button button is-primary float-left" name="button" type="button">Anuluj</button>
                                </div>
                                <div class='col-md-6'>
                                  <form action='{{action('Cms\Cms\Modules\CmsBlogPostsModuleController@destroy',$post->id)}}' method='post'>
                                    @method('DELETE')
                                    @csrf
                                    <button class="modal-button button is-primary">Potwierdź</button>
                                  </form>

                                </div>


                              </div>


                            </slot>
                          </div>

                        </div>
                      </div>
                    </div>


    </div>

  </div>
</div>


<script>
  $(document).ready(function () {

    $('#usun').click(function () {
      $('.modal-change-password').show();
    });

    $('.anuluj').click(function () {
      $('.modal-change-password').hide();
    });
 


  });


</script>

@stop