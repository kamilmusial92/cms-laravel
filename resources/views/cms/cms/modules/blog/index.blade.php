@extends('layouts.cms')
@section('content')

           <section class="row">
<div class="row blog__head">
                        <div class="col-sm-12 col-md-6">
                            <span>
                                Blog
                            </span>
                            
                        </div>
    
                    
                    </div>

           <div class="col-sm-12 col-md-12">
           
    <div class='row shop_nav'>
     

      

        <div class="shop_nav__box">
            <a href="/cms/module/blog/create">
              <div class="shop_nav__box--icon">
                  <img alt="icon" src="/img/icons/pencil.svg" />
              </div>
                <div class="shop_nav__box--text">
                    <p>Nowy post</p>
                </div>
            </a>

        </div>

        

    </div>



                    </div>
</section>
          



 <div class="dashboard-inside">
  <div class="dashboard-column">
   
    @include('notification')
    <div class="dashboard-tab">
      <div class="tab-header" data-toggle="collapse" data-target="#collapseTab1" aria-expanded="true" aria-controls="collapseTab1">
        <h2><span>Posty</span></h2>
      </div>

      <div id="collapseTab1" class="collapse show">
        <div class="tab-content">

          <div class="row">

            <table class="table-briefs tab">
              <thead>

                <tr>
                <th>Miniaturka</th>
                  <th>Tytuł</th>
                  
                  <th>Opublikowany</th>
                  <th>Autor</th>
                    <th>Data publikacji</th>

                  <th>Opcje</th>
                </tr>
              </thead>
              <tbody>
                @foreach($list as $post)
                <tr>
                    <td> 
                        @if($post->thumbnail_id!=0)

                            <img style='width:50px;height:50px;' src='/{{$post->picture($post->thumbnail_id)->file_path.$post->picture($post->thumbnail_id)->file_name}}'>
                            @endif
                    </td>
                  <td> {{$post->title}} </td>
					
                  <td>{{$post->status($post->visible)}} </td>
                  
                  <td>{{$post->user($post->user_id)->name}} </td>
                  <td>{{$post->publish_at}} </td>
                  <td>
                    <a href="/cms/module/blog/{{$post->id}}" class="button-link waves-effect waves-light">zmień</a>

                  

              
                  </td>


                </tr>
                @endforeach
              </tbody>
            </table>

          </div>

        </div>
      </div>

    </div>
  </div>
</div>

    

  @stop