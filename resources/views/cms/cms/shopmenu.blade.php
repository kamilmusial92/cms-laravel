           

            <li class="waves-effect waves-light">
                <a target='_blank' href="http://{{$_SERVER['HTTP_HOST']}}" ><img src="/img/icons/home.svg" class="icon icon-home" /><span>Podgląd sklepu</span><i class="icon-arrow"></i></a>
            </li>

            <li class="waves-effect waves-light">
               <a href="{{url('shop/statistics')}}"><img src="/img/icons/graphic.svg" class="icon icon-home" /><span>Statystyki</span><i class="icon-arrow"></i></a>
            </li>

   				 <li class="waves-effect waves-light">
               <a href="{{url('shop/orders')}}"><img src="/img/icons/list.svg" class="icon icon-home" /><span>Zamówienia </span> 
               @if($countorders>0)
                <span style='color:red'>{{$countorders}}</span>
                @endif
                <i class="icon-arrow"></i></a>
            </li>
             <li class="waves-effect waves-light">
               <a href="{{url('shop/customers')}}"><img src="/img/icons/avatar.svg" class="icon icon-home" /><span>Klienci</span><i class="icon-arrow"></i></a>
            </li>
             <li class="waves-effect waves-light">
               <a href="{{url('shop/products')}}"><img src="/img/icons/package.svg" class="icon icon-home" /><span>Produkty</span><i class="icon-arrow"></i></a>
            </li>
             <li class="waves-effect waves-light">
               <a href="{{url('shop/frames')}}"><img src="/img/icons/icon-puzzle.svg" class="icon icon-home" /><span>Kolory stelaży</span><i class="icon-arrow"></i></a>
            </li>
             <li class="waves-effect waves-light">
               <a href="{{url('shop/materials')}}"><img src="/img/icons/icon-puzzle.svg" class="icon icon-home" /><span>Kolory materiałów</span><i class="icon-arrow"></i></a>
            </li>
             <li class="waves-effect waves-light">
               <a href="{{url('shop/settings')}}"><img src="/img/icons/www.svg" class="icon icon-home" /><span>Ustawienia sklepu</span><i class="icon-arrow"></i></a>
            </li>
             <li class="waves-effect waves-light">
               <a href="{{url('shop/opinion')}}"><img src="/img/icons/contact.svg" class="icon icon-home" /><span>Opinie</span><i class="icon-arrow"></i></a>
            </li> 
            <li class="waves-effect waves-light">
               <a href="{{url('shop/newsletter')}}"><img src="/img/icons/new-brief.svg" class="icon icon-home" /><span>Newsletter</span><i class="icon-arrow"></i></a>
            </li>

            <script>
            $(function(){
    var current = location.pathname;
    $('.nav li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href').indexOf(current) !== -1){
            $this.addClass('router-link-exact-active router-link-active');
        }
    });
});
            </script>