<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







 // Authentication Routes...
 		Route::get('login', 'Auth\LoginShopController@showLoginForm')->name('login');
        Route::get('/verify-user/{code}', 'Auth\Shop\RegisterController@activateUser')->name('activate.user');

        Route::post('login', 'Auth\LoginShopController@login');
        Route::post('logout', 'Auth\LoginShopController@logout')->name('logout');


        // Registration Routes...
        Route::get('register', 'Auth\Shop\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Auth\Shop\RegisterController@register');

        // Password Reset Routes...
        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');
  		Route::get('logout', 'Auth\LoginShopController@logout')->name('getlogout');
/*****STRONA WYNIKOWA******/





/********CMS*********/


   // Authentication Routes...
        Route::get('cms/login', 'Auth\LoginCmsController@showLoginFormCms')->name('logincms');
        Route::post('cms/login', 'Auth\LoginCmsController@login');
        Route::post('cms/logout', 'Auth\LoginCmsController@logout')->name('logout');
        Route::get('cms/logout', 'Auth\LoginCmsController@logout')->name('getlogoutcms');

  

        // Password Reset Routes...
        Route::get('cms/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('cms/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('cms/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('cms/password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'cms','namespace' => 'Cms'], function () {
//dashboard
Route::get('cms/dashboard','Cms\CmsDashboardController@dashboard');



//*CMS//
Route::get('cms/modules','Cms\CmsDashboardController@modules');

    //** edycja uzytkownika*///
    Route::get('cms/user/{id}/editpassword','Cms\CmsUserController@editpassword');
    Route::put('cms/user/{id}/editpassword','Cms\CmsUserController@savepassword');
    Route::resource('cms/user','Cms\CmsUserController');

    //**edycja uzytkownika */

    //** panel admina*///
            //** uzytkownicy*///
        Route::get('cms/admin/users/{id}/editpassword','Cms\Admin\CmsAdminUsersController@editpassword');
        Route::put('cms/admin/users/{id}/editpassword','Cms\Admin\CmsAdminUsersController@savepassword');
        Route::get('cms/admin/users/{id}/editmodules','Cms\Admin\CmsAdminUsersController@editmodules');
        Route::post('cms/admin/users/{id}/editmodules','Cms\Admin\CmsAdminUsersController@savemodules');

            //logi-->
            Route::get('cms/admin/users/{id}/logs','Cms\Admin\CmsAdminUsersController@categorylogs');
            Route::get('cms/admin/users/{id}/logs/{category}','Cms\Admin\CmsAdminUsersController@listoflogs');
            //<--logi
        Route::resource('cms/admin/users','Cms\Admin\CmsAdminUsersController');
            //** uzytkownicy*///

            //** moduły*///
        Route::resource('cms/admin/modules','Cms\Admin\CmsAdminModulesController');
            //** moduły*///
            
            //** języki///
        Route::get('cms/admin/languages/{id}/edit/2','Cms\Admin\CmsAdminLanguagesController@image');
        Route::post('cms/admin/languages/{id}/edit/2','Cms\Admin\CmsAdminLanguagesController@uploadimage');
        Route::post('cms/admin/languages/{id}/edit/3','Cms\Admin\CmsAdminLanguagesController@removelanguageimage')->name('removelanguageimage');
        Route::resource('cms/admin/languages','Cms\Admin\CmsAdminLanguagesController');
            //** jezyki*///


     Route::resource('cms/admin','Cms\Admin\CmsAdminController');

    //**panel admina */

    //**moduł podstrony**//
    Route::get('cms/module/subpages/{lang}','Cms\Modules\CmsSubpagesModuleController@index');
    Route::get('cms/module/subpages/{lang}/create','Cms\Modules\CmsSubpagesModuleController@create');
    Route::put('cms/module/subpages/{lang}/{subpageid}/saveseo','Cms\Modules\CmsSubpagesModuleController@saveseo');
    Route::get('cms/module/subpages/{lang}/{subpageid}/editseo','Cms\Modules\CmsSubpagesModuleController@editseo');
    
     Route::post('cms/module/subpages/{lang}','Cms\Modules\CmsSubpagesModuleController@store');
    Route::get('cms/module/subpages/{lang}/{subpageid}/edit','Cms\Modules\CmsSubpagesModuleController@edit');
    Route::delete('cms/module/subpages/{lang}/{subpageid}/delete','Cms\Modules\CmsSubpagesModuleController@destroy');
    Route::put('cms/module/subpages/{lang}/{subpageid}','Cms\Modules\CmsSubpagesModuleController@update');
     Route::post('cms/module/subpages/{lang}/sort','Cms\Modules\CmsSubpagesModuleController@subpageSort')->name('SubpagePLSort');
   
    
    // Route::resource('cms/module/subpages/{lang}','Cms\Modules\CmsSubpagesModuleController');
               
   

       //**moduł komponenty**//
       Route::get('cms/module/subpages/{lang}/{subpageid}/components/choosetemplate','Cms\Modules\CmsComponentsModuleController@choosetemplate');
       Route::get('cms/module/subpages/{lang}/{subpageid}/components/{main}/choosetemplate','Cms\Modules\CmsComponentsModuleController@choosetemplatemain');
         Route::get('cms/module/subpages/{lang}/{subpageid}/components/choosecomponent','Cms\Modules\CmsComponentsModuleController@choosecomponent');
           Route::get('cms/module/subpages/{lang}/{subpageid}/components/{main}/choosecomponent','Cms\Modules\CmsComponentsModuleController@choosecomponentmain');

       Route::get('cms/module/subpages/{lang}/{subpageid}/components/choosetemplate/{template}','Cms\Modules\CmsComponentsModuleController@showtemplate');
        Route::get('cms/module/subpages/{lang}/{subpageid}/components/{main}/choosetemplate/{template}','Cms\Modules\CmsComponentsModuleController@showtemplatemain');
   Route::get('cms/module/subpages/{lang}/{subpageid}/components/choosecomponent/{template}','Cms\Modules\CmsComponentsModuleController@showcomponent');
   Route::get('cms/module/subpages/{lang}/{subpageid}/components/{main}/choosecomponent/{template}','Cms\Modules\CmsComponentsModuleController@showcomponentmain');


       Route::post('cms/module/subpages/{lang}/{subpageid}/components/chosentemplate','Cms\Modules\CmsComponentsModuleController@chosentemplate');
        Route::post('cms/module/subpages/{lang}/{subpageid}/components/{main}/chosentemplate','Cms\Modules\CmsComponentsModuleController@chosentemplatemain');
        Route::post('cms/module/subpages/{lang}/{subpageid}/components/chosencomponent','Cms\Modules\CmsComponentsModuleController@chosencomponent');
        Route::post('cms/module/subpages/{lang}/{subpageid}/components/{main}/chosencomponent','Cms\Modules\CmsComponentsModuleController@chosencomponentmain');

        Route::post('cms/module/subpages/{lang}/{subpageid}/components/copycomponent','Cms\Modules\CmsComponentsModuleController@copycomponent');
        Route::post('cms/module/subpages/{lang}/{subpageid}/components/{main}/copycomponent','Cms\Modules\CmsComponentsModuleController@copycomponentmain');
       Route::post('cms/module/subpages/{lang}/{subpageid}/components/copytemplate','Cms\Modules\CmsComponentsModuleController@copytemplate');
       Route::post('cms/module/subpages/{lang}/{subpageid}/components/{main}/copytemplate','Cms\Modules\CmsComponentsModuleController@copytemplatemain');

       Route::post('cms/module/subpages/{lang}/{subpageid}/components/list/sort','Cms\Modules\CmsComponentsModuleController@componentSort')->name('ComponentPLSort');
       Route::get('cms/module/subpages/{lang}/{subpageid}/components/list','Cms\Modules\CmsComponentsModuleController@list');
       Route::get('cms/module/subpages/{lang}/{subpageid}/components/list/{new}','Cms\Modules\CmsComponentsModuleController@listnew');
      Route::get('cms/module/subpages/{lang}/{subpageid}/components/{id}/edit','Cms\Modules\CmsComponentsModuleController@edit');
      
      Route::post('cms/module/subpages/{lang}/{subpageid}/components/{id}/remove','Cms\Modules\CmsComponentsModuleController@destroy')->name('removecomponent');
      Route::get('cms/module/subpages/{lang}/{subpageid}/components/{id}','Cms\Modules\CmsComponentsModuleController@show');
       Route::post('cms/module/subpages/{lang}/{subpageid}/components/{id}/update','Cms\Modules\CmsComponentsModuleController@update');
      Route::post('cms/module/subpages/{lang}/{subpageid}/components/{id}/edit/showimage','Cms\Modules\CmsComponentsModuleController@showgalleryimage')->name('showgalleryimage');
   
     Route::resource('cms/module/subpages/{lang}/{subpageid}/components','Cms\Modules\CmsComponentsModuleController');
  

                
        //**moduł komponenty**//


        
     //**moduł podstrony**//

    //**moduł szablony-->//
     Route::get('cms/module/templates/{id}/edit/2','Cms\Modules\CmsTemplatesModuleController@components');
          Route::post('cms/module/templates/edit/showimage','Cms\Modules\CmsTemplatesModuleController@showgalleryimage')->name('showgalleryimageSEO');
          //componenty-->
         // Route::get('cms/module/templates/{id}/components','Cms\Modules\CmsTemplatesComponentsModuleController@list');
           Route::post('cms/module/templates/{id}/components/list/sort','Cms\Modules\CmsTemplatesComponentsModuleController@componentSort')->name('TemplateComponentSort');
          Route::get('cms/module/templates/{id}/components/choosetemplate','Cms\Modules\CmsTemplatesComponentsModuleController@choosetemplate');
          Route::get('cms/module/templates/{id}/components/choosetemplate/{template}','Cms\Modules\CmsTemplatesComponentsModuleController@showtemplate');

           Route::post('cms/module/templates/{id}/components/chosentemplate','Cms\Modules\CmsTemplatesComponentsModuleController@chosentemplate');
           Route::post('cms/module/templates/{id}/components/copytemplate','Cms\Modules\CmsTemplatesComponentsModuleController@copytemplate');

            Route::post('cms/module/templates/{template}/components/{component}/{archive}/copyarchive','Cms\Modules\CmsTemplatesComponentsModuleController@copyarchive');

            Route::post('cms/module/templates/{template}/components/{id}/chosenarchive','Cms\Modules\CmsTemplatesComponentsModuleController@chosenarchive');
             Route::get('cms/module/templates/{template}/components/{id}/showarchive','Cms\Modules\CmsTemplatesComponentsModuleController@showarchive');
          Route::get('cms/module/templates/{template}/components/{id}/choosearchive/{archive}','Cms\Modules\CmsTemplatesComponentsModuleController@choosearchive');

          Route::delete('cms/module/templates/{template}/components/{id}/choosearchive/{archive}','Cms\Modules\CmsTemplatesComponentsModuleController@destroyarchive');

           Route::resource('cms/module/templates/{template}/components','Cms\Modules\CmsTemplatesComponentsModuleController');
         //<--componenty
         
     Route::get('cms/module/templates/editmaster','Cms\Modules\CmsTemplatesModuleController@editmaster');
     Route::put('cms/module/templates/savemaster','Cms\Modules\CmsTemplatesModuleController@savemaster');
     Route::resource('cms/module/templates','Cms\Modules\CmsTemplatesModuleController');
    //<--moduł szablony**//

     //moduł blog**/
       Route::get('cms/module/blog/{id}/header','Cms\Modules\CmsBlogPostsModuleController@showheader');
      Route::post('cms/module/blog/{id}/header/{gallery}','Cms\Modules\CmsBlogPostsModuleController@chooseheaderimages');
      Route::get('cms/module/blog/{id}/header/{gallery}','Cms\Modules\CmsBlogPostsModuleController@showheaderimages');

      Route::get('cms/module/blog/{id}/thumbnail','Cms\Modules\CmsBlogPostsModuleController@showthumbnail');
      Route::post('cms/module/blog/{id}/thumbnail/{gallery}','Cms\Modules\CmsBlogPostsModuleController@choosethumbnailimages');
      Route::get('cms/module/blog/{id}/thumbnail/{gallery}','Cms\Modules\CmsBlogPostsModuleController@showthumbnailimages');
      Route::resource('cms/module/blog','Cms\Modules\CmsBlogPostsModuleController');
      //moduł blog**/

    //-->moduł galeria**//
    
      Route::get('cms/module/galleries/{id}/images','Cms\Modules\CmsGalleriesModuleController@showimages');
      Route::get('cms/module/galleries/{id}/delete','Cms\Modules\CmsGalleriesModuleController@destroy');
      Route::post('cms/module/galleries/{id}/uploadimage','Cms\Modules\CmsGalleriesModuleController@uploadimage');
      Route::post('cms/module/galleries/{id}/removeimage','Cms\Modules\CmsGalleriesModuleController@removeimage')->name('removegalleryimage');
      Route::post('cms/module/galleries/{id}/changenameimage','Cms\Modules\CmsGalleriesModuleController@changenameimage')->name('changenamegalleryimage');
      Route::resource('cms/module/galleries','Cms\Modules\CmsGalleriesModuleController');
           
     //<--moduł galeria//


       //-->moduł pliki do pobrania**//
    
       
       Route::post('cms/module/files_to_download/uploadimage','Cms\Modules\CmsFilesToDownloadModuleController@uploadimage');
       Route::post('cms/module/files_to_download/removeimage','Cms\Modules\CmsFilesToDownloadModuleController@removeimage')->name('removefile');
       Route::post('cms/module/files_to_download/changenameimage','Cms\Modules\CmsFilesToDownloadModuleController@changenameimage')->name('changenamefile');
       Route::resource('cms/module/files_to_download','Cms\Modules\CmsFilesToDownloadModuleController');
            
      //<--moduł pliki do pobrania//

//*CMS//



});

 Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });


/********CMS*********/


/*****STRONA WYNIKOWA******/


Route::group(['namespace' => 'Page\Cms'], function () {

  //strona główna-->
  Route::resource('/','CmsPageController');
  //<--strona główna
  //menu-->
  Route::get('/{id}','CmsPageController@menu');
 // Route::get('/{id}/{menu}/{submenu}','CmsPageController@submenu')->where('id','!=','cms');
  //<--menu
  
      Route::get('/{lang}/{menu}','CmsPageController@lang');
  
  
  
  
  
  


});
