// Add header, footer and widgets do websites
  // $(function(){
  //   $("#header").load("header.html");
  //   $("#widget-companies").load("widget-companies.html");
  //   $("#footer").load("footer.html");
  // });


$(document).ready(function() {

  // Current link in nav
    // var path = $(location).attr('href');
    // console.log(path);
    // console.log(typeof path === "string");
    // $('#navbarsExampleDefault .nav-item').removeClass('current');
    //
    //
    // $("#navbarsExampleDefault .nav-item .nav-link").on("click", function(){
    //   console.log("click");
    //   if (path.indexOf("about") >= 0){
    //     $("#navbarsExampleDefault .nav-link[href='about.html']").parent().addClass('current');
    //     console.log("about is active");
    //   }else if(path.indexOf("blog") >= 0){
    //     $("#navbarsExampleDefault .nav-link[href='blog.html']").parent().addClass('current');
    //       console.log("blog is active");
    //   }else if(path.indexOf("offer") >= 0){
    //     $("#navbarsExampleDefault .nav-link[href='offer.html']").parent().addClass('current');
    //       console.log("offer is active");
    //   }else if(path.indexOf("realizations") >= 0){
    //     $("#navbarsExampleDefault .nav-link[href='realizations.html']").parent().addClass('current');
    //       console.log("realizations is active");
    //   }else if(path.indexOf("contact") >= 0){
    //     $("#navbarsExampleDefault .nav-link[href='contact.html']").parent().addClass('current');
    //       console.log("contact is active");
    //   }else{
    //     $("#navbarsExampleDefault .nav-link[href='home.html']").parent().addClass('current');
    //       console.log("home is active");
    //   }
    // })


  // Add "scroll" class to header
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("header");
  var sticky = header.offsetTop;

  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  }


  // Slick (Logo carousel)
  $('#logo-carousel').slick({
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 1200,
    autoplaySpeed: 1000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  //Form newsletter
 //  $('#form').submit(function(e) {
 //    e.preventDefault();
 //    var email = $('#form_email').val();
 //
 //    $.ajax({
 //      url: 'http://'+window.location.hostname+'/newsletter.php',
 //      type: 'POST',
 //      dataType: 'json',
 //      data: '&email=' + email,
 //    }).done(function() {
 //        $('#form').after('<p class="text-success text-center">Dzikujemy za zgłoszenie do newslettera</p>');
 //        $('#form input[type="email"]').val('');
 //      }
 //    );
 //  }
 // });


  // Swipe slider with jQuery mobile
  $("#slide-carousel").swiperight(function() {
      $(this).carousel('prev');
    });
   $("#slide-carousel").swipeleft(function() {
      $(this).carousel('next');
   });

});
