$(document).ready(function () {

    $('img[src$=".svg"].svg-icon').each(function () {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function (data) {
            var $svg = jQuery(data).find('svg');

            $svg = $svg.removeAttr('xmlns:a');

            $.each(attributes, function () {
                $svg.attr(this.name, this.value);
            });

            $img.replaceWith($svg);
        }, 'xml');
    });

    $(".change-quantity").on("click", function () {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find("input").val(newVal);
    });

    //configurator color change 
    $('#colors-groups1 .color-sample img').on('click', function() {
        let colorImageSrc = $(this).attr('src');
		let colorImageName = $(this).closest('.color-sample').find('span').text();
		let colorImagePrice = $(this).closest('.color-sample').find('.color-price').text();
        $('.selected-color-image1').attr('src', colorImageSrc);
		 $('.selected-color-name1').text(colorImageName);
		 
		  frame=$(this).closest('.color-sample').find('.frameid').val();
          $("input[name='frames']").val(frame);

		  $('#selected-color-price1').text(colorImagePrice);
		 
    });
    $('#colors-groups2 .color-sample img').on('click', function() {
        let colorImageSrc = $(this).attr('src');
		let colorImageName = $(this).closest('.color-sample').find('span').text();
		let colorImagePrice = $(this).closest('.color-sample').find('.color-price').text();
        $('.selected-color-image2').attr('src', colorImageSrc);
		$('.selected-color-name2').text(colorImageName);
		 

          material=$(this).closest('.color-sample').find('.materialid').val();
          $("input[name='materials']").val(material);
        $('#selected-color-price2').text(colorImagePrice);
       


    });




    // 

    $('#toggle-search-container').on("click", function () {
        $('#mobile-navigation').toggleClass('search-visible');
    });

    var navbar = $('#mobile-navigation');

    $(window).scroll(function () {
        var a = $(window).scrollTop();        
        if (a > 100) {
            navbar.removeClass("search-visible");
        } 
    });

    var menu_open = document.getElementById('hamburger-checkbox');
    menu_open.addEventListener('click', function () {
        if(menu_open.checked) {
            $('#mobile-navigation').addClass('active');
        } else {
            $('#mobile-navigation').removeClass('active');
        }
    });

    //toggle quick-cart
    $("#header__cart").on("click", function() {
        $(".quick-cart__container, .cart-overlay").fadeToggle( "fast");
    });
    $(".cart-overlay, #close-quick-cart").on("click", function() {
        $(".quick-cart__container, .cart-overlay").fadeToggle( "fast");
    });
    //
});

// Cart shipping button activate to change current shipping adrress

$("#button-change_address").on("click", function () {
    var address = document.querySelector(".shipping__address--text");
    var button = document.getElementById("button-change_address");
    var form = document.getElementById("form-change_address");
    console.log(address);
    if (button.checked) {
        address.style.display = "none";
        form.style.display = "block";
    }
    else {
        address.style.display = "block";
        form.style.display = "none";
    }
   
});


// Form validation

function validateForm(req) {

    // Global consts
    const className = 'error';
    const regEmail = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    const email = document.querySelector('.form-validate input[name="email"]')
    const pass = document.querySelector('.form-validate input[name="password"]')
    const passConfirm = document.querySelector('.form-validate input[name="password_confirm"]')
    const alertClass = document.getElementById("error-login");
    const alertClass2 = document.getElementById("error-checkbox");
    const checkbox = document.querySelector('.form-validate input[name="acceptable"]')

    // Check input values changes in flow
    addEventListener('change', function () {
        var correct = 0;
        // Prevent check empty value
        if (email.value.length > 0) {
            correct = checkEmail(correct);
        }
        // Prevent check empty value
        if (pass.value.length > 0) {
            correct = checkPassword(correct);
        }
        // Prevent check empty value
        if (passConfirm != undefined) {
            if (passConfirm.value.length > 0) {
                correct = checkPasswordConfirm(correct);
            }
        }

        // Function check if error add vsible class to error message
        function error() {
            if (correct > 0) {
                alertClass.classList.add("active");
                return true;
            } else {
                alertClass.classList.remove("active");
                return false;
            }
        }
        console.log(correct);
        if (passConfirm != undefined) {
            checkCheckbox();
        }
        error();
    });

    // Validate email
    function checkEmail(correct) {
        if (!regEmail.test(email.value)) {
            email.classList.add(className);
            return correct += 1;
        } else {
            email.classList.remove(className);
            return correct;
        }
    }
    // Validate password
    function checkPassword(correct) {
        if (pass.value.length <= 3) {
            pass.classList.add(className);
            return correct += 1;
        } else {
            pass.classList.remove(className);
            return correct;
        }
    }
    // Validate confirm email
    function checkPasswordConfirm(correct) {
        if (passConfirm.value !== pass.value) {
            passConfirm.classList.add(className);
            return correct += 1;
        } else {
            passConfirm.classList.remove(className);
            return correct;
        }
    }
    // Validate checkbox
    function checkCheckbox() {
        if (checkbox.checked !== true) {
            alertClass2.classList.add("active");
        } else {
            alertClass2.classList.remove("active");
        }
    }

    // Check input required
    function inputReq() {
        let inputs = document.querySelectorAll('.form-validate input[type="text"]');

        for (let i = 0; i < inputs.length; i++) {

            if ((inputs[i].required == true) && (inputs[i].value == "")) {
                console.log(inputs[i].name + ' true')
                inputs[i].classList.add(className);
                // alertClass.classList.add("active");
            } else {
                console.log(inputs[i].name + ' false')
                inputs[i].classList.remove(className);
                //  alertClass.classList.remove("active");
            }
        }
    }
    // Fire on click :)
    if (req == 1) {
        inputReq();
    }

}
validateForm(0);

// My account on small device crop date format
window.addEventListener("resize", checkWidth);

function cropDate() {
    const dates = document.querySelectorAll('.item .item-data');
    for (let i = 0; i < dates.length; i++) {
        var str = dates[i].innerText.split("-");
        if (str.length > 2) {
            str.pop();
        }
        dates[i].innerText = str[0] + '-' + str[1];
    }

}

function checkWidth() {
    var w = window.innerWidth;
    if (w <= 560) {
        cropDate();
    }
};


var icon = {
    path: "M256,0C153.8,0,70.6,83.2,70.6,185.4c0,126.9,165.9,313.2,173,321c6.6,7.4,18.2,7.4,24.8,0c7.1-7.9,173-194.1,173-321 C441.4,83.2,358.2,0,256,0z M256,278.7c-51.4,0-93.3-41.9-93.3-93.3s41.9-93.3,93.3-93.3s93.3,41.9,93.3,93.3 S307.4,278.7,256,278.7z",
    fillColor: '#c39835',
    // anchor: new google.maps.Point(0,10),
    fillOpacity: 1,
    strokeWeight: 0,
    scale: .05
}

var map;
function initMap() {
    var myLatLng = {lat: 49.8922836, lng: 22.3070307};

    map = new google.maps.Map(document.getElementById('contact-map'), {
        center: myLatLng,
        zoom: 14,
        styles: [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "lightness": "70"
                    },
                    {
                        "saturation": "0"
                    },
                    {
                        "gamma": "1.34"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ba9450"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d2ab67"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2e6d2"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#c3d6e4"
                    }
                ]
            }
        ]
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: icon,
        title: 'Gmyrex'
    });
}




