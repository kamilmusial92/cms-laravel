

  CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.filebrowserBrowseUrl = '/laravel-filemanager?type=Files';
    CKEDITOR.config.filebrowserUploadUrl = '/laravel-filemanager/upload?type=Files&_token=';
    var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };


    CKEDITOR.config.height = '400px';
     CKEDITOR.config.entities = false;
    if ($('#editor1').length) {
        var editor = CKEDITOR.replace('editor1',options);
        CKEDITOR.config.height = '500px';

    }
    if ($('#editor2').length) {
        var editor = CKEDITOR.replace('editor2',options);
        CKEDITOR.config.height = '500px';
    }

    if ($('#codeeditor').length) {
        var vseditor = CKEDITOR.replace('codeeditor', {
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
            allowedContent: true,
            enterMode: CKEDITOR.ENTER_BR,
            height: '600px',
            removePlugins: 'htmldataprocessor',
            forcePasteAsPlainText: true,
            entities: false,
            basicEntities: false,
            startupMode: 'source'
        });
    }
   