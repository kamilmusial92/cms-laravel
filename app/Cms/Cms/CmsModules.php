<?php

namespace App\Cms\Cms;

use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\Admin\CmsUsersChosenModules;
use Auth;
class CmsModules extends Model
{
           protected $fillable=[
'name','url'
    ];

    public static function chosen($user){

        $module=CmsUsersChosenModules::where('user_id',$user)->orderBy('sort','ASC')->get();

        return $module;
    
    }

    public static function chosenuser($user,$module){

        $module=CmsUsersChosenModules::where('user_id',$user)->where('module_id',$module)->first();

        return $module;
    
    }

    public static function active()
    {
             $module=$_SERVER['REQUEST_URI'];
        $module=substr($module,strpos($module,'module/'));

        $module=substr($module,strpos($module,'/'));
        $module=substr($module,1);
        $module1=substr($module,0,strpos($module,'/'));

        if(!empty($module1))
        {
            $module=$module1;
        }
        else{
           $module=$module; 
        }

      

       
        
        $module=CmsModules::where('url',$module)->first();
        return $module;
    }

   

}
