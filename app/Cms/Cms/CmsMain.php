<?php

namespace App\Cms\Cms;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\CmsLanguages;
use App\Cms\Cms\Admin\CmsUsersChosenModules;
class CmsMain extends Model
{
   
         public static function replaceurl($string)
    {
 $notAllowedCharacters = array(' ', '_' ,'ą', 'ć', 'ę', 'ó', 'ł', 'ń', 'ż', 'ź', 'ś','Ą','Ć','Ę','Ó','Ł','Ń','Ż','Ź','Ś',
                            '?', '!', '.', ',', ':', ';', '!', '@', '#', '$', '%', '^', '*', '(', ')', '+','=', '<', '>', '~', '`', '/', '\\', '{', '}', '[', ']', '\'', '\'', '|', '"');                       
$substitutes = array('-', '-', 'a', 'c', 'e', 'o', 'l', 'n', 'z', 'z', 's','a', 'c', 'e', 'o', 'l', 'n', 'z', 'z', 's',
                            '');
                            

return str_replace($notAllowedCharacters, $substitutes, $string);
    }

    public static function module()
    {
            $module=$_SERVER['REQUEST_URI'];
        $module=substr($module,strpos($module,'module/'));

        $module=substr($module,strpos($module,'/'));
        $module=substr($module,1);
        $module1=substr($module,0,strpos($module,'/'));
        if(!empty($module1))
        {
            $module=$module1;
        }
        else{
           $module=$module; 
        }

      

       
        
        $module=CmsModules::where('url',$module)->first();

        $checkmodule=CmsUsersChosenModules::where('user_id',Auth::user()->id)->where('module_id',$module->id)->first();
        if(empty($checkmodule))
        {
           abort('404');
        }
    }


      public static function mainlang()
    {
            $module=$_SERVER['PHP_SELF'];
        $module=substr($module,strpos($module,'module/'));

        $module=substr($module,strpos($module,'/'));
        $module=substr($module,1);
        $module1=substr($module,0,strpos($module,'/'));
         $module1=substr($module,strpos($module,'/'),strpos($module,'/'));
          $module1=substr($module1,1,2);
      


       
        
        $lang=CmsLanguages::where('url',$module1)->first();

       
        if(empty($lang))
        {
           abort('404','Wersja językowa niedostępna.');
        }
        else{
            return $lang;
        }
    }

}
