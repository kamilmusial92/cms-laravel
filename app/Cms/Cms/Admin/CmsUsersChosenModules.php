<?php

namespace App\Cms\Cms\Admin;

use Illuminate\Database\Eloquent\Model;

class CmsUsersChosenModules extends Model
{
    protected $fillable=[
       'module_id','user_id','sort'
            ];

     

       public function module(){
    	 return $this->belongsTo('App\Cms\Cms\CmsModules');
    }    
}
