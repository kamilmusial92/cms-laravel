<?php

namespace App\Cms\Cms\Admin;

use Illuminate\Database\Eloquent\Model;
use Auth;
class CmsLogs extends Model
{
  protected $fillable=[
       'user_id','text','module_id'
            ];

     public static function CreateLog($module,$text)
     {
     	Cmslogs::create(['user_id'=>Auth::user()->id,'text'=>$text,'module_id'=>$module]);
     }
}
