<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\Modules\CmsComponentsModule;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
class CmsSubpagesModule extends Model
{
    protected $fillable=[
       'name','page_url','lang_id','title_page','description_page','keywords_page','sort','image_page'
            ];


                 public function image($id)
     {
        return CmsGalleriesImagesModule::findorFail($id);
     }
    
      

}
