<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\Modules\CmsTemplatesComponentModule;
use App\Cms\Cms\Modules\CmsTemplatesComponentArchivesModule;
class CmsBoxTypes extends Model
{
  
    public static function chosenboxes($id,$component_id){

        $box=CmsTemplatesComponentModule::chosenBoxes($id);
        $check=CmsTemplatesComponentModule::where($box,1)->where('id',$component_id)->first();
        
       if(!empty($check))
        return true;
    
    }

     public static function chosenboxesarchive($id,$component_id){

        $box=CmsTemplatesComponentArchivesModule::chosenBoxes($id);
        $check=CmsTemplatesComponentArchivesModule::where($box,1)->where('id',$component_id)->first();
        
       if(!empty($check))
        return true;
    
    }

    public static function checkboxes($id){

        $box=CmsTemplatesComponentModule::chosenBoxes($id);
      
        
       
        return $box;
    
    }

}
