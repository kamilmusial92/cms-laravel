<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
class CmsTemplatesModule extends Model
{
    protected $fillable=[
       'name','path','title_page','description_page','keywords_page','image_page'
            ];

              public function image($id)
     {
        return CmsGalleriesImagesModule::findorFail($id);
     }
}
