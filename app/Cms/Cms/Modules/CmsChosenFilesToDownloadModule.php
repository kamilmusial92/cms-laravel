<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;

class CmsChosenFilesToDownloadModule extends Model
{
  
      protected $fillable=[
        'component_id','file_id'
             ];


         public function file()
         {
         	 return $this->belongsTo('App\Cms\Cms\Modules\CmsFilesToDownloadModule','file_id','id');
         }
}
