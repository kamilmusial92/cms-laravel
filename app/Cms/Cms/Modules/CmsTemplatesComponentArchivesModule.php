<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;

class CmsTemplatesComponentArchivesModule extends Model
{
    protected $fillable=[
      'name','component_id','template_id','copied_template_id','content','path','header_status','background_status','title_status','subtitle_status','text_status','image_status','gallery_status','footer_status','url_status','sort','user_visible','file_status'
            ];
public static function chosenBoxes($id)
            {	
            	if($id==1)
            	{
            		$text='header_status';
            	}
            	elseif($id==2)
            	{
					$text='background_status';
            	}
            	elseif($id==3)
            	{
					$text='title_status';
            	}
            	elseif($id==4)
            	{
					$text='subtitle_status';
            	}
            	elseif($id==5)
            	{
					$text='text_status';
            	}
            	elseif($id==6)
            	{
					$text='image_status';
            	}
            	elseif($id==7)
            	{
					$text='gallery_status';
            	}
            	elseif($id==8)
            	{
					$text='footer_status';
            	}
            		elseif($id==9)
            	{
					$text='url_status';
							}
							elseif($id==10)
            	{
					$text='file_status';
            	}
				return $text;

            }
            
       
}
