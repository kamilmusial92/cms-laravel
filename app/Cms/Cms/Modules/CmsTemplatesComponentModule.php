<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\Modules\CmsTemplatesModule;
class CmsTemplatesComponentModule extends Model
{
     protected $fillable=[
      'name','template_id','copied_template_id','copied_template_id','content','path','header_status','background_status','title_status','subtitle_status','text_status','image_status','gallery_status','footer_status','url_status','sort','user_visible','file_status','status'
            ];


          public static function chosenBoxes($id)
            {	
            	if($id==1)
            	{
            		$text='header_status';
            	}
            	elseif($id==2)
            	{
					$text='background_status';
            	}
            	elseif($id==3)
            	{
					$text='title_status';
            	}
            	elseif($id==4)
            	{
					$text='subtitle_status';
            	}
            	elseif($id==5)
            	{
					$text='text_status';
            	}
            	elseif($id==6)
            	{
					$text='image_status';
            	}
            	elseif($id==7)
            	{
					$text='gallery_status';
            	}
            	elseif($id==8)
            	{
					$text='footer_status';
            	}
            		elseif($id==9)
            	{
					$text='url_status';
							}
							elseif($id==10)
            	{
					$text='file_status';
            	}
				return $text;

            }

			public static function chosenBoxesComponent($id)
            {
				$box=CmsTemplatesComponentModule::findorFail($id);
				$header='';
				$background='';
				$title='';
				$subtitle='';
				$text='';
				$image='';
				$gallery='';
				$footer='';
				$url='';
				$file='';
				if($box->header_status==1)
				{
					$header='nagłówek, ';
				}
				if($box->background_status==1)
				{
					$background='tło, ';
				}
				if($box->title_status==1)
				{
					$title='tytuł, ';
				}
				if($box->subtitle_status==1)
				{
					$subtitle='podtytuł, ';
				}
				if($box->text_status==1)
				{
					$text='tekst, ';
				}
				if($box->image_status==1)
				{
					$image='zdjęcie, ';
				}
				if($box->gallery_status==1)
				{
					$gallery='galeria, ';
				}
				if($box->footer_status==1)
				{
					$footer='stopka,';
				}
				if($box->url_status==1)
				{
					$footer='odnośnik,';
				}
				if($box->file_status==1)
				{
					$file='pliki';
				}

				return $header.$background.$title.$subtitle.$text.$image.$gallery.$footer.$url.$file;
			}

			public function template($id){

				return CmsTemplatesModule::findorFail($id);
			}

			public function template_module()
			{
				return $this->belongsTo('App\Cms\Cms\Modules\CmsTemplatesModule','template_id','id');
			}

}
