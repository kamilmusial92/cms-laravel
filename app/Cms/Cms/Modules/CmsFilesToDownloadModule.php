<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;

class CmsFilesToDownloadModule extends Model
{
    protected $fillable=[
        'user_id','file_path','file_name'
            ];
}
