<?php

namespace App\Cms\Cms\Modules;
use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
class CmsBlogPostsModule extends Model
{
     protected $fillable=[
        'title','url','long_text','user_id','thumbnail_id','image_id','visible','publish_at','info','tags','keywords'
            ];
    public function status($status){
        if($status==1)
        {
            return "Tak";
        }
        elseif($status==0)
        {
return "Nie";
        }
    }

    public function user($id){
        return User::findorFail($id);
    }

      public function headerImage()
    {
        return $this->hasMany('App\API\CmsGalleriesImagesModule','id','image_id');
    }

      public function thumnbailImage()
    {
        return $this->hasMany('App\API\CmsGalleriesImagesModule','id','thumbnail_id');
    }

     public function picture($id)
     {
     	return CmsGalleriesImagesModule::findorFail($id);
     }
}
