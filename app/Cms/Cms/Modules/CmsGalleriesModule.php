<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
class CmsGalleriesModule extends Model
{
      protected $fillable=[
        'user_id','name'
            ];

           public static function firstpicture($id){
           	return CmsGalleriesImagesModule::where('gallery_id',$id)->first();
           }
}
