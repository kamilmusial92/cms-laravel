<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;

class CmsGalleriesImagesModule extends Model
{
      protected $fillable=[
        'user_id','gallery_id','file_path','file_name','alt'
            ];

    public function DeleteImage($id)
    {
         $componentsbackground=CmsComponentsModule::where('background',$id)->get();
        foreach($componentsbackground as $comonentb)
        {
            $comonentb->update(['background'=>0]);
        }

       
        $componentsimages=CmsComponentsModule::where('image',$id)->get();
         foreach($componentsimages as $comonenti)
        {
            $comonenti->update(['image'=>0]);
        }

        $templateimages=CmsTemplatesModule::where('image_page',$id)->get();
         foreach($templateimages as $tempimg)
        {
            $tempimg->update(['image_page'=>0]);
        }

          $subpageimages=CmsSubpagesModule::where('image_page',$id)->get();
         foreach($subpageimages as $subimg)
        {
            $subimg->update(['image_page'=>0]);
        }

         $blogthumbs=CmsBlogPostsModule::where('thumbnail_id',$id)->get();
         foreach($blogthumbs as $blogthumb)
        {
            $blogthumb->update(['thumbnail_id'=>0]);
        }

         $blogimages=CmsBlogPostsModule::where('image_id',$id)->get();
         foreach($blogimages as $blogimage)
        {
            $blogimage->update(['image_id'=>0]);
        }


    }	

}
