<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;

class CmsTemplatesMastersModule extends Model
{
    protected $fillable=[
        'name','content'
            ];
}
