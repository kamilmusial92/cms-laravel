<?php

namespace App\Cms\Cms\Modules;

use Illuminate\Database\Eloquent\Model;
use App\Cms\Cms\CmsMainComponents;
use App\Cms\Cms\Modules\CmsGalleriesModule;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
use App\Cms\Cms\Modules\CmsTemplatesModule;
use App\Cms\Cms\Modules\CmsBlogPostsModule;

class CmsComponentsModule extends Model
{
    
    protected $fillable=[
        'subpage_id','template_component_id','name','header','background','title','subtitle','text','image','gallery','url','sort','status','copyable','copyable_id'
             ];

     public function template_module()
     {
     	  return $this->belongsTo('App\Cms\Cms\Modules\CmsTemplatesComponentModule','template_component_id','id');
     }

         public function subpage()
    {
        return $this->hasMany('App\Cms\Cms\Modules\CmsSubpagesModule','id','subpage_id');
    }

     public function Gallery($id)
     {
       return CmsGalleriesModule::findorFail($id);

     }

      public function GalleryImage($id)
     {
       return CmsGalleriesImagesModule::where('gallery_id',$id)->get();

     }


     public function image($id)
     {
        return CmsGalleriesImagesModule::findorFail($id);
     }

     public function view()
     {
      $path=$this->template_module->path;
       $template=$this->template_module->template_module->path;
       return 'page/templates/'.$template.'.'.$path;
     }

     public function imgsrc($id,$class)
     {
       if(!empty($id))
        return "<img src='".CmsGalleriesImagesModule::findorFail($id)->file_path."/".CmsGalleriesImagesModule::findorFail($id)->file_name."' alt='".CmsGalleriesImagesModule::findorFail($id)->alt."' class='".$class."'>";
     }

       public function backgroundsrc($id)
     {
       if(!empty($id))
        return CmsGalleriesImagesModule::findorFail($id)->file_path."/".CmsGalleriesImagesModule::findorFail($id)->file_name;
     }

      public function galleries($id)
     {
        $list=CmsGalleriesImagesModule::where('gallery_id',$id)->get();

        foreach($list as $image) 
        {
          echo  "<img src='".$image->file_path."/".$image->file_name."'>";
        }
     }

       public function blogposts()
     {
       return $list=CmsBlogPostsModule::orderBy('publish_at','DESC')->get();

     
     }



     public static function chosenBoxesComponent($id,$component)
            {
        $box=CmsTemplatesComponentModule::findorFail($id);
        $component=CmsComponentsModule::findorfail($component);
        $header='';
        $background='';
        $title='';
        $subtitle='';
        $text='';
        $image='';
        $gallery='';
        $footer='';
        $url='';
        $file='';
        if($box->header_status==1)
        {
          $header='<b>nagłówek:</b> '.$component->header.', ';
        }
        if($box->background_status==1)
        {
          $background='<b>tło:</b>, '.$component->background.' ';
        }
        if($box->title_status==1)
        {
          $title='<b>tytuł:</b> '.$component->title.' , ';
        }
        if($box->subtitle_status==1)
        {
          $subtitle='<b>podtytuł:</b> '.$component->subtitle.', ';
        }
        if($box->text_status==1)
        {
          $text='<b>tekst:</b> '.substr(strip_tags($component->text),0,30).', ';
        }
        if($box->image_status==1)
        {
          $image='<b>zdjęcie:</b> ';
          if(!empty($component->image))
          {


          $image.="<img style='height:80px;' src='/".CmsGalleriesImagesModule::findorFail($component->image)->file_path."".CmsGalleriesImagesModule::findorFail($component->image)->file_name."'>";
          }
          $image.=', ';
        }
        if($box->gallery_status==1)
        {
          $gallery='<b>galeria:</b> '.$component->gallery.', ';
        }
        if($box->footer_status==1)
        {
          $footer='<b>stopka:</b> '.$component->footer.',';
        }
        if($box->url_status==1)
        {
          $url='<b>odnośnik:</b> '.$component->url.',';
        }
        if($box->file_status==1)
        {
          $file='<b>pliki:</b> ';
        }

        return $header.$background.$title.$subtitle.$text.$image.$gallery.$footer.$url.$file;
      }


}
