<?php

namespace App\Page\Shop;

use Illuminate\Database\Eloquent\Model;

class ShopCustomer extends Model
{
      protected $fillable = [
        'first_name','last_name','user_id','phone','city','address','postcode','country',
        'phone_delivery','first_name_delivery','last_name_delivery','address_delivery','city_delivery','postcode_delivery','country_delivery','company_name','nip','nip_delivery','account_type','company_name_delivery'
    ];
     
   
}
