<?php

namespace App\Page\Shop;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Cookie;
use App\Page\Shop\ShopOrderProducts;

class ShopHome extends Model
{
  public static function cookies(){
  	  if(isset(Auth::user()->id))
        {   
    
          
       ShopCartCookies::addproducttocart();
     return  $count=ShopCart::count();
        }
        else{
    if(empty(Cookie::get('client')))
      {
    
      Cookie::queue('client',md5(time()), 60*60);
    }
       return $count=ShopCartCookies::count();  
        }

  }

  public static function listofcard(){
      
      if(isset(Auth::user()->id))
        {   
         
  return  $list=ShopCart::summ_price();
            
        }
        else{
   
    return  $list=ShopCartCookies::summ_price();
        }

  }

    public static function listofcardpackages(){
      
      if(isset(Auth::user()->id))
        {   
         
  return  $list=ShopCart::summ_packages();
            
        }
        else{
   
    return  $list=ShopCartCookies::summ_packages();
        }

  }

    public static function summcardspackages(){
      if(isset(Auth::user()->id))
        {   
    
          
       
     $summ=ShopCart::summ_packages();
       $allpricepackages=0;
        
        foreach($summ as $pricepackages)
        {
            $pr=$pricepackages->summ_price_packages($pricepackages->packagenr)->all_price_quantity+$pricepackages->summ_pricematerials_packages($pricepackages->packagenr)->all_price_quantity;
            $allpricepackages+=$pr;
        }
        return $allpricepackages;
        }
        else{
   
            
       
     $summ=ShopCartCookies::summ_packages();
       $allpricepackages=0;
        
        foreach($summ as $pricepackages)
        {
            $pr=$pricepackages->summ_price_packages($pricepackages->packagenr)->all_price_quantity+$pricepackages->summ_pricematerials_packages($pricepackages->packagenr)->all_price_quantity;
            $allpricepackages+=$pr;
        }
        return $allpricepackages;
        
        }

  }


  

    public static function summadvancecardspackages(){
      if(isset(Auth::user()->id))
        {   
    
          
       
     $summ=ShopCart::summ_packages();
       $allpricepackages=0;
        
        foreach($summ as $pricepackages)
        {
            $pr=$pricepackages->summ_price_packages($pricepackages->packagenr)->all_advance_price+$pricepackages->summ_pricematerials_packages($pricepackages->packagenr)->all_advance_price;
            $allpricepackages+=$pr;
        }
        return $allpricepackages;
        }
        else{
           $summ=ShopCartCookies::summ_packages();
       $allpricepackages=0;
             
        foreach($summ as $pricepackages)
        {
            $pr=$pricepackages->summ_price_packages($pricepackages->packagenr)->all_advance_price+$pricepackages->summ_pricematerials_packages($pricepackages->packagenr)->all_advance_price;
            $allpricepackages+=$pr;
        }
        return $allpricepackages;
        }
  

  }



      public static function summorderproductspackages($order){
       
    
          
       
     $summ=ShopOrderProducts::summ_price_packages_all($order);
   
     $allpricepackages=0;
     //  dd($order);
        foreach($summ as $pricepackages)
        {

            $pr=$pricepackages->summ_price_packages($order,$pricepackages->packagenr)->all_price_quantity+$pricepackages->summ_pricematerials_packages($order,$pricepackages->packagenr)->all_price_quantity;
            $allpricepackages+=$pr;
        }
        return $allpricepackages;
     

  }

   public static function summorderproductsadvancecardspackages($order){
    
  
       
    $summ=ShopOrderProducts::summ_price_packages_all($order);
       $allpricepackages=0;
        
        foreach($summ as $pricepackages)
        {
            $pr=$pricepackages->summ_price_packages($order,$pricepackages->packagenr)->all_advance_price+$pricepackages->summ_pricematerials_packages($order,$pricepackages->packagenr)->all_advance_price;
            $allpricepackages+=$pr;
        }
        return $allpricepackages;
        
 
  

  }


    public static function summcards(){
      if(isset(Auth::user()->id))
        {   
    
          
       
     return   $summ=ShopCart::summ_price_payu();
        }
        else{
   
    return   $summ=ShopCartCookies::summ_price_payu();
        }

  }


  


}
