<?php

namespace App\Http\Requests\Page\Shop;

use Illuminate\Foundation\Http\FormRequest;

class ShopOrderDelivery extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_method'=>'required',
             'vat'=>'required',
             'delivery_option'=>'required',
             'payment_option'=>'required',
            //
        ];
    }
}
