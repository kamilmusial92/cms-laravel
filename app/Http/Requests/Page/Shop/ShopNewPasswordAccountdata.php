<?php

namespace App\Http\Requests\Page\Shop;
use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
class ShopNewPasswordAccountdata extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currentpassword'=>'required',
            'newpassword'=>'required|min:6',
            'repeatnewpassword'=>'required|min:6',
        ];
    }

  public function withValidator($validator)
    {
        // checks user current password
        // before making changes
        $validator->after(function ($validator) {
             $checkpassword=Auth::User()->password;
            if ( !Hash::check($this->currentpassword,$checkpassword) ) {
                $validator->errors()->add('currentpassword', 'Aktualne hasło jest niepoprawne');
            }
        });
        return;
    }



}
