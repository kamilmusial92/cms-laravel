<?php

namespace App\Http\Requests\Page\Shop;
use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
class ShopUpdateAccountData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'first_name'=>'required',
              'last_name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'address'=>'required',
            'nip'=>'sometimes',
            'nip_delivery'=>'sometimes',
            'company'=>'sometimes',
             'company_delivery'=>'sometimes',
            'postcode'=>'required',
            'city'=>'required',
            'country'=>'required',
            'first_name_delivery'=>'required',
             'last_name_delivery'=>'required',
            'phone_delivery'=>'required',
            'address_delivery'=>'required',
            'postcode_delivery'=>'required',
            'city_delivery'=>'required',
            'country_delivery'=>'required',
            'passwordcheck'=>'required',
           
        ];
    }

    public function withValidator($validator)
    {
        // checks user current password
        // before making changes
        $validator->after(function ($validator) {
             $checkpassword=Auth::User()->password;
            if ( !Hash::check($this->passwordcheck,$checkpassword) ) {
                $validator->errors()->add('current_password', 'Hasło jest niepoprawne.');
            }
        });
        return;
    }

}
