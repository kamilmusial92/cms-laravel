<?php

namespace App\Http\Requests\Page\Shop;

use Illuminate\Foundation\Http\FormRequest;

class ShopCreateOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required',
              'last_name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'address'=>'required',
            'postcode'=>'required',
            'city'=>'required',
            'country'=>'required',
            'first_name_delivery'=>'required',
             'last_name_delivery'=>'required',
            'phone_delivery'=>'required',
            'address_delivery'=>'required',
            'postcode_delivery'=>'required',
            'city_delivery'=>'required',
            'country_delivery'=>'required',


        ];
    }
}
