<?php

namespace App\Http\Requests\Cms\Cms;

use Illuminate\Foundation\Http\FormRequest;

class CmsCreateBlogPosts extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
           'title'=>'required',
           'url'=>'required',
           'long_text'=>'required',
           'keywords'=>'required',
           'info'=>'required',
           'datetimes'=>'required'
        ];
    }

     public function messages()
    {
        return [
           'title.required'=>'Pole tytuł jest wymagane',
           'url.required'=>'Pole url jest wymagane',
           'long_text.required'=>'Pole tekst jest wymagane',
           'keywords.required'=>'Pole słowa kluczowe jest wymagane',
           'info.required'=>'Pole opis jest wymagane',
           'datetimes.required'=>'Data publikacji jest wymagana'
        ];
    }
}
