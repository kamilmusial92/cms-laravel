<?php

namespace App\Http\Requests\Cms\Cms;

use Illuminate\Foundation\Http\FormRequest;

class CmsAdminUsersCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_password'=>'required|min:6',
    'repeat_new_password'=>'required|min:6',
    'first_name'=>'required',
            'email'=>'required|email|unique:users'
        ];
    }
}
