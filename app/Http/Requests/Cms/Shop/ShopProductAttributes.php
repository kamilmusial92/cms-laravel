<?php

namespace App\Http\Requests\Cms\Shop;

use Illuminate\Foundation\Http\FormRequest;

class ShopProductAttributes extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
     
  
$rules=[ 
    'height'=>'required|numeric',
     'width'=>'required|numeric',
    'depth'=>'required|numeric',
    'priority'=>'required|numeric',
    'categoryid'=>'required|numeric|min:1',
   // 'length'=>'required|numeric',
    
   // 'height_seat'=>'required|numeric',
    //'overall_height'=>'required|numeric',
    'inserts_table'=>'sometimes|required',
    'checkboxframe'=>'array|required|min:1',
    'checkboxmaterial'=>'array|required|min:1',
    'framesorting'=>'array|required|min:1',
     'materialsorting'=>'array|required|min:1'
         ];

if(!empty($this->request->get('checkboxframe')))
{
   foreach($this->request->get('checkboxframe') as $key => $val)
  {
    if($this->request->get('checkboxframe')!=0)
    {

    
        $rules['frame.'.$val] = 'required_if:checkboxframe.'.$val.',numeric|numeric';
        
       
    }
   
  }
}

if(!empty($this->request->get('checkboxmaterial')))
{
   foreach($this->request->get('checkboxmaterial') as $key => $val)
  {
    if($this->request->get('checkboxmaterial')!=0)
    {

    
        $rules['material.'.$val] = 'required_if:checkboxmaterial.'.$val.',numeric|numeric';
        
       
    }
   
  }
}



return $rules;

    }
}
