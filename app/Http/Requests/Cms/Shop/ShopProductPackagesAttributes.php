<?php

namespace App\Http\Requests\Cms\Shop;

use Illuminate\Foundation\Http\FormRequest;

class ShopProductPackagesAttributes extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      $rules=[ 
     'framesorting'=>'array|required|min:1',
     'materialsorting'=>'array|required|min:1',
     'priority'=>'required|numeric'

      ];

               if(!empty($this->request->get('chosenproduct')))
{
   foreach($this->request->get('chosenproduct') as $key => $val)
  {
    if($this->request->get('chosenproduct')!=0)
    {

    
        $rules['chosenproductprice.'.$val] = 'required_if:chosenproduct.'.$val.',numeric|numeric';
         $rules['chosenproductquantity.'.$val] = 'required_if:chosenproduct.'.$val.',numeric|numeric';
       
    }
   
  }
}


       if(!empty($this->request->get('checkboxframe')))
{
   foreach($this->request->get('checkboxframe') as $key => $val)
  {
    if($this->request->get('checkboxframe')!=0)
    {

    
        $rules['frame.'.$val] = 'required_if:checkboxframe.'.$val.',numeric|numeric';
        
       
    }
   
  }
}

if(!empty($this->request->get('checkboxmaterial')))
{
   foreach($this->request->get('checkboxmaterial') as $key => $val)
  {
    if($this->request->get('checkboxmaterial')!=0)
    {

    
        $rules['material.'.$val] = 'required_if:checkboxmaterial.'.$val.',numeric|numeric';
        
       
    }
   
  }
}



return $rules;
    }
}
