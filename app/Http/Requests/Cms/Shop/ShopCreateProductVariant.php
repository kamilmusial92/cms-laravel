<?php

namespace App\Http\Requests\Cms\Shop;

use Illuminate\Foundation\Http\FormRequest;

class ShopCreateProductVariant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             
            'name'=>'required',
            'name_product'=>'required',
            'discount'=>'required|numeric',
            'price'=>'required|numeric',
              'quantity'=>'required|numeric',
              'advance_payment'=>'required|numeric|max:100',
        ];
        
    }
}
