<?php

namespace App\Http\Controllers\Cms\Cms\Modules;

use Illuminate\Http\Request;
use App\Cms\Cms\CmsMain;
use App\Cms\Cms\CmsModules;
use App\Http\Controllers\Controller;
use App\Cms\Cms\Modules\CmsGalleriesModule;
use App\Cms\Cms\Modules\CmsComponentsModule;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
use App\Http\Requests\Cms\Cms\CmsCreateGalleries;
use App\Cms\Cms\Modules\CmsTemplatesModule;
use App\Cms\Cms\Modules\CmsSubpagesModule;
use Auth;
use Session;
use Input;
use File;
use App\Http\Requests\Cms\UploadImage;
use App\Cms\Cms\Admin\CmsLogs;
class CmsGalleriesModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

       public function __construct()
       {
        $this->middleware('cms');

         

        }

    public function index()
    {
         $module=CmsMain::module();
          
         $activemodules=CmsModules::chosen(Auth::user()->id);
         $list=CmsGalleriesModule::get();
        return view('cms/cms/modules/galleries.index',compact('activemodules','list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $module=CmsMain::module();
         
         $activemodules=CmsModules::chosen(Auth::user()->id);
         return view('cms/cms/modules/galleries.create',compact('activemodules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function store(CmsCreateGalleries $request)
    {
        CmsGalleriesModule::create(['name'=>$request->name,'user_id'=>Auth::user()->id]);
        Session::flash('success','Stworzono galerię.');
        return redirect('/cms/module/galleries');
    }

     public function showimages($id)
    {
         $module=CmsMain::module();
         
         $activemodules=CmsModules::chosen(Auth::user()->id);
         $list=CmsGalleriesImagesModule::where('gallery_id',$id)->get();
         
         $gallery=CmsGalleriesModule::findorfail($id);
        return view('cms/cms/modules/galleries.showimages',compact('activemodules','list','gallery'));
    }
    public function uploadimage(UploadImage $request,$id)
    {
        
        $file = Input::file('file');
         $filename =  preg_replace('/[^a-zA-Z0-9-_\.]/','',$file->getClientOriginalName());

        $file->move(('storage/cms/galleries/'.$id), $filename);
        CmsLogs::CreateLog(CmsModules::active()->id,'Wgrano plik o nazwie '.$filename);
        $image=CmsGalleriesImagesModule::create(['user_id'=>Auth::user()->id,'gallery_id'=>$id,'file_path'=>'storage/cms/galleries/'.$id.'/','file_name'=>$filename,'alt'=>$filename]);
        Session::flash('success','Wgrano wybrane pliki.');
        return redirect('/cms/module/galleries/'.$id);

    }

      public function removeimage(Request $request,$id)
    {
        $product=CmsGalleriesImagesModule::findorFail($request->image);
        CmsLogs::CreateLog(CmsModules::active()->id,'Usunięto plik o nazwie '.$product->file_name);

        $product->DeleteImage($request->image);

        $filename='storage/cms/galleries/'.$id.'/'.$product->file_name;
           if (file_exists($filename)) {
            unlink($filename);
       }
          $product->delete();

       return redirect('/cms/module/galleries/'.$id);
    }

    public function changenameimage(Request $request,$id)
    {
        $product=CmsGalleriesImagesModule::findorFail($request->image);
        CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono plik o nazwie '.$product->file_name.' na '.$request->name);
        File::move($product->file_path.$product->file_name,$product->file_path.$request->name);
          $product->update(['file_name'=>$request->name,'alt'=>$request->alt]);
    Session::flash('success','Zmieniono dane zdjęcia.');
       //return redirect('/cms/module/galleries/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function destroy($id)
    {
        $gallery=CmsGalleriesModule::findorfail($id);
        $images=CmsGalleriesImagesModule::where('gallery_id',$id)->get();
       
        foreach($images as $image)
        {
            $image->DeleteImage($image->id);
              $filename=$image->file_path.$image->file_name;
           if (file_exists($filename)) {
           
            unlink($filename);
            }


            $image->delete();
        }
        

        $gallery->delete();
        Session::flash('success','Usunięto galerię.');
        return redirect('/cms/module/galleries');
    }
}
