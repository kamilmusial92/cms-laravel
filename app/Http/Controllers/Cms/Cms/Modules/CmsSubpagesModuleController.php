<?php

namespace App\Http\Controllers\Cms\Cms\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\CmsLanguages;
use App\Cms\Cms\Modules\CmsSubpagesModule;
use App\Cms\Cms\Modules\CmsTemplatesModule;
use App\Cms\Cms\Modules\CmsTemplatesComponentModule;
use App\Cms\Cms\Modules\CmsComponentsModule;
use App\Cms\Cms\Modules\CmsGalleriesModule;
use Auth;
use App\Cms\Cms\CmsMain;
use Session;
use App\Http\Requests\Cms\Cms\CmsCreateSubpage;
use Illuminate\Support\Facades\Input;
use App\Cms\Cms\Admin\CmsLogs;
class CmsSubpagesModuleController extends Controller
{
   public $title_page;
   public $description_page;
   public $keywords_page;
   public $image_page;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
       {
        $this->middleware('cms');

         

        }

    public function index()
    {
      
          $module=CmsMain::module();
          
          $mainlang=CmsMain::mainlang();
         $activemodules=CmsModules::chosen(Auth::user()->id);
         $lang=CmsLanguages::findorfail($mainlang->id);
       
        $menus=CmsSubpagesModule::where('lang_id',$lang->id)->orderBy('sort')->get();
        $languages=CmsLanguages::get();
      
     return view('cms/cms/modules/subpages.index',compact('activemodules','menus','languages','mainlang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
         $mainlang=CmsMain::mainlang();
         $templates=CmsTemplatesModule::get();
           return view('cms/cms/modules/subpages.create',compact('activemodules','mainlang','templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsCreateSubpage $request)
    {
       $module=CmsMain::module();
        $mainlang=CmsMain::mainlang();
        $sort=CmsSubpagesModule::where('lang_id',$mainlang->id)->orderby('sort','DESC')->first();
        if(!empty($sort))
        {   
        $sort=$sort->sort+1;
        }
        else
        {
            $sort=0;
        }
            if(!empty($request->seo))
            {
            $template=CmsTemplatesModule::findorfail($request->seo);
            $this->keywords_page=$template->keywords_page;
            $this->description_page=$template->description_page;
            $this->title_page=$template->title_page;
            $this->image_page=$template->image_page;
            }
        

        CmsSubpagesModule::create(['name'=>$request->name,'page_url'=>$request->url,'lang_id'=>$mainlang->id,'sort'=>$sort,'title_page'=>$this->title_page,'description_page'=>$this->description_page,'keywords_page'=>$this->keywords_page,'image_page'=>$this->image_page]);

        Session::flash('success','Stworzono nową podstronę.');
         CmsLogs::CreateLog(CmsModules::active()->id,'Stworzono nową podstronę o nazwie '.$request->name);
        return redirect('/cms/module/subpages/'.$mainlang->url);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$id)
    {
      // $module=CmsMain::module();
      // $activemodules=CmsModules::chosen(Auth::user()->id);
      //  return view('cms/cms/modules/subpages.show',compact('activemodules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang,$id)
    {
         $module=CmsMain::module();
          $mainlang=CmsMain::mainlang();
           $activemodules=CmsModules::chosen(Auth::user()->id);
           $subpage=CmsSubpagesModule::findorfail($id);
            return view('cms/cms/modules/subpages.edit',compact('activemodules','mainlang','subpage'));
    }

    public function editseo($lang,$id)
    {
         $module=CmsMain::module();
          $mainlang=CmsMain::mainlang();
           $activemodules=CmsModules::chosen(Auth::user()->id);
           $subpage=CmsSubpagesModule::findorfail($id);
            $galleries=CmsGalleriesModule::get();
            return view('cms/cms/modules/subpages.editseo',compact('activemodules','mainlang','subpage','galleries'));
    }

    public function saveseo(Request $request, $lang,$id)
    {
        $module=CmsMain::module();
        $mainlang=CmsMain::mainlang();
        $subpage=CmsSubpagesModule::findorFail($id);
        $subpage->update($request->all());
        $subpage->update(['image_page'=>$request->image]);
        Session::flash('success','Edytowano podstronę pomyślnie.');
         CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono dane do pozycjonowania podstrony '.$subpage->name);
        return redirect('/cms/module/subpages/'.$mainlang->url);

    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsCreateSubpage $request, $lang,$id)
    {
        $module=CmsMain::module();
        $mainlang=CmsMain::mainlang();
        $subpage=CmsSubpagesModule::findorFail($id);
         CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono nazwę podstrony '.$subpage->name.' na '.$request->name);
        CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono url podstrony '.$subpage->url.' na '.$request->url);
        $subpage->update($request->all());
        Session::flash('success','Edytowano podstronę pomyślnie.');
        
        return redirect('/cms/module/subpages/'.$mainlang->url);

    }

    public function subpageSort($lang)
    {

        $subpages = CmsSubpagesModule::orderBy('sort', 'ASC')->get();
        $list = Input::get('id');
       
       foreach ($list as $item) {
          CmsSubpagesModule::where('id', '=', $item['id'])->update(array('sort' => $item['sorting']));
     
        }
  
 

    //dd($request->all());
    // gallery sort save...
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang,$id)
    {
         $module=CmsMain::module();
         
         $subpage=CmsSubpagesModule::findorFail($id);
          CmsLogs::CreateLog(CmsModules::active()->id,'Usunięto podstronę '.$subpage->name);
         $subpage->delete();
         Session::flash('success','Usunięto podstronę pomyślnie.');

         return redirect('/cms/module/subpages/'.$lang);

    }

    

}
