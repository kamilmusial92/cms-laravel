<?php

namespace App\Http\Controllers\Cms\Cms\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsMain;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\Modules\CmsBlogPostsModule;
use App\Cms\Cms\Modules\CmsGalleriesModule;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
use App\Http\Requests\Cms\Cms\CmsCreateBlogPosts;
use Session;
use Auth;
class CmsBlogPostsModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
       {
        $this->middleware('cms');

         

        }

    public function index()
    {
       $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);
       $list=CmsBlogPostsModule::get();
        return view('cms/cms/modules/blog.index',compact('activemodules','list'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);
        return view('cms/cms/modules/blog.create',compact('activemodules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsCreateBlogPosts $request)
    {
        
         $date=date("Y-m-d H:i",strtotime($request->datetimes));
        $post=CmsBlogPostsModule::create(['title'=>$request->title,'url'=>$request->url,'long_text'=>$request->long_text,'user_id'=>Auth::user()->id,'thumbnail_id'=>0,'image_id'=>0,'tags'=>$request->tags,'keywords'=>$request->keywords,'info'=>$request->info,'publish_at'=>$date]);
        $id=$post->id;
        Session::flash('info','Wgraj nagłówek');
        return redirect('cms/module/blog/'.$id.'/header');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


      public function showheader($id)
    {   
          $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);
        $post=CmsBlogPostsModule::findorFail($id);
        $galleries=CmsGalleriesModule::get();
       if($post->image_id!=0)
        $image=CmsGalleriesImagesModule::findorFail($post->image_id);
    

        $header='nagłówka';
         $form='header';
        return view('cms/cms/modules/blog.header',compact('activemodules','post','galleries','header','form','image'));
    }

       public function showthumbnail($id)
    {   
         $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);
        $post=CmsBlogPostsModule::findorFail($id);
        $galleries=CmsGalleriesModule::get();
         if($post->thumbnail_id!=0)
         $image=CmsGalleriesImagesModule::findorFail($post->thumbnail_id);
        $header='miniaturki';
         $form='thumbnail';
        return view('cms/cms/modules/blog.header',compact('activemodules','post','galleries','header','form','image'));
    }

       public function showheaderimages($id,$galleryid)
    {   
         $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);

        $post=CmsBlogPostsModule::findorFail($id);
        $galleries=CmsGalleriesModule::get();
        if($post->image_id!=0)
        $image=CmsGalleriesImagesModule::findorFail($post->image_id);
        $images=CmsGalleriesImagesModule::where('gallery_id',$galleryid)->get();
        $galleryid=CmsGalleriesModule::findorFail($galleryid);
        $form='header';
        $header='nagłówka';
        return view('cms/cms/modules/blog.headerimages',compact('activemodules','post','galleries','images','galleryid','form','header','image'));
    }

        public function showthumbnailimages($id,$galleryid)
    {   
         $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);

        $post=CmsBlogPostsModule::findorFail($id);
        $galleries=CmsGalleriesModule::get();
        if($post->thumbnail_id!=0)
          $image=CmsGalleriesImagesModule::findorFail($post->thumbnail_id);
        $images=CmsGalleriesImagesModule::where('gallery_id',$galleryid)->get();
        $galleryid=CmsGalleriesModule::findorFail($galleryid);
        $form='thumbnail';
        $header='miniaturki';
        return view('cms/cms/modules/blog.headerimages',compact('activemodules','post','galleries','images','galleryid','form','header','image'));
    }

       public function chooseheaderimages(Request $request,$id,$galleryid)
    {   

     

        $post=CmsBlogPostsModule::findorFail($id);
       $post->update(['image_id'=>$request->image]);
         Session::flash('success','Wybrano nagłówek.');
           return redirect('cms/module/blog/'.$id.'/header/'.$galleryid);
    }

    public function choosethumbnailimages(Request $request,$id,$galleryid)
    {   
        $post=CmsBlogPostsModule::findorFail($id);
       $post->update(['thumbnail_id'=>$request->image]);
         Session::flash('success','Wybrano miniaturkę.');
           return redirect('cms/module/blog/'.$id.'/thumbnail/'.$galleryid);
    }
   

    public function show($id)
    {
         $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);

         $post=CmsBlogPostsModule::findorFail($id);

        if($post->image_id!=0)
        $header=CmsGalleriesImagesModule::findorfail($post->image_id);

        if($post->thumbnail_id!=0)
        $thumbnail=CmsGalleriesImagesModule::findorfail($post->thumbnail_id);

         return view('cms/cms/modules/blog.show',compact('activemodules','post','thumbnail','header'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $module=CmsMain::module();
        
       $activemodules=CmsModules::chosen(Auth::user()->id);

         $post=CmsBlogPostsModule::findorFail($id);
         return view('cms/cms/modules/blog.edit',compact('activemodules','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsCreateBlogPosts $request, $id)
    {
          $post=CmsBlogPostsModule::findorfail($id);
        $date=date("Y-m-d H:i",strtotime($request->datetimes));
         $post->update(['title'=>$request->title,'url'=>$request->url,'long_text'=>$request->long_text,'user_id'=>Auth::user()->id,'publish_at'=>$date,'tags'=>$request->tags,'keywords'=>$request->keywords,'info'=>$request->info]);
       
        Session::flash('success','Zapisano zmiany');
        return redirect('cms/module/blog/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $post=CmsBlogPostsModule::findorfail($id);
        $post->delete();
         Session::flash('success','Usunięto post');
        return redirect('cms/module/blog');
    }
}
