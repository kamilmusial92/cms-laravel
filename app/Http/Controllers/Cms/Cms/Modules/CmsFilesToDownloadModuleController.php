<?php

namespace App\Http\Controllers\Cms\Cms\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsMain;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\Modules\CmsFilesToDownloadModule;
use App\Cms\Cms\Modules\CmsChosenFilesToDownloadModule;
use Auth;
use Session;
use Input;
use File;
use App\Http\Requests\Cms\UploadImage;
use App\Cms\Cms\Admin\CmsLogs;
class CmsFilesToDownloadModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
     $this->middleware('cms');

      

     }

    public function index()
    {
        $module=CmsMain::module();
         
         $activemodules=CmsModules::chosen(Auth::user()->id);
         $list=CmsFilesToDownloadModule::get();
         
        
        return view('cms/cms/modules/filestodownload.index',compact('activemodules','list'));
    }

    public function uploadimage(UploadImage $request)
    {
        
        $file = Input::file('file');
         $filename =  preg_replace('/[^a-zA-Z0-9-_\.]/','',$file->getClientOriginalName());

        $file->move(('storage/cms/files_to_download'), $filename);
        CmsLogs::CreateLog(CmsModules::active()->id,'Wgrano plik o nazwie '.$filename);
        $image=CmsFilesToDownloadModule::create(['user_id'=>Auth::user()->id,'file_path'=>'storage/cms/files_to_download/','file_name'=>$filename]);
        Session::flash('success','Wgrano wybrane pliki.');

        return redirect('/cms/module/files_to_download');

    }

      public function removeimage(Request $request)
    {
        $product=CmsFilesToDownloadModule::findorFail($request->image);

         CmsLogs::CreateLog(CmsModules::active()->id,'Usunięto plik o nazwie '.$product->file_name);
              $filename='storage/cms/files_to_download/'.$product->file_name;
            $deletechosen=CmsChosenFilesToDownloadModule::where('file_id',$request->image)->delete();

           if (file_exists($filename)) {
            unlink($filename);
            }
          $product->delete();

       return redirect('/cms/module/files_to_download');
    }

    public function changenameimage(Request $request)
    {
        $product=CmsFilesToDownloadModule::findorFail($request->image);
         CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono plik o nazwie '.$product->file_name.' na '.$request->name);
        File::move($product->file_path.$product->file_name,$product->file_path.$request->name);
          $product->update(['file_name'=>$request->name]);

       return redirect('/cms/module/files_to_download');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
