<?php

namespace App\Http\Controllers\Cms\Cms\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsMain;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\Modules\CmsSubpagesModule;
use App\Cms\Cms\Modules\CmsComponentsModule;
use App\Cms\Cms\Modules\CmsGalleriesModule;
use App\Cms\Cms\Modules\CmsFilesToDownloadModule;
use App\Cms\Cms\Modules\CmsChosenFilesToDownloadModule;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
use App\Cms\Cms\Modules\CmsTemplatesModule;
use App\Cms\Cms\CmsLanguages;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Cms\Cms\Modules\CmsTemplatesComponentModule;
use App\Cms\Cms\Admin\CmsLogs;
class CmsComponentsModuleController extends Controller
{

  public $sort;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
       {
        $this->middleware('cms');

         

        }

    public function list($lang,$subpageid)
    {
         $module=CmsMain::module();
          $mainlang=CmsMain::mainlang();
         $activemodules=CmsModules::chosen(Auth::user()->id);
          
            $components=CmsComponentsModule::where('subpage_id',$subpageid)->with('template_module')->orderBy('sort','ASC')->get();
            
            
            

        $subpage=CmsSubpagesModule::findorfail($subpageid);
        
     return view('cms/cms/modules/subpages/components.index',compact('activemodules','mainlang','components','subpage'));
    }

      public function listnew($lang,$subpageid,$new)
    {
         $module=CmsMain::module();
          $mainlang=CmsMain::mainlang();
         $activemodules=CmsModules::chosen(Auth::user()->id);
          
            $components=CmsComponentsModule::where('subpage_id',$subpageid)->with('template_module')->orderBy('sort','ASC')->get();
            
            
            

        $subpage=CmsSubpagesModule::findorfail($subpageid);
        
     return view('cms/cms/modules/subpages/components.index',compact('activemodules','mainlang','components','subpage','new'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function showgalleryimage(Request $request,$lang,$subpage,$id)
    {
        
        $images=CmsGalleriesImagesModule::where('gallery_id',$request->gallery)->get();
        $gallery=CmsGalleriesModule::findorFail($request->gallery);
        return json_encode(array('images'=>$images,'gallery'=>$gallery));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$subpageid,$id)
    {

        $module=CmsMain::module();
          $mainlang=CmsMain::mainlang();
         $activemodules=CmsModules::chosen(Auth::user()->id);
        $subpage=CmsSubpagesModule::findorfail($subpageid);
        $component=CmsComponentsModule::findorfail($id);

       

        $templatecomponent=CmsTemplatesComponentModule::findorfail($component->template_component_id);
     
        $chosenfiles=CmsChosenFilesToDownloadModule::where('component_id',$id)->get();
         
           
          return view('cms/cms/modules/subpages/components.show',compact('activemodules','mainlang','component','subpage','templatecomponent','chosenfiles'));
           
        }


    public function choosetemplate($lang,$id)
    {
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
      $mainlang=CmsMain::mainlang();
      $templates=CmsTemplatesModule::get();
      $subpage=CmsSubpagesModule::findorfail($id);
      return view('cms/cms/modules/subpages/components.choosetemplate',compact('activemodules','templates','mainlang','subpage'));
      
    }
     public function choosetemplatemain($lang,$id,$main)
    {
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
      $mainlang=CmsMain::mainlang();
      $templates=CmsTemplatesModule::get();
      $subpage=CmsSubpagesModule::findorfail($id);
      return view('cms/cms/modules/subpages/components.choosetemplate',compact('activemodules','templates','mainlang','subpage','main'));
      
    }

       public function choosecomponent($lang,$id)
    {
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
      $mainlang=CmsMain::mainlang();
      $templates=CmsSubpagesModule::get();
      $subpage=CmsSubpagesModule::findorfail($id);
      return view('cms/cms/modules/subpages/components.choosecomponent',compact('activemodules','templates','mainlang','subpage'));
      
    }

        public function choosecomponentmain($lang,$id,$main)
    {
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
      $mainlang=CmsMain::mainlang();
      $templates=CmsSubpagesModule::get();
      $subpage=CmsSubpagesModule::findorfail($id);
      return view('cms/cms/modules/subpages/components.choosecomponent',compact('activemodules','templates','mainlang','subpage','main'));
      
    }

    public function showtemplate($lang,$id,$templateid)
    {
      
      
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
    //  $template=CmsTemplatesModule::findorfail($id);
      $templates=CmsTemplatesModule::get();
      $components=CmsTemplatesComponentModule::where('template_id',$templateid)->orderBy('sort')->get();
      $mainlang=CmsMain::mainlang();
      $subpage=CmsSubpagesModule::findorfail($id);
       return view('cms/cms/modules/subpages/components.choosetemplate',compact('activemodules','templates','mainlang','subpage','components'));
      
    }


    public function showtemplatemain($lang,$id,$main,$templateid)
    {
      
      
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
    //  $template=CmsTemplatesModule::findorfail($id);
      $templates=CmsTemplatesModule::get();
      $components=CmsTemplatesComponentModule::where('template_id',$templateid)->orderBy('sort')->get();
      $mainlang=CmsMain::mainlang();
      $subpage=CmsSubpagesModule::findorfail($id);
       return view('cms/cms/modules/subpages/components.choosetemplate',compact('activemodules','templates','mainlang','subpage','components','main'));
      
    }

      public function showcomponent($lang,$id,$templateid)
    {
      
      
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
    //  $template=CmsTemplatesModule::findorfail($id);
      $templates=CmsSubpagesModule::get();
      $components=CmsComponentsModule::where('subpage_id',$templateid)->orderBy('sort')->get();
      $mainlang=CmsMain::mainlang();
      $subpage=CmsSubpagesModule::findorfail($id);
       return view('cms/cms/modules/subpages/components.choosecomponent',compact('activemodules','templates','mainlang','subpage','components'));
      
    }
        public function showcomponentmain($lang,$id,$main,$templateid)
    {
      
      
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
    //  $template=CmsTemplatesModule::findorfail($id);
      $templates=CmsSubpagesModule::get();
      $components=CmsComponentsModule::where('subpage_id',$templateid)->orderBy('sort')->get();
      $mainlang=CmsMain::mainlang();
      $subpage=CmsSubpagesModule::findorfail($id);
       return view('cms/cms/modules/subpages/components.choosecomponent',compact('activemodules','templates','mainlang','subpage','components','main'));
      
    }




    public function chosentemplate(Request $request,$lang,$id)
    {
      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/choosetemplate/'.$request->chosen);
    }
      public function chosentemplatemain(Request $request,$lang,$id,$main)
    {
      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/'.$main.'/choosetemplate/'.$request->chosen);
    }

     public function chosencomponent(Request $request,$lang,$id)
    {
      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/choosecomponent/'.$request->chosen);
    }
     public function chosencomponentmain(Request $request,$lang,$id,$main)
    {
      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/'.$main.'/choosecomponent/'.$request->chosen);
    }

    public function copytemplate(Request $request,$lang,$id)
    {

      $subpage=CmsSubpagesModule::findorfail($id);
      foreach ($request->checkboxmodule as $key =>$value) 
      {
        $idcomponent=$request->checkboxmodule[$key];
         if($request->checkboxmodule[$key]!=0)
         {
          $template=CmsTemplatesComponentModule::findorFail($idcomponent);
         // $component=CmsComponentsModule::where('subpage_id',$id)->orderBy('sort','DESC')->first();
         
         // $temp=CmsTemplatesComponentModule::findorFail($component->template_id);
         
         if(!empty($component))
          {
            $sort=$component->sort;
          }
          else{
            $sort=0;
          }
 if($request->withsort=='on')
          {
            $status=1;
          }
          else{
            $status=0;
             $sort=-2;
          }
          CmsComponentsModule::create(['name'=>$template->name,'subpage_id'=>$id,'template_component_id'=>$template->id,'sort'=>$sort+1,'status'=>$status]);
           CmsLogs::CreateLog(CmsModules::active()->id,'Utworzono komponent '.$template->name.' w podstronie '.$subpage->name);
         }
      }
    Session::flash('success','Utworzono komponenty pomyślnie.');

      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/list');



    }

      public function copytemplatemain(Request $request,$lang,$id,$main)
    {

      $subpage=CmsSubpagesModule::findorfail($id);
      foreach ($request->checkboxmodule as $key =>$value) 
      {
        $idcomponent=$request->checkboxmodule[$key];
         if($request->checkboxmodule[$key]!=0)
         {
          $template=CmsTemplatesComponentModule::findorFail($idcomponent);
          $component=CmsComponentsModule::findorfail($main);
         
         // $temp=CmsTemplatesComponentModule::findorFail($component->template_id);
         
            $status=1;
      
         
         if(!empty($component))
          {
            $sort=$component->sort;
          }
          else{
            $sort=0;
          }
          CmsComponentsModule::create(['name'=>$template->name,'subpage_id'=>$id,'template_component_id'=>$template->id,'sort'=>$sort+1,'status'=>$status]);
           CmsLogs::CreateLog(CmsModules::active()->id,'Utworzono komponent '.$template->name.' w podstronie '.$subpage->name);
         }
      }
    Session::flash('success','Utworzono komponenty pomyślnie.');

      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/list/'.$main);



    }

    public function copycomponent(Request $request,$lang,$id)
    {

      $subpage=CmsSubpagesModule::findorfail($id);
      foreach ($request->checkboxmodule as $key =>$value) 
      {
        $idcomponent=$request->checkboxmodule[$key];
         if($request->checkboxmodule[$key]!=0)
         {
        
          $component=CmsComponentsModule::findorfail($idcomponent);
          $temp=CmsTemplatesComponentModule::findorFail($component->template_component_id);

          $sort=CmsComponentsModule::where('subpage_id',$id)->orderBy('sort','DESC')->first();
          if(!empty($sort))
            $this->sort=$sort->sort;

          if($temp->user_visible==1)
          {
            $status=1;
          }
          else{
            $status=0;
          }

          if($component->copyable==1)
          {
            CmsComponentsModule::create(['header'=>$component->header,'background'=>$component->background,'title'=>$component->title,'subtitle'=>$component->subtitle,'text'=>$component->text,'image'=>$component->image,'gallery'=>$component->gallery,'url'=>$component->url,'name'=>$component->name,'subpage_id'=>$id,'template_component_id'=>$component->template_component_id,'sort'=>$sort->sort+1,'status'=>$status,'copyable'=>$component->copyable,'copyable_id'=>$component->id]);
           
          }
          else
          {
           
          CmsComponentsModule::create(['name'=>$component->name,'subpage_id'=>$id,'template_component_id'=>$component->template_component_id,'sort'=>$this->sort+1,'status'=>$status,'copyable'=>$component->copyable,'copyable_id'=>$component->id]);
          } 
          CmsLogs::CreateLog(CmsModules::active()->id,'Skopiowano komponent '.$component->name.' w podstronie '.$subpage->name);
         }
      }
    Session::flash('success','Utworzono komponenty pomyślnie.');

      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/list');



    }

     public function copycomponentmain(Request $request,$lang,$id,$main)
    {

      $subpage=CmsSubpagesModule::findorfail($id);
      foreach ($request->checkboxmodule as $key =>$value) 
      {
        $idcomponent=$request->checkboxmodule[$key];
         if($request->checkboxmodule[$key]!=0)
         {
        
          $component=CmsComponentsModule::findorfail($idcomponent);
          $temp=CmsTemplatesComponentModule::findorFail($component->template_component_id);
          $sort=CmsComponentsModule::findorFail($main);
         if(!empty($sort))
            $this->sort=$sort->sort;


          
            $status=1;
          
         
          if($component->copyable==1)
          {
            CmsComponentsModule::create(['header'=>$component->header,'background'=>$component->background,'title'=>$component->title,'subtitle'=>$component->subtitle,'text'=>$component->text,'image'=>$component->image,'gallery'=>$component->gallery,'url'=>$component->url,'name'=>$component->name,'subpage_id'=>$id,'template_component_id'=>$component->template_component_id,'sort'=>$sort->sort+1,'status'=>$status,'copyable'=>$component->copyable,'copyable_id'=>$component->id]);
           
          }
          else
          {
          CmsComponentsModule::create(['name'=>$component->name,'subpage_id'=>$id,'template_component_id'=>$component->template_component_id,'sort'=>$this->sort+1,'status'=>$status,'copyable'=>$component->copyable,'copyable_id'=>$component->id]);
          } 
          CmsLogs::CreateLog(CmsModules::active()->id,'Skopiowano komponent '.$component->name.' w podstronie '.$subpage->name);
         }
      }
    Session::flash('success','Utworzono komponenty pomyślnie.');

      return redirect('/cms/module/subpages/'.$lang.'/'.$id.'/components/list/'.$main);



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang,$subpageid,$id)
    {
         $module=CmsMain::module();
          $mainlang=CmsMain::mainlang();
         $activemodules=CmsModules::chosen(Auth::user()->id);
        $component=CmsComponentsModule::findorfail($id);
        $subpage=CmsSubpagesModule::findorfail($subpageid);
        $templatecomponent=CmsTemplatesComponentModule::findorfail($component->template_component_id);
        $galleries=CmsGalleriesModule::get();
        $files=CmsFilesToDownloadModule::get();
        $chosenfiles=CmsChosenFilesToDownloadModule::where('component_id',$id)->get();
        if((Auth::user()->admincms==1) or (Auth::user()->admincms==0 and Auth::user()->usercms==1 and $templatecomponent->user_visible==1))
           {             
             return view('cms/cms/modules/subpages/components.edit',compact('activemodules','mainlang','component','subpage','templatecomponent','galleries','files','chosenfiles'));
           }
           else
           {
            abort('404');
           }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang,$subpage,$id)
    {
        
       $component=CmsComponentsModule::findorfail($id);
       $subpage=CmsSubpagesModule::findorfail($subpage);
        $mainlang=CmsMain::mainlang();
       $component->update(['name'=>$request->name,'header'=>$request->header,'background'=>$request->background,'url'=>$request->url,'title'=>$request->title,'subtitle'=>$request->subtitle,'text'=>$request->text,'image'=>$request->image,'gallery'=>$request->gallery,'copyable'=>$request->copyable]);

     
      $components=CmsComponentsModule::where('copyable',1)->where('copyable_id',$component->id)->get();
     

       foreach($components as $copy)
       {
        $subpagecopy=CmsSubpagesModule::findorfail($copy->subpage_id);
        $language=CmsLanguages::findorfail($subpagecopy->lang_id);
        
        if($lang==$language->url)
        {
           CmsComponentsModule::findorfail($copy->id)->update(['name'=>$request->name,'header'=>$request->header,'background'=>$request->background,'url'=>$request->url,'title'=>$request->title,'subtitle'=>$request->subtitle,'text'=>$request->text,'image'=>$request->image,'gallery'=>$request->gallery,'copyable'=>$request->copyable]);
        }
        
         
       }

      CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono treści w komponencie '.$component->name.' w podstronie '.$subpage->name);

         CmsChosenFilesToDownloadModule::where('component_id',$id)->delete();
         if(!empty($request->choosefile))
         {
         foreach ($request->choosefile as $key =>$value) 
       {
     
    
       if($request->choosefile[$key]!=0)
       {
         $module=CmsChosenFilesToDownloadModule::where('file_id',$request->choosefile[$key])->where('component_id',$id)->first();

            if(empty($module))
            {
             
           
            CmsChosenFilesToDownloadModule::create(['file_id'=>$request->choosefile[$key],'component_id'=>$id]);
            
            }

        }

        }
      }
     Session::flash('success','Zmieniono treści pomyślnie.');
      return redirect('/cms/module/subpages/'.$lang.'/'.$subpage->id.'/components/'.$id);
    }

    public function componentSort($lang,$subpageid)
    {

        $Components = CmsComponentsModule::where('subpage_id',$subpageid)->orderBy('sort', 'ASC')->get();
    
       
       
      $list = Input::get('id');

       foreach ($list as $item) {
          CmsComponentsModule::where('id', '=', $item['id'])->update(array('sort' => $item['sorting'],'status'=>1));
     
        }
  
 

    //dd($request->all());
    // gallery sort save...
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang,$subpageid,$id)
    {
        if(Auth::user()->admincms==1)
        {
      $component=CmsComponentsModule::findorfail($id)->delete();

      Session::flash('success','Usunięto komponent');
        }
      return redirect('/cms/module/subpages/'.$lang.'/'.$subpageid.'/components/list');
    }
}
