<?php

namespace App\Http\Controllers\Cms\Cms\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\CmsMain;
use App\Cms\Cms\Modules\CmsTemplatesModule;
use App\Cms\Cms\Modules\CmsTemplatesComponentModule;
use App\Cms\Cms\Modules\CmsTemplatesMastersModule;
use App\Cms\Cms\Admin\CmsUsersChosenModules;
use App\Cms\Cms\Modules\CmsGalleriesModule;
use App\Cms\Cms\Modules\CmsGalleriesImagesModule;
use Auth;
use Session;
use File;
use Storage;
use App\Http\Requests\Cms\Cms\CmsCreateTemplate;
use App\Cms\Cms\Admin\CmsLogs;
class CmsTemplatesModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('cms');

        

    }

    public function index()
    {
       $module=CmsMain::module();
        
        $activemodules=CmsModules::chosen(Auth::user()->id);
       $templates=CmsTemplatesModule::get();
     return view('cms/cms/modules/templates.index',compact('activemodules','templates'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
          $templates=CmsTemplatesModule::get();
            $galleries=CmsGalleriesModule::get();
          return view('cms/cms/modules/templates.create',compact('activemodules','templates','galleries'));
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsCreateTemplate $request)
    {
        $module=CmsMain::module();
         $activemodules=CmsModules::chosen(Auth::user()->id);
        $path=CmsMain::replaceurl($request->name);
         $template=CmsTemplatesModule::create(['name'=>$request->name,'path'=>$path,'title_page'=>$request->title_page,'description_page'=>$request->description_page,'keywords_page'=>$request->keywords_page,'image_page'=>$request->image]);
         File::makeDirectory("../resources/views/page/templates/".$path, $mode = 0777, true, true);
         File::makeDirectory('assets_'.$path, $mode = 0777, true, true);

           Session::flash('info','Stwórz komponenty.');
           CmsLogs::CreateLog(CmsModules::active()->id,'Stworzono szablon '.$request->name);
           return redirect('/cms/module/templates/'.$template->id.'/components');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module=CmsMain::module();
         $activemodules=CmsModules::chosen(Auth::user()->id);
         $template=CmsTemplatesModule::findorfail($id);
         $components=CmsTemplatesComponentModule::where('template_id',$id)->orderby('sort')->get();
        return view('cms/cms/modules/templates.show',compact('activemodules','template','components'));
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
         $template=CmsTemplatesModule::findorfail($id);
           $galleries=CmsGalleriesModule::get();
          return view('cms/cms/modules/templates.edit',compact('activemodules','template','galleries'));
    }

     public function showgalleryimage(Request $request)
    {
        
        $images=CmsGalleriesImagesModule::where('gallery_id',$request->gallery)->get();
        $gallery=CmsGalleriesModule::findorFail($request->gallery);
        return json_encode(array('images'=>$images,'gallery'=>$gallery));
    }


     public function editmaster()
    {
            $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
            $templatemaster=CmsTemplatesMastersModule::first();
            if(!empty($templatemaster))
            {
                $templatemastercontent=$templatemaster->content;
            }
            else
            {
                $templatemastercontent='';

            }

          return view('cms/cms/modules/templates.editmaster',compact('activemodules','templatemastercontent'));
    }

      public function savemaster(Request $request)
    {
         $templatemaster=CmsTemplatesMastersModule::first();
          if(!empty($templatemaster))
          {

            $templatemaster->update(['name'=>'master','content'=>$request->content]);
            File::delete('../resources/views/page/master.blade.php');
            File::put('../resources/views/page/master.blade.php', $request->content);
          }
          else
          {
             CmsTemplatesMastersModule::create(['name'=>'master','content'=>$request->content]);
             File::put('../resources/views/page/master.blade.php', $request->content);
          }
           Session::flash('success','Edycję pliku master zakończono pomyślnie.');

           CmsLogs::CreateLog(CmsModules::active()->id,'Edytowano plik master');

       return redirect('/cms/module/templates');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsCreateTemplate $request, $id)
    {

        $module=CmsMain::module();

        $template=CmsTemplatesModule::findorfail($id);

          $path=CmsMain::replaceurl($request->name);

        
        File::move("../resources/views/page/templates/".$template->path,"../resources/views/page/templates/".$path);

        CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono nazwę szablonu z '.$template->name.' na '.$request->name);
        CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono dane seo dla szablonu '.$request->name);
        $template->update(['name'=>$request->name,'path'=>$path,'title_page'=>$request->title_page,'description_page'=>$request->description_page,'keywords_page'=>$request->keywords_page,'image_page'=>$request->image]);
        
        return redirect('/cms/module/templates/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $module=CmsMain::module();

        $template=CmsTemplatesModule::findorfail($id);
        CmsLogs::CreateLog(CmsModules::active()->id,'Usunięto szablon '.$template->name);
        File::deleteDirectory('../resources/views/page/templates/'.$template->path);
        $template->delete();
         
         Session::flash('success','Usunięto szablon pomyślnie.');
   
       return redirect('/cms/module/templates');
    }
}
