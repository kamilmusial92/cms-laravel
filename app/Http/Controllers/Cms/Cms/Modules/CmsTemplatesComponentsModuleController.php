<?php

namespace App\Http\Controllers\Cms\Cms\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsMain;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\Modules\CmsTemplatesModule;
use App\Cms\Cms\Modules\CmsTemplatesComponentModule;
use App\Cms\Cms\Modules\CmsTemplatesComponentArchivesModule;
use App\Cms\Cms\Modules\CmsBoxTypes;
use Auth;
use Session;

use File;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Cms\Cms\CmsCreateTemplateComponent;
use App\Cms\Cms\Admin\CmsLogs;
class CmsTemplatesComponentsModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
    {
        $this->middleware('cms');

        

    }

    public function index($id)
    {
        $module=CmsMain::module();
         $activemodules=CmsModules::chosen(Auth::user()->id);
         $template=CmsTemplatesModule::findorfail($id);
          $components=CmsTemplatesComponentModule::where('template_id',$id)->orderby('sort')->get();

        return view('cms/cms/modules/templates/components.index',compact('activemodules','template','components'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
           $template=CmsTemplatesModule::findorfail($id);
           $boxtypes=CmsBoxTypes::get();
          return view('cms/cms/modules/templates/components.create',compact('activemodules','template','boxtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsCreateTemplateComponent $request)
    {
       
        $id=$request->template_id;
        $template=CmsTemplatesModule::findorfail($request->template_id);
        $path=CmsMain::replaceurl($request->name); 
          // $componentlast=CmsTemplatesComponentModule::where('template_id',$id)->orderBy('sort','DESC')->first();
           if(!empty($componentlast))
           {
            $last=$componentlast->sort;
           }  
           else{
            $last=-1;
           }
         $component=CmsTemplatesComponentModule::create(['name'=>$request->name,'template_id'=>$id,'content'=>$request->content,'path'=>$path,'user_visible'=>$request->user_visible,'sort'=>$last]);
         
           if($request->savearchive==1)
        {
          $archive=CmsTemplatesComponentArchivesModule::create(['name'=>$request->name,'component_id'=>$component->id,'template_id'=>$component->template_id,'copied_template_id'=>$component->copied_template_id,'content'=>$request->content,'path'=>$path,'header_status'=>0,'background_status'=>0,'title_status'=>0,'subtitle_status'=>0,'text_status'=>0,'image_status'=>0,'gallery_status'=>0,'footer_status'=>0,'user_visible'=>$request->user_visible,'sort'=>$component->sort]);
        }
         if(!empty($request->checkboxmodule))
         {
              foreach ($request->checkboxmodule as $key =>$value) 
       {
  

       if($request->checkboxmodule[$key]!=0)
       {

         $component->update([$component->chosenBoxes($request->checkboxmodule[$key])=>1]);

          if($request->savearchive==1)
          {
             $archive->update([$component->chosenBoxes($request->checkboxmodule[$key])=>1]);
          }

         CmsLogs::CreateLog(CmsModules::active()->id,'Włączono opcję '.$component->chosenBoxes($request->checkboxmodule[$key]).' w komponencie '.$request->name.' w szablonie '.$template->name);

        }

        }
      }

           Session::flash('success','Stworzono komponent.');
        $template=CmsTemplatesModule::findorfail($id);
       
        
          File::put('../resources/views/page/templates/'.$template->path.'/'.$path.'.blade.php', $request->content);

          CmsLogs::CreateLog(CmsModules::active()->id,'Stworzono komponent o nazwie '.$request->name.' w szablonie '.$template->name);
           return redirect('/cms/module/templates/'.$id.'/components');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

      public function componentSort($templateid)
    {
      $template=CmsTemplatesModule::findorfail($templateid);
        //$Components = CmsTemplatesComponentModule::where('template_id',$templateid)->orderBy('sort', 'ASC')->get();
        $list = Input::get('id');
        //$sorting = Input::get('sorting');
      // $list=json_encode($list);
     // $list=json_decode($list,true);
     //  print($list);
      
       foreach ($list as $item) {
          
       
          CmsTemplatesComponentModule::where('id', '=', $item['id'])->update(array('sort' => $item['sorting'],'status'=>1));
        
       }
  
  

    //dd($request->all());
    // gallery sort save...
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($template,$component)
    {
        $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
          $template=CmsTemplatesModule::findorfail($template);
           $component=CmsTemplatesComponentModule::findorfail($component);
           $boxtypes=CmsBoxTypes::get();
          return view('cms/cms/modules/templates/components.edit',compact('activemodules','template','component','boxtypes','template'));
    }

     public function showarchive($template,$componentid)
    {
        $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
          $template=CmsTemplatesModule::findorfail($template);
          $component=CmsTemplatesComponentModule::findorfail($componentid);
         $archives=CmsTemplatesComponentArchivesModule::where('component_id',$component->id)->get();
         
          return view('cms/cms/modules/templates/components.choosearchive',compact('activemodules','template','archives','component','componentid'));
    }

     public function choosearchive($id,$componentid,$archive)
    {
         $module=CmsMain::module();
          $activemodules=CmsModules::chosen(Auth::user()->id);
          $template=CmsTemplatesModule::findorfail($id);

           $component=CmsTemplatesComponentArchivesModule::findorfail($archive);

           $boxtypes=CmsBoxTypes::get();
           $archives=CmsTemplatesComponentArchivesModule::where('component_id',$component->component_id)->get();
      return view('cms/cms/modules/templates/components.choosearchive',compact('activemodules','template','archives','component','archive','boxtypes','componentid'));
      
    }
    public function choosetemplate($id)
    {
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
      $template=CmsTemplatesModule::findorfail($id);
      $templates=CmsTemplatesModule::get();
      return view('cms/cms/modules/templates/components.choosetemplate',compact('activemodules','templates','template'));
      
    }

      public function showtemplate($id,$templateid)
    {
      $module=CmsMain::module();
      $activemodules=CmsModules::chosen(Auth::user()->id);
      $template=CmsTemplatesModule::findorfail($id);
      $templates=CmsTemplatesModule::get();
      $components=CmsTemplatesComponentModule::where('template_id',$templateid)->orderBy('sort')->get();
     
      return view('cms/cms/modules/templates/components.choosetemplate',compact('activemodules','templates','template','components'));
      
    }

    public function chosentemplate(Request $request,$id)
    {
      return redirect('/cms/module/templates/'.$id.'/components/choosetemplate/'.$request->chosen);
    }
     public function chosenarchive(Request $request,$id,$component)
    {
      return redirect('/cms/module/templates/'.$id.'/components/'.$component.'/choosearchive/'.$request->chosen);
    }

    public function copytemplate(Request $request,$id)
    {


      $template=CmsTemplatesModule::findorFail($id);
     // $components=CmsTemplatesComponentModule::where('template_id',$template->id)->get();
     // $lastcomponent=CmsTemplatesComponentModule::where('template_id',$template->id)->orderBy('sort','DESC')->first();
      if(!empty($lastcomponent))
      {
        $sort=$lastcomponent->sort;
      }
      else
      {
       $sort= -1;
      }
        foreach ($request->checkboxmodule as $key =>$value) 
       {
     
          if($request->checkboxmodule[$key]!=0)
          {
            $component=CmsTemplatesComponentModule::findorFail($request->checkboxmodule[$key]);
            
            $n=CmsTemplatesComponentModule::create(['name'=>$component->name,'copied_template_id'=>$component->template_id,'copied_template_component_id'=>$component->id,'template_id'=>$id,'header_status'=>$component->header_status,'background_status'=>$component->background_status,'title_status'=>$component->title_status,'subtitle_status'=>$component->subtitle_status,'text_status'=>$component->text_status,'image_status'=>$component->image_status,'gallery_status'=>$component->gallery_status,'footer_status'=>$component->footer_status,'url_status'=>$component->url_status,'file_status'=>$component->file_status,'content'=>$component->content,'path'=>$component->path,'sort'=>$sort,'user_visible'=>$component->user_visible,'status'=>0]);
              $n->update(['name'=>$n->name.$n->id]);
               $path=CmsMain::replaceurl($n->name); 
               $n->update(['path'=>$n->path.$n->id]);
            $template=CmsTemplatesModule::findorfail($id);
       
        
          File::put('../resources/views/page/templates/'.$template->path.'/'.$path.'.blade.php', $n->content);

            CmsLogs::CreateLog(CmsModules::active()->id,'Skopiowano komponent '.$component->name.' do szablonu '.$template->name);
          }

        }
   
       Session::flash('success','Skopiowano komponenty pomyślnie.');


    return redirect('/cms/module/templates/'.$id.'/components');
      
    }

    public function copyarchive(Request $request,$template,$component,$archive)
    {

        $archive=CmsTemplatesComponentArchivesModule::findorFail($archive);
         $template=CmsTemplatesModule::findorFail($template);
        $component=CmsTemplatesComponentModule::findorFail($component);
        $component->update(['name'=>$archive->name,'template_id'=>$archive->template_id,'copied_template_id'=>$archive->copied_template_id,'header_status'=>$archive->header_status,'background_status'=>$archive->background_status,'title_status'=>$archive->title_status,'subtitle_status'=>$archive->subtitle_status,'text_status'=>$archive->text_status,'image_status'=>$archive->image_status,'gallery_status'=>$archive->gallery_status,'footer_status'=>$archive->footer_status,'url_status'=>$archive->url_status,'file_status'=>$archive->file_status,'content'=>$archive->content,'path'=>$archive->path,'sort'=>$archive->sort,'user_visible'=>$archive->user_visible]);
   
       Session::flash('success','Skopiowano komponenty z archiwum pomyślnie.');

 CmsLogs::CreateLog(CmsModules::active()->id,'Skopiowano komponent z archiwum '.$archive->name.' ('.$archive->created_at.')  do komponentu '.$component->name.'z szablonu '.$template->name);
    return redirect('/cms/module/templates/'.$template->id.'/components');
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsCreateTemplateComponent $request, $template,$id)
    {
        $component=CmsTemplatesComponentModule::findorfail($id); 
        $path=CmsMain::replaceurl($request->name); 
        $template=CmsTemplatesModule::findorfail($request->template_id);

        $findsamecomponent=CmsTemplatesComponentModule::where('name',$request->name)->where('template_id',$template->id)->where('id','!=',$id)->first();
    
        if(empty($findsamecomponent))

      {
        CmsLogs::CreateLog(CmsModules::active()->id,'Zmieniono nazwę komponentu '.$component->name.' na '.$request->name.' w szablonie '.$template->name);


        File::delete('../resources/views/page/templates/'.$template->path.'/'.$component->path.'.blade.php');
     
        $component->update(['name'=>$request->name,'content'=>$request->content,'path'=>$path,'header_status'=>0,'background_status'=>0,'title_status'=>0,'subtitle_status'=>0,'text_status'=>0,'image_status'=>0,'gallery_status'=>0,'footer_status'=>0,'user_visible'=>$request->user_visible]);
        if($request->savearchive==1)
        {
          $archive=CmsTemplatesComponentArchivesModule::create(['name'=>$request->name,'component_id'=>$id,'template_id'=>$component->template_id,'copied_template_id'=>$component->copied_template_id,'content'=>$request->content,'path'=>$path,'header_status'=>0,'background_status'=>0,'title_status'=>0,'subtitle_status'=>0,'text_status'=>0,'image_status'=>0,'gallery_status'=>0,'footer_status'=>0,'user_visible'=>$request->user_visible,'sort'=>$component->sort]);
        }
      
        if(!empty($request->checkboxmodule))
       {
        foreach ($request->checkboxmodule as $key =>$value) 
        {
          
        if($request->checkboxmodule[$key]!=0)
        {
 
          $component->update([$component->chosenBoxes($request->checkboxmodule[$key])=>1]);
          if($request->savearchive==1)
          {
             $archive->update([$component->chosenBoxes($request->checkboxmodule[$key])=>1]);
          }
          CmsLogs::CreateLog(CmsModules::active()->id,'Włączono opcję '.$component->chosenBoxes($request->checkboxmodule[$key]).' w komponencie '.$request->name.' w szablonie '.$template->name);

         }
 
         }
        }

        File::put('../resources/views/page/templates/'.$template->path.'/'.$path.'.blade.php', $request->content);

        Session::flash('success','Edytowano komponent pomyślnie.');
      }
      else{
         Session::flash('error','Istnieje już komponent o takiej nazwie.');
    
        return redirect()->back();
    
      }

        return redirect('/cms/module/templates/'.$request->template_id.'/components');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($template,$id)
    {
       $module=CmsMain::module();
         $template=CmsTemplatesModule::findorfail($template);
        $component=CmsTemplatesComponentModule::findorfail($id); 
        CmsLogs::CreateLog(CmsModules::active()->id,'Usunięto komponent '.$component->name.' w szablonie '.$template->name);

         File::delete('../resources/views/page/templates/'.$template->path.'/'.$component->path.'.blade.php');
        $component->delete();
        Session::flash('success','Usunięto komponent pomyślnie.');
       return redirect('/cms/module/templates/'.$template->id.'/components');
    }

     public function destroyarchive($template,$component,$id)
    {
       $module=CmsMain::module();
         $template=CmsTemplatesModule::findorfail($template);
        $component=CmsTemplatesComponentArchivesModule::findorfail($id); 
        CmsLogs::CreateLog(CmsModules::active()->id,'Usunięto archiwum komponentu '.$component->name.' w szablonie '.$template->name);

         
        $component->delete();
        Session::flash('success','Usunięto archiwum komponentu pomyślnie.');
       return redirect('/cms/module/templates/'.$template->id.'/components');
    }

}
