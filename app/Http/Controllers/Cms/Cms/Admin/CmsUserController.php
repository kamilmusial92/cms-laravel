<?php

namespace App\Http\Controllers\Cms\Cms\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Cms\Shop\ShopCustomer;
use App\User;
use App\Http\Requests\Cms\Cms\CmsUserUpdate;
use App\Http\Requests\Cms\Cms\CmsUserSavePassword;
use Illuminate\Support\Facades\Hash;
class CmsUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       if(Auth::user()->id==$id)
       {
           $user=Auth::user();
           return view('cms/cms/user.show',compact('user'));
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->id==$id)
       {
           $user=Auth::user();
           return view('cms/cms/user.edit',compact('user'));
       }
    }

    public function editpassword($id)
    {
        if(Auth::user()->id==$id)
       {
           $user=Auth::user();
           return view('cms/cms/user.editpassword',compact('user'));
       }
    }
    
    public function savepassword(CmsUserSavePassword $request, $id)
    {
      
        $user=User::findorFail($id);

        $currentpass=$request->current_password;

        if(!Hash::check($currentpass, $user->password))
        {
            Session::flash('error','Podano nieprawidłowe aktualne hasło.');
            return redirect('/cms/user/'.$id);
        }
        elseif($request->new_password!=$request->repeat_new_password)
        {
            Session::flash('error','Podane hasła różnią się.');
            return redirect('/cms/user/'.$id);
        }
        else
        {
           
            $user->update('password'=>Hash::make($request['new_password']));
            Session::flash('success','Edytowano dane poprawnie.');
            return redirect('/cms/user/'.$id);
        }


           
        
       
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsUserUpdate $request, $id)
    {
       $customer=ShopCustomer::where('user_id',$id);
       $customer->update(['first_name'=>$request->first_name,'last_name'=>$request->last_name]);
        $user=User::findorFail($id);
        $user->update(['email'=>$request->email]);
        Session::flash('success','Edytowano dane poprawnie.');
       return redirect('/cms/user/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
