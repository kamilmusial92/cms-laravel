<?php

namespace App\Http\Controllers\Cms\Cms\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsModules;
use App\Http\Requests\Cms\Cms\CmsCreateModule;
use Session;
use Auth;
class CmsAdminModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
    {
        $this->middleware('admincms');
    }

    public function index()
    {
        $activemodules=CmsModules::chosen(Auth::user()->id);
       $modules=CmsModules::get();
         return view('cms/cms/admin/modules.index',compact('activemodules','modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $activemodules=CmsModules::chosen(Auth::user()->id);
     return view('cms/cms/admin/modules.create',compact('activemodules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsCreateModule $request)
    {
       
        $new=CmsModules::create($request->all());

        Session::flash('success','Stworzono moduł.');
        return redirect('/cms/admin/modules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activemodules=CmsModules::chosen(Auth::user()->id);
        $module=CmsModules::findorFail($id);
       return view('cms/cms/admin/modules.show',compact('activemodules','module'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activemodules=CmsModules::chosen(Auth::user()->id);
        $module=CmsModules::findorFail($id);
        return view('cms/cms/admin/modules.edit',compact('activemodules','module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsCreateModule $request, $id)
    {

        $module=CmsModules::findorFail($id);
        $module->update($request->all());
        Session::flash('success','Edytowany moduł.');
        return redirect('/cms/admin/modules/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $module=CmsModules::findorFail($id);
        $module->delete();
        Session::flash('success','Usunięto moduł.');
        return redirect('/cms/admin/modules');
    }
}
