<?php

namespace App\Http\Controllers\Cms\Cms\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Shop\ShopCustomer;
use App\User;
use Session;
use Auth;
use Hash;
use App\Http\Requests\Cms\Cms\CmsAdminUsersCreate;
use App\Http\Requests\Cms\Cms\CmsUserSavePassword;
use App\Http\Requests\Cms\Cms\CmsUserNewPassword;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\Admin\CmsUsersChosenModules;
use App\Cms\Cms\Admin\CmsLogs;
class CmsAdminUsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('admincms');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activemodules=CmsModules::chosen(Auth::user()->id);
        $users=User::where('usercms',1)->get();
         return view('cms/cms/admin/users.index',compact('activemodules','users'));
    }

    public function categorylogs($id)
    {
        $activemodules=CmsModules::chosen(Auth::user()->id);
        $categorylogs=CmsModules::get();
        $user=User::findorFail($id);
         return view('cms/cms/admin/users.categorylogs',compact('activemodules','categorylogs','id','user'));
    }

    public function listoflogs($id,$category)
    {
        $activemodules=CmsModules::chosen(Auth::user()->id);
       $logs=CmsLogs::where('user_id',$id)->where('module_id',$category)->get();
       $category=CmsModules::findorFail($category);
       $categorylogs=CmsModules::get();
        $user=User::findorFail($id);
         return view('cms/cms/admin/users.logs',compact('activemodules','category','categorylogs','id','user','logs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        if($user->AdminUser())
        {
            $activemodules=CmsModules::chosen(Auth::user()->id);
     $firstname='';
            $lastname='';
            $email='';
             return view('cms/cms/admin/users.create',compact('activemodules','firstname','lastname','email'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsAdminUsersCreate $request)
    {

       
         if($request->new_password!=$request->repeat_new_password)
        {
            Session::flash('error','Podane hasła różnią się.');
            $firstname=$request->first_name;
            $lastname=$request->last_name;
            $email=$request->email;
            $activemodules=CmsModules::chosen(Auth::user()->id);
            return view('cms/cms/admin/users.create',compact('activemodules','firstname','lastname','email'));
        }
        else{

         $user=User::create(['email'=>$request->email,'password' => Hash::make($request['new_password']),'usercms'=>1]);
        

        ShopCustomer::create(['first_name'=>$request->first_name,'last_name'=>$request->last_name,'user_id'=>$user->id]);

        Session::flash('success','Stworzono użytkownika.');

          return redirect('/cms/admin/users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::findorFail($id);
        if($user->usercms==1)
        {
            $activemodules=CmsModules::chosen(Auth::user()->id);
            $modules=CmsUsersChosenModules::where('user_id',$id)->get();
             return view('cms/cms/admin/users.show',compact('activemodules','user','modules'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $user=User::findorFail($id);
        if($user->usercms==1)
        {
    $activemodules=CmsModules::chosen(Auth::user()->id);
             return view('cms/cms/admin/users.edit',compact('activemodules','user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $customer=ShopCustomer::where('user_id',$id);
       $customer->update(['first_name'=>$request->first_name,'last_name'=>$request->last_name]);
        $user=User::findorFail($id);
        $user->update(['email'=>$request->email]);
        Session::flash('success','Edytowano dane poprawnie.');
       return redirect('/cms/admin/users/'.$id);
    }

       public function editpassword($id)
    {
         $user=User::findorFail($id);
        if($user->usercms==1)
        {
          $activemodules=CmsModules::chosen(Auth::user()->id);
           return view('cms/cms/admin/users.editpassword',compact('activemodules','user'));
       }
    }

     public function savepassword(CmsUserNewPassword $request, $id)
    {
      
        $user=User::findorFail($id);

        $currentpass=$request->current_password;

      
        if($request->new_password!=$request->repeat_new_password)
        {
            Session::flash('error','Podane hasła różnią się.');
            return redirect('/cms/admin/users/'.$id.'/editpassword');
        }
        else
        {
           
$user->update(['password'=>Hash::make($request['new_password'])]);
            Session::flash('success','Edytowano dane poprawnie.');
            return redirect('/cms/admin/users/'.$id);
        }


       
    }

    public function editmodules($id)
    {
        $user=User::findorFail($id);
      $modules=CmsModules::get();
          $activemodules=CmsModules::chosen(Auth::user()->id);
           return view('cms/cms/admin/users.editmodules',compact('activemodules','user','modules'));
       
    }

      public function savemodules(Request $request,$id)
    {

        $user=User::findorFail($id);

        
        CmsUsersChosenModules::where('user_id',$id)->delete();
    


       foreach ($request->checkboxmodule as $key =>$value) 
       {
     
    
       if($request->checkboxmodule[$key]!=0)
       {
         $module=CmsUsersChosenModules::where('module_id',$request->checkboxmodule[$key])->where('user_id',$id)->first();

            if(empty($module))
            {
             $sort=CmsModules::findorFail($request->checkboxmodule[$key]);
           
            CmsUsersChosenModules::create(['module_id'=>$request->checkboxmodule[$key],'user_id'=>$id,'sort'=>$sort->sort]);
            
            }

        }

        }
        
        Session::flash('success','Edytowano dane poprawnie.');
        return  redirect('/cms/admin/users/'.$id);
       
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
       $user=User::findorFail($id);
        
        if($user->usercms==1)
        {
       $customer=ShopCustomer::where('user_id',$id)->delete();
       $user->delete();

        Session::flash('success','Usunięto użytkownika.');
            return redirect('/cms/admin/users');
        }
    }
}
