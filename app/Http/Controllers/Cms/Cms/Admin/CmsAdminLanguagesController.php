<?php

namespace App\Http\Controllers\Cms\Cms\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\CmsLanguages;
use Session;
use Auth;
use App\Http\Requests\Cms\UploadImage;
class CmsAdminLanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
         public function __construct()
    {
        $this->middleware('admincms');
    }
    public function index()
    {
      $activemodules=CmsModules::chosen(Auth::user()->id);
       $languages=CmsLanguages::get();
         return view('cms/cms/admin/languages.index',compact('activemodules','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activemodules=CmsModules::chosen(Auth::user()->id);
     return view('cms/cms/admin/languages.create',compact('activemodules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $lang=CmsLanguages::create($request->all());
       return redirect('/cms/admin/languages/'.$lang->id.'/edit/2');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

         public function image($id)
    {
         $activemodules=CmsModules::chosen(Auth::user()->id);
      $lang=CmsLanguages::findorFail($id);
       return view('cms/cms/admin/languages.image',compact('activemodules','lang'));
    }

  

   public function uploadimage(UploadImage $request,$id)
    {
    
if(!empty($request->file('files')))
{
 $newimage=CmsLanguages::findorFail($id);
 if($newimage->src!='')
 {
    unlink('storage/cms/languages/'.$newimage->src);
 }


       $file= $request->file('files') ;
       
           
       
      

            $filename =  time().'-'.preg_replace('/[^a-zA-Z0-9-_\.]/','',$file->getClientOriginalName());
            
            $file->move(('storage/cms/languages/'), $filename);
           
           
            $newimage->update(['src'=>$filename]);
          
        


          
           Session::flash('success','Wgrano nowe zdjęcia.');

        return redirect('/cms/admin/languages/'.$id.'/edit/2');
      }
      else{
        Session::flash('error','Nie wybrano zdjęć.');
      return redirect('/cms/admin/languages/'.$id.'/edit/2');
      }
    }

       public function removelanguageimage(Request $request,$id)
    {

         $lang=CmsLanguages::findorFail($request->image);
            unlink('storage/cms/languages/'.$lang->src);
       
          $lang->update(['src'=>'']);

        return redirect('/cms/admin/languages/'.$id.'/edit/2');
    }

    public function show($id)
    {
         $activemodules=CmsModules::chosen(Auth::user()->id);
        $lang=CmsLanguages::findorFail($id);
       return view('cms/cms/admin/languages.show',compact('activemodules','lang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $activemodules=CmsModules::chosen(Auth::user()->id);
        $lang=CmsLanguages::findorFail($id);
        return view('cms/cms/admin/languages.edit',compact('activemodules','lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $lang=CmsLanguages::findorFail($id);
        $lang->update($request->all());
        Session::flash('success','Edytowany wersję językową.');
        return redirect('/cms/admin/languages/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
