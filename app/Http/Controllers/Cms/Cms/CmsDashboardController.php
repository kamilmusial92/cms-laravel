<?php

namespace App\Http\Controllers\Cms\Cms;

use App\Cms\Cms\InteractiveOffers;
use View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Cms\Cms\CmsModules;
class CmsDashboardController extends Controller
{
   	
  
       public function __construct()
       {
        $this->middleware('cms');

         

        }

       public function dashboard()
       {
      
       	
		    return view('cms/cms.dashboard');
       }


         public function modules()
       {
      $activemodules=CmsModules::chosen(Auth::user()->id);
      
     return view('cms/cms/modules.index',compact('activemodules'));
       }

}
