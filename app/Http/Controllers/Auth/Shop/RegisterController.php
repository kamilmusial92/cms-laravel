<?php

namespace App\Http\Controllers\Auth\Shop;

use App\User;
use App\Page\Shop\ShopCustomer;
use App\Page\Shop\ShopNewsletterCustomer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Page\Shop\ShopCartCookies;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Session;
use App\Rules\GoogleRecaptcha;
use App\Page\Cms\CmsPageMenu;

use App\Page\Shop\ShopHome;

use Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

     public function showRegistrationForm(){
         $listpackages=ShopHome::listofcardpackages();
         $allpricepackages=ShopHome::summcardspackages();
          $count=ShopHome::cookies(); 
           $listofmenu=CmsPageMenu::menu();
        $listoffootermenu=CmsPageMenu::footermenu();
        $listofcategories=CmsPageMenu::submenu();
        $summcards=ShopHome::summcards();
        $listofcards=ShopHome::listofcard();
       return view('auth/shop.register',compact('allpricepackages','listpackages','count','listofmenu','listofcategories','listoffootermenu','listofcards','summcards'));
    }
    

    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'nip'=>'sometimes',
            'company_name'=>'sometimes',
            'postcode'=>'required',
            'city'=>'required|string',
            'country'=>'required|string',
            'address'=>'required|string',
            'phone'=>'required|numeric',
             'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'acceptable'=>'required'

           //  'g-recaptcha-response' => ['required', new GoogleRecaptcha]
        ]);
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
          $user=User::create([
            
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'active'=>0,
            'activation_code'=>str_random(30).time(),

        ]);
             ShopCustomer::create([
            'first_name' => $data['first_name'],
             'last_name' => $data['last_name'],
             'nip'=>$data['nip'],
             'company_name'=>$data['company_name'],
             'postcode'=>$data['postcode'],
             'city'=>$data['city'],
             'address'=>$data['address'],
             'phone'=>$data['phone'],
             'country'=>$data['country'],

              'first_name_delivery' => $data['first_name'],
             'last_name_delivery' => $data['last_name'],
             'nip_delivery'=>$data['nip'],
             'company_name_delivery'=>$data['company_name'],
             'postcode_delivery'=>$data['postcode'],
             'city_delivery'=>$data['city'],
             'address_delivery'=>$data['address'],
             'phone_delivery'=>$data['phone'],
             'country_delivery'=>$data['country'],

           'user_id'=>$user->id
        ]);
         if(!empty($data['newsletter']))
         {


             if($data['newsletter']=='tak')
             {
                $find=ShopNewsletterCustomer::where('email',$data['email'])->first();
                if(empty($find))
                {
                    $new=new ShopNewsletterCustomer(['email'=>$data['email']]);
                    $new->save();
                }
             }
         }
              //$user->notify(new \App\Notifications\Page\Shop\UserRegisteredSuccessfully($user));
            $email=$user->email;
            $title='Rejestracja w sklepie Gmyrex.pl';
        Mail::send('auth/shop.sendemail_register', ['activate_code'=>$_SERVER['SERVER_NAME'].'/verify-user/'.$user->activation_code], function ($message) use ($email,$title) {
        $message->from(env('MAIL_USERNAME'));

            $message->to($email)->subject($title);

        });
              
                return $user;
 

    }
   public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        return $this->registered($request, $user);
                        
    }

  protected function registered(Request $request, $user)
    {
         Session::flash('success','Aktywuj konto! Kod aktywacyjny został wysłany na adres e-mail.');
        return redirect('/login');
    }

 public function activateUser(string $activationCode)
    {
        
            $user = User::where('activation_code', $activationCode)->first();
            if (!$user) {
                Session::flash('error','Niepoprawny kod aktywacyjny!');
                return redirect('/login');
            }
            $user->active          = 1;
            $user->activation_code = null;
            $user->save();
          //  auth()->login($user);
         Session::flash('success','Aktywowano konto pomyślnie. Możesz teraz się już zalogować.');
        return redirect()->to('/login');
    }

}
