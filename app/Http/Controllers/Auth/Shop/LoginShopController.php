<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Page\Shop\ShopCartCookies;
use Session;
use App\Rules\GoogleRecaptcha;
use App\Page\Cms\CmsPageMenu;

use App\Page\Shop\ShopHome;
class LoginShopController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */


public function authenticated(Request $request)
    {
        if (Auth::user()->active==0) {
            auth()->logout();

      
             Session::flash('info','Aktywuj konto! Kod aktywacyjny został wysłany na adres e-mail.');
       return redirect('login');
        }
        elseif(Auth::user()->admin==1){
            auth()->logout();
             Session::flash('error','Nie masz uprawnień.');
            return redirect('login');
        }
       
    }
  
  protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
          //   'g-recaptcha-response' => ['required', new GoogleRecaptcha]
        ]);
    }

     public function showLoginForm(){
         $listpackages=ShopHome::listofcardpackages();
         $allpricepackages=ShopHome::summcardspackages();
        $count=ShopHome::cookies();
        $listofmenu=CmsPageMenu::menu();
        $listoffootermenu=CmsPageMenu::footermenu();
        $listofcategories=CmsPageMenu::submenu();
        $summcards=ShopHome::summcards();
            $listofcards=ShopHome::listofcard();
       return view('auth/shop.login',compact('allpricepackages','listpackages','count','listofmenu','listofcategories','listoffootermenu','listofcards','summcards'));
    }



    protected function redirectTo()
{
  
      
 
   
        if(Auth::User()->admin==1)
        {
            route('getlogout');
        }

       else{


        return '/';
        }
    
    
   
    
}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
