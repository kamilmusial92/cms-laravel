<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

use App\Page\Cms\CmsPageMenu;
use App\Page\Shop\ShopCartCookies;
use App\Page\Shop\ShopHome;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

  public function showLinkRequestForm()
    {
         $listpackages=ShopHome::listofcardpackages();
         $allpricepackages=ShopHome::summcardspackages();
 $count=ShopHome::cookies();
        $listofmenu=CmsPageMenu::menu();
        $listoffootermenu=CmsPageMenu::footermenu();
        $listofcategories=CmsPageMenu::submenu();
        $summcards=ShopHome::summcards();
            $listofcards=ShopHome::listofcard();
        return view('auth.passwords.email',compact('allpricepackages','listpackages','count','listofmenu','listofcategories','listoffootermenu','listofcards','summcards'));
    }

   public function sendResetLinkEmail(Request $request)
    {
        $us=User::where('email',$request->email)->first();
         if($us->admin==0)
        {
        $this->validateEmail($request);
        
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);
                }
                else{
                     Session::flash('error','Podany adres e-mail nie istnieje.');
                    return view('auth.passwords.email');
                }

    }

      protected function validateEmail(Request $request)
    {

        $this->validate($request, ['email' => 'required|email']);
   
    }
        protected function sendResetLinkResponse($response)
    {
        return back()->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()->withErrors(
            ['email' => trans($response)]
        );
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }

  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
