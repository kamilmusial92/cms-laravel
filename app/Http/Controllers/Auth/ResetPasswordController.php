<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use App\Page\Cms\CmsPageMenu;
use App\Page\Shop\ShopCartCookies;
use App\Page\Shop\ShopHome;
use Illuminate\Http\Request;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

 public function showResetForm(Request $request, $token = null)
    {
         $listpackages=ShopHome::listofcardpackages();
         $allpricepackages=ShopHome::summcardspackages();
        $count=ShopHome::cookies();
        $listofmenu=CmsPageMenu::menu();
        $listoffootermenu=CmsPageMenu::footermenu();
        $listofcategories=CmsPageMenu::submenu();
        $summcards=ShopHome::summcards();
        $listofcards=ShopHome::listofcard();
        $email=$request->email;

        return view('auth.passwords.reset',compact('allpricepackages','listpackages','email','token','count','listofmenu','listofcategories','listoffootermenu','listofcards','summcards'));
    }

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
