<?php

namespace App\Http\Controllers\Page\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms\Cms\Modules\CmsComponentsModule;
use App\Cms\Cms\Modules\CmsSubpagesModule;
use App\Cms\Cms\CmsLanguages;

class CmsPageController extends Controller
{
       
       
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $language=CmsLanguages::where('url','pl')->first();
        $page=CmsSubpagesModule::where('sort',0)->where('lang_id',$language->id)->first();
  
        $components=CmsComponentsModule::join('cms_templates_component_modules', 'cms_components_modules.template_component_id', '=', 'cms_templates_component_modules.id')->join('cms_subpages_modules','cms_components_modules.subpage_id','=','cms_subpages_modules.id')->orderby('cms_components_modules.sort','ASC')->where('cms_subpages_modules.sort',0)->where('cms_subpages_modules.lang_id',$language->id)->get();
       return view('page.index',compact('components','page'));
    }

    public function menu($id)
    {
        $lang=CmsLanguages::where('url',$id)->first();
        if(empty($lang))
        {

            
            $page=CmsSubpagesModule::where('page_url',$id)->first();
            if(!empty($page))
            {

            
            $components=CmsComponentsModule::join('cms_templates_component_modules', 'cms_components_modules.template_component_id', '=', 'cms_templates_component_modules.id')->join('cms_subpages_modules','cms_components_modules.subpage_id','=','cms_subpages_modules.id')->orderby('cms_components_modules.sort','ASC')->where('cms_subpages_modules.page_url',$id)->get();
                return view('page.index',compact('components','page'));
            }
            else{
                return abort('404');
            }
        }
        else
        {
        $language=CmsLanguages::where('url',$lang->url)->first();
        $page=CmsSubpagesModule::where('sort',0)->where('lang_id',$language->id)->first();
        return redirect($lang->url.'/'.$page->page_url);
        }

    }

    public function lang($lang,$menu)
    {
        $language=CmsLanguages::where('url',$lang)->first();
       
        $page=CmsSubpagesModule::where('page_url',$menu)->where('lang_id',$language->id)->first();
        if(!empty($page))
        {

        
        $components=CmsComponentsModule::join('cms_templates_component_modules', 'cms_components_modules.template_component_id', '=', 'cms_templates_component_modules.id')->join('cms_subpages_modules','cms_components_modules.subpage_id','=','cms_subpages_modules.id')->orderby('cms_components_modules.sort','ASC')->where('cms_subpages_modules.page_url',$menu)->get();
            return view('page.index',compact('components','page'));
        }
        else{
            return abort('404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
