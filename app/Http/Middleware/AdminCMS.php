<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AdminCMS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() &&  Auth::user()->admincms==1) {
            return $next($request);
     }
      //  return $next($request);
    }
}
