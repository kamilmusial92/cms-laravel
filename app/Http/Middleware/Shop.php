<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Shop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      //    if ( Auth::user()->active ==0) {
        //  return redirect()->route('getlogout');
       // }
           if (Auth::user() &&  Auth::user()->admin == 1) 
        {
           return redirect()->route('getlogout');
             
        }
        else
        {
            return $next($request);
        }



    return redirect('/');
    }
}
