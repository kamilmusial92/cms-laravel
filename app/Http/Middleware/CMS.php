<?php

namespace App\Http\Middleware;
use App\Cms\Cms\CmsModules;
use App\Cms\Cms\CmsMain;
use Closure;
use Auth;
class CMS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
     

        if (Auth::user() &&  Auth::user()->admin == 1 or Auth::User()->usercms==1 or Auth::User()->admincms==1) {
            
            return $next($request);
        }
          else 
        {
           return redirect()->route('getlogout');
             
        }

    return redirect('/cms/login');
    }
}
