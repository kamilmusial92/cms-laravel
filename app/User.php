<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','activation_code','usercms'
    ];
 protected $casts = [
        'active' => 'boolean'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //klient posiada wiele produktów w koszyku
      public function products(){
        return $this->hasMany('App\Page\Shop\ShopProduct','customer_id');
    }
      public function orders(){
        return $this->hasMany('App\Page\Shop\ShopOrder','customer_id');
    }
       public function favorites(){
        return $this->hasMany('App\Page\Shop\ShopFavoriteProducts','customer_id');
    }
    public function customer(){
      return $this->hasOne('App\Page\Shop\ShopCustomer');
    }

    public function AdminUser(){
        if($this->admincms==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    

}
