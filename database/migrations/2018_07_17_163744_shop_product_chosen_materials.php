<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductChosenMaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('shop_product_chosen_materials', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('product_id')->unsigned();
             $table->integer('main_product');
              $table->integer('material_id')->unsigned();
              $table->decimal('price',9,2);
               $table->integer('basic');
            $table->timestamps();
        });

             Schema::table('shop_product_chosen_materials', function(Blueprint $table) {
           
            $table->foreign('product_id')->references('id')->on('shop_products')->onDelete('cascade');
            $table->foreign('material_id')->references('id')->on('shop_materials')->onDelete('cascade');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_product_chosen_materials');
    }
}
