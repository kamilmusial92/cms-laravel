<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopCartCookies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_cart_cookies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('package_id');
            $table->integer('frame_id');
            $table->integer('material_id');
             $table->integer('category_id')->unsigned();
            $table->integer('quantity');
            $table->integer('packagenr');
            $table->decimal('price',9,2);
               $table->decimal('price_frame',9,2);
            $table->decimal('price_material',9,2);
            $table->integer('advance_payment');
             $table->decimal('width',10,1);
                $table->decimal('height',10,1);
                $table->decimal('overall_height',10,1)->nullable();
                $table->decimal('height_seat',10,1)->nullable();
                $table->decimal('depth',10,1)->nullable();
                $table->decimal('length',10,1);
                $table->string('inserts_table');
                $table->string('type');
            $table->text('cookies');
            $table->timestamps();
        });
          Schema::table('shop_cart_cookies', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('shop_products')->onDelete('cascade');
             //$table->foreign('frame_id')->references('id')->on('shop_frames')->onDelete('cascade');
           // $table->foreign('material_id')->references('id')->on('shop_materials')->onDelete('cascade');
            
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_cart_cookies');
    }
}
