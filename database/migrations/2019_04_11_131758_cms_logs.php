<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('cms_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('module_id')->unsigned();
             $table->string('text');
          
            
                 
            $table->timestamps();
        });

            Schema::table('cms_logs', function(Blueprint $table) {
           
         
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
             $table->foreign('module_id')->references('id')->on('cms_modules')->onDelete('cascade');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_logs');
    }
}
