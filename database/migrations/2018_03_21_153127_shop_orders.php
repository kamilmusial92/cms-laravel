<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('status')->default(0);
            $table->string('address');
            $table->string('city');
            $table->integer('phone');
            $table->string('postcode');
            $table->string('country');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('payment_method');
            $table->string('payment_option');
            $table->integer('delivery_option');
            $table->decimal('price',9,2);
            $table->decimal('delivery_price',9,2);
             $table->decimal('free_delivery_price',9,2);
            $table->integer('status_of_implementation');
            $table->string('first_name_delivery');
            $table->string('last_name_delivery');
            $table->string('address_delivery');
            $table->string('city_delivery');
            $table->integer('phone_delivery');
            $table->string('postcode_delivery');
            $table->string('country_delivery');
            $table->string('vat');
            $table->string('company_name')->nullable();
            $table->string('company_name_delivery')->nullable();
            $table->string('nip');
            $table->integer('discount_id')->unsigned();
            $table->integer('discount_price');
            $table->string('orderId');
            $table->dateTime('date_order');
            $table->timestamps();
        });

          Schema::table('shop_orders', function(Blueprint $table) {
          
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
            
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_orders');
    }
}
