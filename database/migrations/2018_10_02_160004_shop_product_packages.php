<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('shop_product_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
                $table->integer('quantity');
            
                   $table->integer('advance_payment');
                   $table->text('description')->nullable();
                    $table->integer('status');
                     $table->integer('recommended');
                     $table->decimal('basicfilterprice',9,2);
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_product_packages');
    }
}
