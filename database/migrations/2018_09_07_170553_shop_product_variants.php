<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductVariants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('shop_product_variants', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('main_product');
            $table->string('name');
             $table->integer('category_id')->unsigned();
            $table->string('name_product');
             $table->integer('quantity');
             $table->decimal('price',9,2);
             $table->decimal('discount',9,2);
             $table->text('description')->nullable();
             $table->text('tags');
             $table->integer('advance_payment');
               $table->decimal('width',10,1);
                $table->decimal('height',10,1);
                $table->decimal('overall_height',10,1);
                $table->decimal('height_seat',10,1);
                $table->decimal('depth',10,1);
                $table->decimal('length',10,1);
                $table->string('inserts_table');
                $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('shop_newsletters');
    }
}
