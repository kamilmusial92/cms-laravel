<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductVariantPictures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_variant_pictures', function (Blueprint $table) {
            $table->increments('id');
              $table->text('path');
             $table->integer('variant_id')->unsigned();
               $table->integer('sorting');
            $table->timestamps();
        });

        Schema::table('shop_product_variant_pictures', function(Blueprint $table) {
       $table->foreign('variant_id')->references('id')->on('shop_product_variants')->onDelete('cascade');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_product_variant_pictures');
    }
}
