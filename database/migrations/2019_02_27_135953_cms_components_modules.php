<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsComponentsModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_components_modules', function (Blueprint $table) {
            $table->increments('id');
          
            $table->string('name');
           $table->integer('subpage_id')->unsigned();
           $table->integer('template_component_id')->unsigned();
            $table->integer('sort');
            $table->integer('status');
            $table->text('header');
            $table->integer('background');
            $table->string('title');
            $table->string('subtitle');
            $table->text('text');
            $table->string('image');
            $table->integer('gallery');
            $table->text('footer');
            $table->string('url');
            $table->integer('copyable');
           $table->integer('copyable_id');
            $table->timestamps();
        });
            Schema::table('cms_components_modules', function(Blueprint $table) {
           
            $table->foreign('subpage_id')->references('id')->on('cms_subpages_modules')->onDelete('cascade');
            $table->foreign('template_component_id')->references('id')->on('cms_templates_component_modules')->onDelete('cascade');
           
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('cms_components_modules');
    }
}
