<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopSettingsDeliveryOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('shop_settings_delivery_options', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('delivery_id')->unsigned();
             $table->decimal('price',9,2);
            
            $table->timestamps();
        });
            Schema::table('shop_settings_delivery_options', function(Blueprint $table) {
           
         $table->foreign('delivery_id')->references('id')->on('shop_settings_deliveries')->onDelete('cascade');
         });
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('shop_settings_delivery_options');
    }
}
