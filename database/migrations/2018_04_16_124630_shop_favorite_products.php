<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopFavoriteProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_favorite_products', function (Blueprint $table) {
            $table->increments('id');
               $table->integer('product_id')->unsigned();
                  $table->integer('customer_id')->unsigned();
            $table->timestamps();
        });

           Schema::table('shop_favorite_products', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('shop_products')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
            
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_favorite_products');
    }
}
