<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductChosenFrames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('shop_product_chosen_frames', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('product_id')->unsigned();
             $table->integer('main_product');
              $table->integer('frame_id')->unsigned();
                $table->decimal('price',9,2);
                 $table->integer('basic');
            $table->timestamps();
        });

             Schema::table('shop_product_chosen_frames', function(Blueprint $table) {
           
            $table->foreign('product_id')->references('id')->on('shop_products')->onDelete('cascade');
            $table->foreign('frame_id')->references('id')->on('shop_frames')->onDelete('cascade');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('shop_product_chosen_frames');
    }
}
