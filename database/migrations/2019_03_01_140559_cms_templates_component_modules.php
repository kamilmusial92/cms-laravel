<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsTemplatesComponentModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_templates_component_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
             $table->integer('template_id')->unsigned();
                 $table->integer('copied_template_id')->default(0);
                 $table->integer('copied_template_component_id')->default(0);
                 $table->integer('header_status')->default(0);
              $table->integer('background_status')->default(0);
              $table->integer('title_status')->default(0);
              $table->integer('subtitle_status')->default(0);
              $table->integer('text_status')->default(0);
              $table->integer('image_status')->default(0);
              $table->integer('gallery_status')->default(0);
              $table->integer('footer_status')->default(0);
              $table->integer('url_status')->default(0);
              $table->integer('file_status')->default(0);
             $table->string('content');
             $table->string('path');
            $table->integer('sort');
             $table->integer('user_visible')->default(0);
                 
            $table->timestamps();
        });

         Schema::table('cms_templates_component_modules', function(Blueprint $table) {
           
            $table->foreign('template_id')->references('id')->on('cms_templates_modules')->onDelete('cascade');
           
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_template_component_modules');
    }
}
