<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsChosenFilesToDownloadModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
             Schema::create('cms_chosen_files_to_download_modules', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('component_id')->unsigned();
         $table->integer('file_id')->unsigned();
           
          
            $table->timestamp('created_at')->nullable();
             $table->timestamp('updated_at')->nullable();
        });

               Schema::table('cms_chosen_files_to_download_modules', function(Blueprint $table) {
           
            $table->foreign('file_id')->references('id')->on('cms_files_to_download_modules')->onDelete('cascade');
            $table->foreign('component_id')->references('id')->on('cms_components_modules')->onDelete('cascade');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('cms_chosen_files_to_download_modules');
    }
}
