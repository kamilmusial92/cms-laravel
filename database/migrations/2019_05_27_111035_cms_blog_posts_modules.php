<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsBlogPostsModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('cms_blog_posts_modules', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('user_id')->unsigned();
           $table->integer('thumbnail_id')->unsigned();
           $table->integer('image_id')->unsigned();
           $table->integer('visible');
           $table->string('title');
           $table->string('url');
           $table->string('keywords');
           $table->string('info');
           $table->text('long_text');
             $table->text('tags');
           $table->timestamp('publish_at')->nullable();
            $table->timestamp('created_at')->nullable();
             $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('cms_blog_posts_modules');
    }
}
