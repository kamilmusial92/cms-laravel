<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('shop_customers', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('user_id')->unsigned();
            $table->string('first_name');
             $table->string('last_name');
            $table->integer('phone')->default(0);
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('postcode')->default(0);
            $table->string('country')->nullable();
            $table->integer('phone_delivery')->default(0);
            $table->string('first_name_delivery')->nullable();
            $table->string('last_name_delivery')->nullable();
            $table->string('address_delivery')->nullable();
            $table->string('city_delivery')->nullable();
            $table->string('postcode_delivery')->default(0);
            $table->string('country_delivery')->nullable();
            $table->string('company_name')->nullable();
            $table->string('nip')->nullable();
            $table->string('nip_delivery')->nullable();
            $table->string('account_type')->nullable();
            $table->string('company_name_delivery')->nullable();
            
            
            $table->rememberToken();
            $table->timestamps();
        });

              Schema::table('shop_customers', function(Blueprint $table) {
           
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('shop_customers');
    }
}
