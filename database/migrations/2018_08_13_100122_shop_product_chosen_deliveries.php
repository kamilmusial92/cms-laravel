<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductChosenDeliveries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('shop_product_chosen_deliveries', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('product_id')->unsigned();
             $table->integer('main_product');
              $table->integer('delivery_id')->unsigned();
                $table->decimal('price',9,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('shop_product_chosen_deliveries');
    }
}
