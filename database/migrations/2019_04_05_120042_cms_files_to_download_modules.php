<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsFilesToDownloadModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cms_files_to_download_modules', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('user_id')->unsigned();
         
           
           $table->string('alt');
           $table->string('file_path');
           $table->string('file_name');
           $table->string('file_type');
            $table->timestamp('created_at')->nullable();
             $table->timestamp('updated_at')->nullable();
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_files_to_download_modules');
    }
}
