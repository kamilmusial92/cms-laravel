<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductVariantsChosenMaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('shop_product_variants_chosen_materials', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('main_product');
             $table->integer('variant_id')->unsigned();
              $table->integer('material_id')->unsigned();
                $table->decimal('price',9,2);
            $table->timestamps();
        });

             Schema::table('shop_product_variants_chosen_materials', function(Blueprint $table) {
           
            $table->foreign('variant_id')->references('id')->on('shop_product_variants')->onDelete('cascade');
            $table->foreign('material_id')->references('id')->on('shop_materials')->onDelete('cascade');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_product_variants_chosen_materials');
    }
}
