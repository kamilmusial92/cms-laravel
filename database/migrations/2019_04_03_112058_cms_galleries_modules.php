<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsGalleriesModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_galleries_modules', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('user_id')->unsigned();
           
           
           $table->string('name');
           $table->text('description');
          
            $table->timestamp('created_at')->nullable();
             $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('cms_galleries_modules');
    }
}
