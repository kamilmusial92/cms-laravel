<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsSubpagesModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('cms_subpages_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lang_id')->unsigned();
            $table->string('name');
            $table->string('url');
                $table->integer('sort');
                $table->string('title_page');
                $table->string('description_page');  
                   $table->string('keywords_page');  
                    $table->integer('image_page');  
                $table->timestamps();
        });
        Schema::table('cms_subpages_modules', function(Blueprint $table) {
           
            $table->foreign('lang_id')->references('id')->on('cms_languages')->onDelete('cascade');
            
           
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('cms_subpages_modules');
    }
}
