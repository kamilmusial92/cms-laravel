<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopProductOpinions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_opinions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('opinion');
            $table->tinyInteger('rate');
            $table->tinyInteger('status');
            $table->integer('product_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('shop_product_opinions', function(Blueprint $table) {
       $table->foreign('product_id')->references('id')->on('shop_products')->onDelete('cascade');
        $table->foreign('user_id')->references('user_id')->on('shop_customers')->onDelete('cascade');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('shop_product_opinions');
    }
}
