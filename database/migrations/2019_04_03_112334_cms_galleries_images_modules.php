<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsGalleriesImagesModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('cms_galleries_images_modules', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('user_id')->unsigned();
           $table->integer('gallery_id')->unsigned();
           
           $table->string('alt');
           $table->string('file_path');
           $table->string('file_name');
           $table->string('file_type');
            $table->timestamp('created_at')->nullable();
             $table->timestamp('updated_at')->nullable();
        });
        Schema::table('cms_galleries_images_modules', function(Blueprint $table) {
           
            $table->foreign('gallery_id')->references('id')->on('cms_galleries_modules')->onDelete('cascade');
           
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('cms_galleries_images_modules');
    }
}
