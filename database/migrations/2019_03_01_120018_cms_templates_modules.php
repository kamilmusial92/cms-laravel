<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsTemplatesModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_templates_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('path');
             $table->string('title_page');

               $table->string('description_page');  
                  $table->string('keywords_page');  
                   $table->integer('image_page');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('cms_templates_modules');
    }
}
